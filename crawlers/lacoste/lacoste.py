from curadoria.crawlers.crawler_modelo.crawler_modelo import Crawler_Modelo
from selenium.webdriver.chrome.service import Service
from curadoria.libs.busca_codigo import Busca_Codigo
from selenium import webdriver
from pathlib import Path
from time import sleep
from math import floor
import os

class Lacoste(Crawler_Modelo):
    def pegar_urls_produtos(self):
        driver = webdriver.Chrome('C:\workspace\curadoria\webdriver\chromedriver.exe')
        driver.maximize_window()
        url_prefixo = 'https://www.lacoste.com/br/lacoste/'
        url_sufixo = ['masculino/',
                    'feminino/',
                    'infantil/',
                    'sale/']

        links = []
        for sufixo in url_sufixo:
            url = url_prefixo + sufixo
            driver.get(url)

            total_produtos = int(driver.find_element_by_css_selector('.js-plp-num-results.fs--medium.l-vmargin--medium').text.split(' ')[0].replace('.',''))
            total_pages = int(floor(total_produtos / 30) + 2)
            for page in range(1, total_pages):
                driver.get(url + '?page=' + str(page))
                try:
                    if driver.find_element_by_css_selector('.title--xlarge.ff-semibold.l-vmargin--medium.text-black').text == 'A página solicitada não foi encontrada':
                        continue
                except:
                    pass

                produtos = driver.find_element_by_css_selector('.js-plp-tiles-wrapper.js-promotions-section.cell-25').find_elements_by_css_selector('.l-relative.l-overflow-hidden.l-vmargin--small')
                for produto in produtos:
                    links.append(produto.find_element_by_tag_name('a').get_attribute('href'))
        return links

    #Nome, Categoria, Subcategoria, Código de referencia, EAN, Decrição, Fotos, Cor, Código da cor, Tamanho
    def pegar_dados_produtos(self, urls='0'):
        if type(urls) is not list:
            urls = [urls]
        coletados = self.carregar_links_coletados_csv()
        if urls == '0':
            urls = self.carregar_links_encontrados_csv()
        for url in urls:
            dicionario_produto = {}
            if url in coletados:
                continue            
            driver.get(url)
            sleep(2)
            try:
                if 'A página solicitada não foi encontrada' in driver.find_element_by_css_selector('.title--xlarge.ff-semibold.l-vmargin--medium.text-black').text:
                    continue
            except:
                pass
            tamanhos = []
            try:
                driver.find_element_by_css_selector('.js-select-size-label.critical-skeleton.btn-cta.text-black.js-popin-btn.js-pdp-size.js-track.no-mob.fs--medium.ff-normal').click()
                tamanhos_box = driver.find_element_by_css_selector('.js-pdp-popin-size-list.grid.flex--space-around.fs--medium.ff-normal').find_elements_by_tag_name('li')
                sleep(1)
                for tamanho in tamanhos_box:
                    tamanhos.append(tamanho.text)
                driver.find_element_by_css_selector('.l-popin-close.js-popin-close').click()
                sleep(1)
            except:
                tamanhos.append(driver.find_element_by_css_selector('.js-select-size-label.critical-skeleton.btn-cta.text-black.js-pdp-size.no-mob.fs--medium.ff-normal').text)
            dicionario_produto['nome'] = driver.find_element_by_css_selector('.title--medium.l-vmargin--medium.padding-m-2').text
            url_atual = driver.current_url
            try:
                dicionario_produto['categoria1'] = url_atual.split('/')[5]
            except:
                dicionario_produto['categoria1'] = ''
            try:
                dicionario_produto['categoria2'] = url_atual.split('/')[6]
            except:
                dicionario_produto['categoria2'] = ''
            try:
                dicionario_produto['categoria3'] = url_atual.split('/')[7]
            except:
                dicionario_produto['categoria3'] = ''
            dicionario_produto['codigo_referencia'] = url_atual.split('/')[-1].split('.')[0]
            dicionario_produto['sku'] = driver.find_element_by_css_selector('.js-pdp-section.js-fitanalytics-datas.page-section.pdp-section.grid').get_attribute('data-ean')
            descricao_box = driver.find_element_by_css_selector('.js-care-section.js-accordion-target.accordion-target')
            try:
                descricao = descricao_box.find_element_by_css_selector('.fs--medium.text-grey.l-vmargin--large').text
                dicionario_produto['descricao'] = descricao + '\n' + descricao_box.find_element_by_css_selector('.dashed-list.fs--medium.text-grey.l-vmargin-row-1.l-vmargin-row-m-3').text
            except:
                dicionario_produto['descricao'] = descricao_box.text.replace('Descrição\n','')
            dicionario_produto['codigo_cor'] = driver.find_element_by_css_selector('.js-pdp-color-title.flex.flex--align-center.fs--small.l-vmargin--small.l-hmargin--small').text.split(' ')[-1]
            try:
                dicionario_produto['cor'] = driver.find_element_by_name('og:product:color').get_attribute('content')
            except:
                dicionario_produto['cor'] = driver.find_element_by_css_selector('.js-pdp-color-title.flex.flex--align-center.fs--small.l-vmargin--small.l-hmargin--small').text.replace(dicionario_produto['codigo_cor'],'')[:-3]
            fotos_box = driver.find_element_by_css_selector('.js-slideshow-thumbs.pdp-slideshow-thumbs.flex.cell-mt-22.cell-m-24.flex--align-end').find_elements_by_tag_name('img')
            fotos = ''
            for foto in fotos_box:
                try:
                    fotos += foto.get_attribute('src').replace('imwidth=50','imwidth=1000') + ', '
                except:
                    continue
            dicionario_produto['fotos'] = fotos[:-2]
            for tamanho in tamanhos:
                dicionario_produto['tamanho'] = tamanho
                self.gerar_arquivo('lacoste.csv', dicionario_produto)
            self.adicionar_links_coletados_csv(url)                

# #links = navegar_site()
# coleta_dados(links)


# #--------------------------------
# fotos_box = driver.find_element_by_css_selector('.js-slideshow-thumbs.pdp-slideshow-thumbs.flex.cell-mt-22.cell-m-24.flex--align-end').find_elements_by_tag_name('img')
# fotos = ''
# for foto in fotos_box:
#     try:
#         fotos += foto.get_attribute('src').replace('imwidth=50','imwidth=1000') + ', '
#     except:
#         continue
# print(fotos[:-2])