from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from time import sleep
from pathlib import Path
from bs4 import BeautifulSoup
import os
from datetime import date
from datetime import datetime
import json
import pandas as pd
import re
import requests

#TOR
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile

#Diversos
from IPython.display import clear_output
from random import sample

url_homepage = 'https://www.anyany.com.br/'
col_Coletados = ['Data','Link']
col_Lidos = ['Data', 'Categoria','Link']
col_Quebrados = ['Data', 'Link']
col_Produtos = [
            'link produto',
            'departamento',
            'categoria',
            'productId',
            'codigo',
            'sku',
            'nome sku',
            'nome',
            'cor',
            'tamanho',
            'marca',
            'img_links',
            'img_code']
    
    
    

# Data e Hora para o Versionamento
diversao = date.today().strftime('%Y-%m-%d')

# Criação das Pastas
my_path = os.getcwd()
my_path = my_path.replace('\\','/')

# Criação da pasta 'leituras'
pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    pass
    #print ("Diretorio %s já criado" % pathLeituras)
#else:
#    print ("Diretorio %s criado com sucesso " % pathLeituras)

# Criação da pasta 'imagens' 
pathImg = my_path+"/imagens/img "+diversao
try:
    os.makedirs(pathImg)
except OSError:
    #print ("Diretorio %s já criado" % pathImg)
    pass
#else:
    #print ("Diretorio %s criado com sucesso " % pathImg)
    
# Pegar nome do site
name_site = url_homepage.split('.')[1]

# Criação dos nomes dos arquivos
name_file_Produtos = 'produtos_' + str(name_site) + '.csv'

name_file_Coletados = 'coletados_' + str(name_site) + '.csv'  # Dados coletados dos links

name_file_Lidos = 'lidos_' + str(name_site) + '.csv' # Lidos do site

name_file_Quebrados = 'quebrados_' + str(name_site) + '.csv' # Lidos do site

name_file_ProdVer = pathLeituras + '/' + diversao + '_' + 'produtos_' + str(name_site) + '.csv'

# Verificação/Criação do arquivo 'produtos'
fileProdutos = Path(my_path + '/' + name_file_Produtos) 

if fileProdutos.is_file():
    produtos = pd.read_csv(fileProdutos)
else:
    produtos = pd.DataFrame(columns = col_Produtos)
    produtos.to_csv(name_file_Produtos, index = False)
    

# Verificação/Criação do arquivo 'coletados'
fileColetados = Path(my_path + '/' + name_file_Coletados) 

if fileColetados.is_file():
    coletados = pd.read_csv(fileColetados)
else:
    coletados = pd.DataFrame(columns = col_Coletados)
    coletados.to_csv(name_file_Coletados, index = False)

# Verificação/Criação do arquivo 'linkLidos'
fileLidos = Path(my_path + '/' + name_file_Lidos) 

if fileLidos.is_file():
    lidos = pd.read_csv(fileLidos)
else:
    lidos = pd.DataFrame(columns = col_Lidos)
    lidos.to_csv(name_file_Lidos, index = False)
    
# Verificação/Criação do arquivo 'linkLidos'
fileQuebrados = Path(my_path + '/' + name_file_Quebrados) 

if fileQuebrados.is_file():
    quebrados = pd.read_csv(fileQuebrados)
    
else:
    quebrados = pd.DataFrame(columns = col_Quebrados)
    quebrados.to_csv(name_file_Quebrados, index = False)

def carrega_pagina(url, sleep_time = 2):
    driver.get(url)
    #driver.switch_to.window(driver.current_window_handle)
    sleep(sleep_time)
    soup = BeautifulSoup(driver.page_source)
    
    return soup

soup = carrega_pagina(url_homepage)

fileCategorias = Path(my_path + '/' + 'categorias_' + str(name_site) + '.csv') 
if  fileCategorias.is_file():
    my_cats = pd.read_csv(fileCategorias)
else:
    my_cats = pd.DataFrame(columns = ['Categoria','Link'])
    my_cats.to_csv('categorias_' + str(name_site) + '.csv', index = False)

soup = carrega_pagina(url_homepage,2)

col_12 = soup.find('div',{'class' : 'row-3'}).find('div',{'class' : 'col-12 col-sm-12 col-md-12'})
menu = col_12.find_all('li')

for i in menu:
    my_cat_title = i.find('a').text.strip()
    my_cat_link = 'https://www.anyany.com.br' + i.find('a')['href']
    
    if my_cat_link in list(my_cats.Link):
        continue
    else:
        my_cats = pd.read_csv(fileCategorias)
        my_cats = my_cats.append({
            'Categoria' : my_cat_title,
            'Link' : my_cat_link
        }, ignore_index = True)
        
        my_cats.to_csv('categorias_' + str(name_site) + '.csv', index = False)
    
def xpath_soup(element):
    components = []
    child = element if element.name else element.parent
    for parent in child.parents:  # type: bs4.element.Tag
        siblings = parent.find_all(child.name, recursive=False)
        components.append(
            child.name if 1 == len(siblings) else '%s[%d]' % (
                child.name,
                next(i for i, s in enumerate(siblings, 1) if s is child)
                )
            )
        child = parent
    components.reverse()
    return '/%s' % '/'.join(components)

ef pega_produto(categoria, vitrine):
    global lidos

    for vit in vitrine:
        link_produto = vit.find('a')['href']
                    
        if link_produto in list(lidos.Link):
            continue
                        
        else:
            now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                        
            lidos = pd.read_csv(fileLidos)
                        
            lidos = lidos.append({
                'Data' : now,
                'Categoria' : categoria,
                'Link' : link_produto
            }, ignore_index = True)
                        
            lidos.drop_duplicates(subset=['Link'], inplace=True)
            lidos.to_csv(name_file_Lidos, index = False)
                        
    clear_output(wait=True)
    print(categoria)
    print(len(vitrine))
    print('Total de produtos: ' + str(len(lidos)))
    print(lidos.Categoria.value_counts())

for i in range(len(my_cats)):
    my_link = my_cats.Link[i]
    categoria = my_cats.Categoria[i]
    
    try:
        soup = carrega_pagina(my_link)
    except:
        continue
    
    while True:                      
        for _ in range(3):
            driver.execute_script("window.scrollTo(0, window.scrollY + 300)")
            prateleira = soup.find_all('div',{'class' : re.compile('^prateleira n')})
            sleep(1)
            for prat in prateleira:
                vitrine = prat.find_all('li')
                try:
                    pega_produto(categoria, vitrine) 
                except:
                    continue
        try: 
            botao = soup.find('div',{'class' : 'btn-load-more confira-todos-produtos'})
            print(botao)
            driver.find_element_by_xpath(xpath_soup(botao)).click()
            sleep(0.5)
            soup = BeautifulSoup(driver.page_source)
        except:
            print('Fim da categoria!')
            break

def salva_imagens(imagens, prod_code):
    global pathImg
    # Coleta e download da imagem através do dicionário
    count = 1
    img_codes = ''
    for imgLink in imagens:
        response = requests.get(imgLink)
        file = open(pathImg+'/' + str(prod_code) +'_'+str(count)+'.jpg', 'wb')
        file.write(response.content)
        file.close()
        img_codes += str(prod_code) +'_'+str(count)+'.jpg;'      
        count += 1
        sleep(1)
        
    img_codes = img_codes[:-1]
    
    return img_codes

def pega_dado(url):
    global col_Produtos
    info = pd.DataFrame(columns = col_Produtos)
    
    soup = carrega_pagina(url)
    
    codigo = soup.find('div',{'class' : re.compile('^productReference ')}).text
    link_produto = url    
    
    # scripts   
    scripts = soup.find_all('script')
    for scr in scripts:
        if 'var skuJson' in str(scr):
            skuJson = str(scr)
            
        if 'vtex.events.addData' in str(scr):
            addData = str(scr)
            
        if 'vtxctx' in str(scr):
            vtxctx = str(scr)
            
    skuJson = skuJson.split('skuJson_0 = ')[1].split(';CATALOG_SDK')[0]
    skuJson = json.loads(skuJson)
    
    addData = addData.split('.addData(')[1].split(');\n</scr')[0]
    addData = json.loads(addData)
    
    vtxctx = vtxctx.split('">vtxctx = ')[1].split(';</script>')[0]
    vtxctx = json.loads(vtxctx)
    
    # addData e vtxctx
    try:
        productId = addData['productId']
    except:
        productId = soup.find('meta', {'itemprop' : 'productID'})['content']
        
    marca = addData['productBrandName']
    
    try:
        categoria = addData['productCategoryName']
    except:
        categoria = vtxctx['categoryName']
        
    try:
        departamento = addData['productDepartmentName']
    except:
        departamento = vtxctx['departmentName']
            
    # skuJson
    nome = skuJson['name']
    nome = " ".join(nome.split())
    
    # imagens
    li = soup.find('ul',{'class' : 'thumbs'}).find_all('li')
    img_links = ''
    imagens = []
    for l in li:
        try:
            img_links += l.find('a')['rel'][0] + ';'
            imagens.append(l.find('a')['rel'][0])
        except:
            continue
        
    img_code = salva_imagens(imagens, productId)
    
    # dict
    skus = skuJson['skus']
    for s in skus:
        sku = s['sku']
        skuname = s['skuname']
        skuname = " ".join(skuname.split())
        
        tamanho = s['dimensions']['Tamanho']
        cor = s['dimensions']['Cor']
        
        info_dados = {
            'link produto' : url,
            'departamento' : departamento,
            'categoria' : categoria,
            'productId' : productId,
            'codigo' : codigo,
            'sku' : sku,
            'nome sku' : skuname,
            'nome' : nome,
            'cor' : cor,
            'tamanho' : tamanho,
            'marca' : marca,
            'img_links' : img_links,
            'img_code' : img_code
        }
        
        info = info.append(info_dados, ignore_index = True)
    
    return info

# Verificação/Criação do arquivo 'produtos versionados'
fileProdVer = Path(name_file_ProdVer) 

if fileProdVer.is_file():
    prod_versionado = pd.read_csv(name_file_ProdVer)
    
else:
    prod_versionado = pd.DataFrame(columns = col_Produtos)
    prod_versionado.to_csv(name_file_ProdVer, index = False)
    

# Executando
#sam = sample(range(len(lidos)),20)
#count = 1
#for i in sam:
#    print(count)
#    count += 1

for i in range(len(lidos)):
    
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    url_product = lidos.loc[i, 'Link']
    
    if  url_product in list(coletados.Link) or url_product in list(quebrados.Link):
        continue
        
    else:
        print(i)
        print(url_product)
        
    try:
        info_dados = pega_dado(url_product)
    except:       
        quebrados = pd.read_csv(fileQuebrados)
        quebrados = quebrados.append({
            'Data' : now,
            'Link' : url_product
        }, ignore_index = True)
        quebrados.to_csv(name_file_Quebrados, index = False)
        print('Algo de errado aconteceu!')
        continue
        
    coletados = pd.read_csv(fileColetados)
    coletados = coletados.append({
        'Data' : now,
        'Link' : url_product
    }, ignore_index = True)
    coletados.to_csv(name_file_Coletados, index = False)
    
    produtos = pd.read_csv(fileProdutos)
    produtos = produtos.append(info_dados, ignore_index=True)
    produtos.to_csv(name_file_Produtos, index = False)
    
    prod_versionado = pd.read_csv(name_file_ProdVer)
    prod_versionado = prod_versionado.append(info_dados, ignore_index=True)
    prod_versionado.to_csv(name_file_ProdVer, index = False)

link = ['https://www.anyany.com.br/pantufa-bota-fluffy-13-02-0060/p']

for i in link:
    pega_dado(i)

scripts = soup.find_all('script')

for scr in scripts:
    if 'var skuJson' in str(scr):
        skuJson = str(scr)
        
    if 'vtex.events.addData' in str(scr):
        addData = str(scr)
        
    if 'vtxctx' in str(scr):
        vtxctx = str(scr)
        
skuJson = skuJson.split('skuJson_0 = ')[1].split(';CATALOG_SDK')[0]
skuJson = json.loads(skuJson)

addData = addData.split('.addData(')[1].split(');\n</scr')[0]
addData = json.loads(addData)
addData

vtxctx = vtxctx.split('">vtxctx = ')[1].split(';</script>')[0]
vtxctx = json.loads(vtxctx)
vtxctx

vtxctx['departmentName']

soup.find('meta', {'itemprop' : 'productID'})['content']

skus = skuJson['skus']

for s in skus:
    print(s['dimensions']['Tamanho'])

soup.find('div',{'class' : re.compile('^productReference ')}).text

name = skuJson['name']
name = " ".join(name.split())
name

soup = carrega_pagina('https://www.anyany.com.br/pijama-manga-longa-trio-maternidade-lolita-04-01-1665/p')

li = soup.find('ul',{'class' : 'thumbs'}).find_all('li')

img_links = ''
imagens = []
for l in li:
    img_links += l.find('a')['rel'][0] + ';'
    imagens.append(l.find('a')['rel'][0])
    
img_links = img_links[:-1]
imagens
