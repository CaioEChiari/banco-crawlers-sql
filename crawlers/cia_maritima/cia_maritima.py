from libs.crawler_modelo import Crawler_Modelo
from libs.banco_de_dados import Banco_De_Dados
from datetime import datetime
from bs4 import BeautifulSoup
from pathlib import Path
import urllib.parse
import requests
import json

class Cia_Maritima(Crawler_Modelo):
    def pegar_urls_produtos(self):
        lidos = []
        page_number = 1
        while True:
            params = (
                ('O', 'OrderByReleaseDateDESC'),
                ('PS', '48'),
                ('sl', '36c3ab1c-2298-4f43-a098-f6723993d9c4'),
                ('cc', '48'),
                ('sm', '0'),
                ('PageNumber', str(page_number)),
            )

            if page_number == 3:
                break

            response = requests.get('https://www.ciamaritima.com.br/buscapagina', params=params)
            soup = BeautifulSoup(response.text, 'html.parser')
            vitrine = soup.find_all('a', {'class': 'product-more'})
            if len(vitrine) == 0:
                break
            for vit in vitrine:
                link_produto = vit['href']
                if link_produto in lidos:
                    continue
                lidos.append(link_produto)
            page_number += 1
            print(page_number)
            print(len(lidos))
        return lidos

    def pegar_dados_produtos(self, urls, cur):
        for url in urls:
            #try:
            campos_obrigatorios = self.bd.criar_dicionario_campos_obrigatorios()

            campos_adicionais = {}
            campos_obrigatorios['url'] = url
            campos_obrigatorios['data_coleta'] = str(datetime.today())

            if url in self.coletados or url == 'url':
                continue

            response = requests.get(url)
            soup = BeautifulSoup(response.text, 'html.parser')

            scripts = soup.find_all('script')
            for script in scripts:
                if 'vtex.events.addData' in str(script):
                    addData = str(script)
                if 'var skuJson_0' in str(script):
                    skuJson = str(script)

            addData = addData.split('vtex.events.addData(')[1].split(');\n</script')[0]
            d_addData = json.loads(addData)

            try:
                skuJson = skuJson.split('skuJson_0 = ')[1].split(';CATALOG_SD')[0]
                d_skuJson = json.loads(skuJson)
            except:
                continue

            campos_obrigatorios['product_id'] = d_addData['productId']
            campos_obrigatorios['referencia'] = d_addData['productReferenceId']
            campos_obrigatorios['nome'] = d_addData['productName']
            campos_obrigatorios['marca'] = d_addData['productBrandName']
            campos_obrigatorios['descricao'] = soup.find('div', {'class': 'productDescription'}).text.strip()

            eans = d_addData['productEans']
            skus_eans = list(d_addData['skuStocks'].keys())
            skus = d_skuJson['skus']
            
            for i in range(len(eans)):
                campos_obrigatorios['ean'] = eans[i]
                campos_obrigatorios['sku'] = skus_eans[i]
                sku = skus_eans[i]
                for s in skus:
                    if str(sku) == str(s['sku']):
                        campos_obrigatorios['tamanho'] = s['dimensions']['Tamanho'].strip()
                        skuname = s['skuname']
                        campos_obrigatorios['cor'] = skuname.split(' - ')[0]

                        url = f'https://www.ciamaritima.com.br/produto/sku/{sku}'
                        response = requests.get(url)
                        soup = BeautifulSoup(response.text, 'html.parser')

                        img_json = response.json()
                        lis = img_json[0]['Images']
                        imagens = []
                        for li in lis:
                            my_img_link = li[0]['Path']
                            my_img_link = my_img_link.split('/')
                            my_img_link[5] = my_img_link[5].split('-')[0]
                            my_img_link = '/'.join(my_img_link)
                            if my_img_link in imagens:
                                continue
                            else:
                                imagens.append(my_img_link)

                        campos_obrigatorios['imagens'] = ';'.join(imagens)

                self.bd.adicionar_dados_obrigatorios_produto_bd(cur, campos_obrigatorios, campos_adicionais)
            #except:
            #    print('erro')
            #    continue
