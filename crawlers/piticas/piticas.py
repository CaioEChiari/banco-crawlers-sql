from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from pathlib import Path
from time import sleep
import pandas as pd
import requests
import csv
import os

driver = webdriver.Chrome('C:\workspace\curadoria\webdriver\chromedriver.exe')
driver.maximize_window()

driver.get('https://www.piticas.com.br/')

categorias_box = driver.find_elements_by_class_name('menu__wrapper')[1].find_elements_by_class_name('menu__group-item')[1:-1]
categorias_links =[]
for categoria in categorias_box:
    categorias_links.append(categoria.find_element_by_tag_name('a').get_attribute('href'))

produtos_links = []
for categoria_link in categorias_links:
    driver.get(categoria_link)
    sleep(2)
    total_paginas = len(driver.find_element_by_class_name('pagination__items').find_elements_by_tag_name('button'))
    for i in range(1, total_paginas):
        produtos_a = driver.find_element_by_class_name('vitrine__group').find_elements_by_tag_name('a')    
        for produto in produtos_a:
            produto_href = produto.get_attribute('href')
            if produto_href is None:
                continue
            produtos_links.append(produto_href)

        botao_next = driver.find_element_by_css_selector('.pagination__button.pagination__button--next')

        driver.execute_script('arguments[0].scrollIntoView();', botao_next)
        driver.execute_script('window.scrollBy(0, -200)')
        sleep(1)
        botao_next.click()
        sleep(2)

produtos_links = list(set(produtos_links))

for produto in produtos_links:
    driver.get(produto)
    print(produto)
    sleep(1)
    try:
        nome = driver.find_element_by_class_name('productPage__name').text
    except:
        continue
    descricao = driver.find_element_by_class_name('productPage__description--bottom-content').text
    try:
        img_box = driver.find_element_by_css_selector('.productPage__thumbs.js-image-thumbs.slick-initialized.slick-slider').find_elements_by_tag_name('img')
        fotos = ''
        for img in img_box:
            fotos += img.get_attribute('src').replace('150-150', '1000-1000') + ', '
        fotos = fotos[:-2]
    except:
        print('deu ruim na imagem')
        fotos = ''
    metas = driver.find_elements_by_tag_name('meta')
    for meta in metas:
        if meta.get_attribute('itemprop') == "productID":
            product_id = meta.get_attribute('content')
        if meta.get_attribute('itemprop') == "gtin13":
            product_gtin = meta.get_attribute('content')

    #-------------------------------
    headers = {
        'authority': 'www.piticas.com.br',
        'accept': 'application/json',
        'referer': 'https://www.piticas.com.br/camiseta-space-jam-2---pernalonga-' + str(product_id) + '/p',
    }

    response = requests.get('https://www.piticas.com.br/api/catalog_system/pub/products/variations/' + str(product_id), headers=headers)
    response = response.json()

    for i in range(len(response['skus'])):
        sku = response['skus'][i]['sku']
        tamanho = response['skus'][i]['dimensions']['Tamanho']
        cor = response['skus'][i]['dimensions']['Cor']

        my_path = os.getcwd()
        my_path = my_path.replace('\\','/')
        file_coletados = Path(my_path + '/piticas_produtos.csv')
        with open(file_coletados, 'a', newline='') as file:
            writer = csv.writer(file)
            writer.writerow([nome, descricao, fotos, sku, tamanho, cor])


# import pandas as pd
# df = pd.DataFrame(produtos_links)
# df.to_csv('links_encontrados.csv', index=False, header=False, encoding='utf-8')