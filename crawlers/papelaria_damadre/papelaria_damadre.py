from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from time import sleep
from pathlib import Path
from bs4 import BeautifulSoup
import os
from datetime import date
from datetime import datetime
import json
import pandas as pd
import re
import requests

#TOR
from selenium import webdriver
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
import os

#Diversos
from IPython.display import clear_output
from random import sample

url_homepage = 'https://www.papelariadamadre.com.br/'
col_Coletados = ['Data','Link']
col_Lidos = ['Data', 'Categoria','Link']
col_Quebrados = ['Data', 'Link']
col_Produtos = [
        'ean',
        'nome',
        'marca',
        'categoria',
        'subcategoria',
        'imagens link',
        'imagens cod']

# Data e Hora para o Versionamento
diversao = date.today().strftime('%Y-%m-%d')

# Criação das Pastas
my_path = os.getcwd()
my_path = my_path.replace('\\','/')

# Criação da pasta 'leituras'
pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    pass
     
# Criação da pasta 'imagens' 
pathImg = my_path+"/imagens/img "+diversao
try:
    os.makedirs(pathImg)
except OSError:
    pass
     
# Pegar nome do site
name_site = url_homepage.split('.')[1]

# Criação dos nomes dos arquivos
name_file_Produtos = 'produtos_' + str(name_site) + '.csv'
name_file_Coletados = 'coletados_' + str(name_site) + '.csv'  # Dados coletados dos links
name_file_Lidos = 'lidos_' + str(name_site) + '.csv' # Lidos do site
name_file_Quebrados = 'quebrados_' + str(name_site) + '.csv' # Lidos do site
name_file_ProdVer = pathLeituras + '/' + diversao + '_' + 'produtos_' + str(name_site) + '.csv'

# Verificação/Criação do arquivo 'produtos'
fileProdutos = Path(my_path + '/' + name_file_Produtos) 

if fileProdutos.is_file():
    produtos = pd.read_csv(fileProdutos)  
else:
    produtos = pd.DataFrame(columns = col_Produtos)
    produtos.to_csv(name_file_Produtos, index = False)
    
# Verificação/Criação do arquivo 'coletados'
fileColetados = Path(my_path + '/' + name_file_Coletados) 

if fileColetados.is_file():
    coletados = pd.read_csv(fileColetados)
else:
    coletados = pd.DataFrame(columns = col_Coletados)
    coletados.to_csv(name_file_Coletados, index = False)
    
# Verificação/Criação do arquivo 'linkLidos'
fileLidos = Path(my_path + '/' + name_file_Lidos) 
if fileLidos.is_file():
    lidos = pd.read_csv(fileLidos)
else:
    lidos = pd.DataFrame(columns = col_Lidos)
    lidos.to_csv(name_file_Lidos, index = False)
    
# Verificação/Criação do arquivo 'linkLidos'
fileQuebrados = Path(my_path + '/' + name_file_Quebrados) 
if fileQuebrados.is_file():
    quebrados = pd.read_csv(fileQuebrados)
else:
    quebrados = pd.DataFrame(columns = col_Quebrados)
    quebrados.to_csv(name_file_Quebrados, index = False)

torexe = os.popen(r'C:\Users\Artur\Documents\Tor Browser\Browser\TorBrowser\Tor\tor.exe')

profile = FirefoxProfile(r'C:\Users\Artur\Documents\Tor Browser\Browser\TorBrowser\Data\Browser\profile.default')
profile.set_preference('network.proxy.type', 1)
profile.set_preference('network.proxy.socks', '127.0.0.1')
profile.set_preference('network.proxy.socks_port', 9050)
profile.set_preference("network.proxy.socks_remote_dns", False)
profile.update_preferences()

driver = webdriver.Firefox(firefox_profile= profile, executable_path=r'C:\Users\Artur\Documents\Tor Browser\geckodriver.exe')
#driver.get("http://check.torproject.org")
#driver.get('https://whatismyipaddress.com/')
driver.get(url_homepage)
driver.maximize_window()

def carrega_pagina(url, sleep_time = 2):
    driver.get(url)
    driver.switch_to.window(driver.current_window_handle)
    sleep(sleep_time)
    soup = BeautifulSoup(driver.page_source)
    return soup

fileCategorias = Path(my_path + '/' + 'categorias_' + str(name_site) + '.csv') 
if  fileCategorias.is_file():
    my_cats = pd.read_csv(fileCategorias)
else:
    my_cats = pd.DataFrame(columns = ['Categoria','Link'])
    my_cats.to_csv('categorias_' + str(name_site) + '.csv', index = False)

soup = carrega_pagina(url_homepage,2)
menuHeader = soup.find_all('li',{'class' : re.compile('^dropdown menuHeader-li')})

for i in menuHeader:
    try:
        my_cat_title = i.find('a').text
        my_cat_link = i.find('a')['href']
    except:
        continue
    
    if my_cat_link in list(my_cats.Link):
        continue
    else:
        my_cats = pd.read_csv(fileCategorias)
        my_cats = my_cats.append({
            'Categoria' : my_cat_title,
            'Link' : my_cat_link
        }, ignore_index = True)
        
        my_cats.to_csv('categorias_' + str(name_site) + '.csv', index = False)

def pega_produto(categoria, vitrine):
    global lidos   
                
    for vit in vitrine:
        link_produto = vit.find('a')['href']
                    
        if link_produto in list(lidos.Link):
            continue
                        
        else:
            now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                        
            lidos = pd.read_csv(fileLidos)
                        
            lidos = lidos.append({
                'Data' : now,
                'Categoria' : categoria,
                'Link' : link_produto
            }, ignore_index = True)
                        
            lidos.drop_duplicates(subset=['Link'], inplace=True)
            lidos.to_csv(name_file_Lidos, index = False)
                        
    clear_output(wait=True)
    print(categoria)
    print(len(vitrine))
    print('Total de produtos: ' + str(len(lidos)))
    print(lidos.Categoria.value_counts())

# Verificação/Criação do arquivo 'paginas_lidas'
filePaginasLidas = Path(my_path + '/' + 'paginas_lidas_' + str(name_site) + '.csv') 

if  filePaginasLidas.is_file():
    paginas_lidas = pd.read_csv(filePaginasLidas)
else:
    paginas_lidas = pd.DataFrame(columns = ['Data', 'Categoria','Link_Pagina'])
    paginas_lidas.to_csv('paginas_lidas_' + str(name_site) + '.csv', index = False)
    
    
for i in range(len(my_cats)):
    page_count = 1
    while True:        
        my_link = my_cats.Link[i] + 'pagina-' + str(page_count) + '/'
        
        if my_link in list(paginas_lidas.Link_Pagina):
            page_count += 1
            continue
        
        driver.get(my_link)
        sleep(2)
        
        soup = BeautifulSoup(driver.page_source)
        vitrine = soup.find_all('div',{'class' : 'product-thumb-container'})
        if len(vitrine) == 0:
            print('Fim da categoria!')
            break
        
        pega_produto(my_cats.Categoria[i], vitrine)

        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        
        paginas_lidas = pd.read_csv(filePaginasLidas)
        paginas_lidas = paginas_lidas.append({
            'Data' : now,
            'Categoria' : my_cats.Categoria[i],
            'Link_Pagina' : my_link
        }, ignore_index = True)
        paginas_lidas.to_csv('paginas_lidas_' + str(name_site) + '.csv', index = False)
        
        print('Acabou a página!')
        page_count += 1

def salva_imagens(imagens, prod_code):
    global pathImg
    # Coleta e download da imagem através do dicionário
    count = 1
    img_codes = ''
    for imgLink in imagens:
        response = requests.get(imgLink)
        file = open(pathImg+'/' + str(prod_code) +'_'+str(count)+'.jpg', 'wb')
        file.write(response.content)
        file.close()
        img_codes += str(prod_code) +'_'+str(count)+'.jpg;'      
        count += 1
        sleep(0.5)
        
    img_codes = img_codes[:-1]
    
    return img_codes

def pega_dado(url):
    
    soup = carrega_pagina(url)
    
    ean = soup.find('span',{'id' : 'product-id'}).text
    nome = soup.find('span',{'itemprop' : 'name'}).text.strip()
    marca = soup.find('span',{'itemprop' : 'brand'}).text.strip()
    
    li = soup.find('ul',{'class' : 'breadcrumb'}).find_all('li')
    categoria = li[1].text
    subcategoria = li[2].text
    
    imagens = []
    img_links = ''
    
    thumbs = soup.find('div',{'id':'thumbs'})
    img = thumbs.find_all('link',{'itemprop' : 'image'})
    
    if len(img) != 0:
        for i in img:
            imagens.append(i['href'])
            img_links += i['href'] + 'jpg;'
        img_links = img_links[:-1]
        img_codes = salva_imagens(imagens, ean)
    else:
        main_photo = soup.find('div',{'class' : 'main-photo col-xs-12'}).find('a')['href']
        imagens.append(main_photo)
        img_links = main_photo
        img_codes = salva_imagens(imagens, ean)
    
    info_dados = {
        'ean' : ean,
        'nome' : nome,
        'marca' : marca,
        'categoria' : categoria,
        'subcategoria' : subcategoria,
        'imagens link' : img_links,
        'imagens cod' : img_codes
    }
    
    return info_dados

# Verificação/Criação do arquivo 'produtos versionados'
fileProdVer = Path(name_file_ProdVer) 

if fileProdVer.is_file():
    prod_versionado = pd.read_csv(name_file_ProdVer)
    
else:
    prod_versionado = pd.DataFrame(columns = col_Produtos)
    prod_versionado.to_csv(name_file_ProdVer, index = False)
    
sam = sample(range(len(lidos)),20)
# Executando

#count = 1
#for i in sam:
#    print(count)
#    count += 1

for i in range(len(lidos)):
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    url_product = lidos.loc[i, 'Link']
    
    if  url_product in list(coletados.Link) or url_product in list(quebrados.Link):
        continue
        
    else:
        print(i)
        print(url_product)
        
    try:
        info_dados = pega_dado(url_product)
    except:       
        quebrados = pd.read_csv(fileQuebrados)
        quebrados = quebrados.append({
            'Data' : now,
            'Link' : url_product
        }, ignore_index = True)
        quebrados.to_csv(name_file_Quebrados, index = False)
        print('Algo de errado aconteceu!')
        continue
        
    coletados = pd.read_csv(fileColetados)
    coletados = coletados.append({
        'Data' : now,
        'Link' : url_product
    }, ignore_index = True)
    coletados.to_csv(name_file_Coletados, index = False)
    
    produtos = pd.read_csv(fileProdutos)
    produtos = produtos.append(info_dados, ignore_index=True)
    produtos.to_csv(name_file_Produtos, index = False)
    
    prod_versionado = pd.read_csv(name_file_ProdVer)
    prod_versionado = prod_versionado.append(info_dados, ignore_index=True)
    prod_versionado.to_csv(name_file_ProdVer, index = False)