from libs.crawler_modelo import Crawler_Modelo
from libs.banco_de_dados import Banco_De_Dados
from datetime import datetime
from bs4 import BeautifulSoup
from pathlib import Path
import urllib.parse
import requests
import json
import ast


class Puket(Crawler_Modelo):
    
    def pegar_urls_produtos(self):
        try:
            produtos_encontrados = []
            site = 'https://www.puket.com.br'
            response = requests.get(site)
            soup = BeautifulSoup(response.text, 'html.parser')
            menu = soup.findAll('li',{'class':'menuList_link'})
            categorias = []
            for i in range(len(menu)-3):
                link = menu[i].find('h3')
                link = link.find('a')
                try:
                    link = link['href']
                except KeyError:
                    continue
                categorias.append(link)
            
            for categoria in categorias:
                if 'https' not in categoria:
                    urlCat = f'{site}{categoria}?PageNumber='
                    sufix = '&O='
                else:
                    urlCat = f'{categoria}&page='
                    sufix = ''

                page = 1
                while True:
                    response = requests.get(f'{urlCat}{page}{sufix}')
                    soup = BeautifulSoup(response.text, 'html.parser')
                    vitrine = soup.findAll('div',{'class':'project-shelf__item-image'})
                    if len(vitrine) == 0:
                        break
                    for a in vitrine:
                        link = a.find('a')['href']
                        if link not in produtos_encontrados:
                            produtos_encontrados.append(link)
                    page += 1
            return produtos_encontrados
        except:
            self.arq_csv.gravar_erro('erros_urls', url=categoria, crawler=self.pega_nome_classe())
    
    
    def pegar_dados_produtos(self, urls, cur):
                
        for url in urls:
            try:
                r = requests.get(url).text
                soup = BeautifulSoup(r, 'html.parser')
                try:
                    info = r.split("vtex.events.addData(")[1].split(");")[0]
                except IndexError:
                    continue
                if 'http://www.puket.com.br/Sistema/404' in info:
                    continue
                info = ast.literal_eval(info)
                try:
                    eanCode = info['productEans']
                except KeyError:
                    continue
                
                referencia = info['productReferenceId']
                productId = info['productId']
                nome = info['productName']
                marca = info['productBrandName']
                descricao = soup.find('div',{'class':'productDescription'}).text
                            
                imagens = ''
                images = soup.findAll('a',{'id':'botaoZoom'})
                for im in images:
                    imgLink = im.find('img')['src']
                    imagens = f'{imagens}{imgLink};'
                imagens = imagens[:-1]
                
                for ean in eanCode:
                
                    campos_adicionais = {}
                    campos_obrigatorios = self.bd.criar_dicionario_campos_obrigatorios()
                    campos_obrigatorios['url'] = url
                    campos_obrigatorios['data_coleta'] = str(datetime.today())
                    campos_obrigatorios['referencia'] = referencia
                    campos_obrigatorios['product_id'] = productId
                    campos_obrigatorios['ean'] = ean
                    campos_obrigatorios['sku'] = ''
                    campos_obrigatorios['nome_crawler'] = nome
                    campos_obrigatorios['marca'] = marca
                    campos_obrigatorios['tamanho'] = ''
                    campos_obrigatorios['descricao'] = descricao
                    campos_obrigatorios['imagens'] = imagens
                    self.bd.adicionar_dados_obrigatorios_produto_bd(cur, campos_obrigatorios, campos_adicionais)
            except:
                self.arq_csv.gravar_erro('erros_coleta', url=url)
