from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from time import sleep
from pathlib import Path
from bs4 import BeautifulSoup
import os
from datetime import date
from datetime import datetime
import json
import pandas as pd
import re
import requests

#TOR
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile

#Diversos
from IPython.display import clear_output
from random import sample


url_homepage = 'https://br.tommy.com/'
col_Coletados = ['Data','Link']
col_Lidos = ['Data', 'Categoria','Link']
col_Quebrados = ['Data', 'Link']
col_Produtos = [
            'departamento',
            'categoria',
            'productId',
            'referencia',
            'sku',
            'sku_nome',
            'nome',
            'tamanho',
            'cor',
            'origem',
            'material',
            'marca',
            'descricao',
            'img_links',
            'img_codes'
]

# Data e Hora para o Versionamento
diversao = date.today().strftime('%Y-%m-%d')

# Criação das Pastas
my_path = os.getcwd()
my_path = my_path.replace('\\','/')

# Criação da pasta 'leituras'
pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    print ("Diretorio %s já criado" % pathLeituras)
else:
    print ("Diretorio %s criado com sucesso " % pathLeituras)

# Criação da pasta 'imagens' 
pathImg = my_path+"/imagens/img "+diversao
try:
    os.makedirs(pathImg)
except OSError:
    print ("Diretorio %s já criado" % pathImg)
else:
    print ("Diretorio %s criado com sucesso " % pathImg)
    
# Pegar nome do site
name_site = url_homepage.split('.')[1]

# Criação dos nomes dos arquivos
name_file_Produtos = 'produtos_' + str(name_site) + '.csv'

name_file_Coletados = 'coletados_' + str(name_site) + '.csv'  # Dados coletados dos links

name_file_Lidos = 'lidos_' + str(name_site) + '.csv' # Lidos do site

name_file_Quebrados = 'quebrados_' + str(name_site) + '.csv' # Lidos do site

name_file_ProdVer = pathLeituras + '/' + diversao + '_' + 'produtos_' + str(name_site) + '.csv'

# Verificação/Criação do arquivo 'produtos'
fileProdutos = Path(my_path + '/' + name_file_Produtos) 

if fileProdutos.is_file():
    produtos = pd.read_csv(fileProdutos)
else:
    produtos = pd.DataFrame(columns = col_Produtos)
    produtos.to_csv(name_file_Produtos, index = False)
    

# Verificação/Criação do arquivo 'coletados'
fileColetados = Path(my_path + '/' + name_file_Coletados) 

if fileColetados.is_file():
    coletados = pd.read_csv(fileColetados)
else:
    coletados = pd.DataFrame(columns = col_Coletados)
    coletados.to_csv(name_file_Coletados, index = False)

# Verificação/Criação do arquivo 'linkLidos'
fileLidos = Path(my_path + '/' + name_file_Lidos) 

if fileLidos.is_file():
    lidos = pd.read_csv(fileLidos)
else:
    lidos = pd.DataFrame(columns = col_Lidos)
    lidos.to_csv(name_file_Lidos, index = False)
    
# Verificação/Criação do arquivo 'linkLidos'
fileQuebrados = Path(my_path + '/' + name_file_Quebrados) 

if fileQuebrados.is_file():
    quebrados = pd.read_csv(fileQuebrados)
    
else:
    quebrados = pd.DataFrame(columns = col_Quebrados)
    quebrados.to_csv(name_file_Quebrados, index = False)


torexe = os.popen(r'C:\Users\Artur\Documents\Tor Browser\Browser\TorBrowser\Tor\tor.exe')

profile = FirefoxProfile(r'C:\Users\Artur\Documents\Tor Browser\Browser\TorBrowser\Data\Browser\profile.default')
profile.set_preference('network.proxy.type', 1)
profile.set_preference('network.proxy.socks', '127.0.0.1')
profile.set_preference('network.proxy.socks_port', 9050)
profile.set_preference("network.proxy.socks_remote_dns", False)
profile.update_preferences()

driver = webdriver.Firefox(firefox_profile= profile, executable_path=r'C:\Users\Artur\Documents\Tor Browser\geckodriver.exe')
#driver.get("http://check.torproject.org")
#driver.get('https://whatismyipaddress.com/')
driver.get(url_homepage)
driver.maximize_window()

def carrega_pagina(url, sleep_time = 2):
    driver.get(url)
    #driver.switch_to.window(driver.current_window_handle)
    sleep(sleep_time)
    soup = BeautifulSoup(driver.page_source)
    
    return soup

fileCategorias = Path(my_path + '/' + 'categorias_' + str(name_site) + '.csv') 
if  fileCategorias.is_file():
    my_cats = pd.read_csv(fileCategorias)
else:
    my_cats = pd.DataFrame(columns = ['Categoria','Link'])
    my_cats.to_csv('categorias_' + str(name_site) + '.csv', index = False)

soup = carrega_pagina(url_homepage,2)

nivel2 = soup.find_all('div',{'class' : 'menu-nivel2-content'})
for niv in nivel2:
    a = niv.find_all('a')
    for i in a:    
        if i['href'] == '':
            continue
        my_cat_title = i.text.strip()
        my_cat_link = 'https://br.tommy.com' + i['href'].strip()

        if my_cat_link in list(my_cats.Link):
            continue
        else:
            my_cats = pd.read_csv(fileCategorias)
            my_cats = my_cats.append({
                'Categoria' : my_cat_title,
                'Link' : my_cat_link
            }, ignore_index = True)

            my_cats.to_csv('categorias_' + str(name_site) + '.csv', index = False)


def xpath_soup(element):
    components = []
    child = element if element.name else element.parent
    for parent in child.parents:  # type: bs4.element.Tag
        siblings = parent.find_all(child.name, recursive=False)
        components.append(
            child.name if 1 == len(siblings) else '%s[%d]' % (
                child.name,
                next(i for i, s in enumerate(siblings, 1) if s is child)
                )
            )
        child = parent
    components.reverse()
    return '/%s' % '/'.join(components)


def pega_produto(categoria, vitrine):
    global lidos   

    for vit in vitrine:
        link_produto = vit.find('a')['href']
                    
        if link_produto in list(lidos.Link):
            continue
                        
        else:
            now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                        
            lidos = pd.read_csv(fileLidos)
                        
            lidos = lidos.append({
                'Data' : now,
                'Categoria' : categoria,
                'Link' : link_produto
            }, ignore_index = True)
                        
            lidos.drop_duplicates(subset=['Link'], inplace=True)
            lidos.to_csv(name_file_Lidos, index = False)
                        
    clear_output(wait=True)
    print(categoria)
    print(len(vitrine))
    print('Total de produtos: ' + str(len(lidos)))
    print(lidos.Categoria.value_counts())


# Verificação/Criação do arquivo 'paginas_lidas'
filePaginasLidas = Path(my_path + '/' + 'paginas_lidas_' + str(name_site) + '.csv') 

if  filePaginasLidas.is_file():
    paginas_lidas = pd.read_csv(filePaginasLidas)
else:
    paginas_lidas = pd.DataFrame(columns = ['Data', 'Categoria','Link'])
    paginas_lidas.to_csv('paginas_lidas_' + str(name_site) + '.csv', index = False)
    
    
for i in range(len(my_cats)):
    my_link = my_cats.Link[i]
    categoria = my_cats.Categoria[i]
    
    if my_link in list(paginas_lidas.Link):
        continue
    
    soup = carrega_pagina(my_link)
    while True:        
        for _ in range(3):
            driver.execute_script("window.scrollTo(0, window.scrollY + 500)")
            sleep(0.8)
            
            prateleira = soup.find_all('div',{'class' : re.compile('^prateleira vitrine n')})
            for prat in prateleira:
                vitrine = soup.find_all('h3',{'class' : 'product-name'})
                pega_produto(categoria, vitrine)
                
        try:
            botao = soup.find('button',{'class' : 'categoria-load-more'})
            driver.find_element_by_xpath(xpath_soup(botao)).click()
            sleep(1)
            soup = BeautifulSoup(driver.page_source)

            if 'inactive' in str(botao):
                break
        except:
            break


    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    paginas_lidas = pd.read_csv(filePaginasLidas)
    paginas_lidas = paginas_lidas.append({
        'Data' : now,
        'Categoria' : my_cats.Categoria[i],
        'Link' : my_link
    }, ignore_index = True)
    paginas_lidas.to_csv('paginas_lidas_' + str(name_site) + '.csv', index = False)
        
    print('Acabou a categoria!')


def salva_imagens(imagens, prod_code):
    global pathImg
    # Coleta e download da imagem através do dicionário
    count = 1
    img_codes = ''
    for imgLink in imagens:
        response = requests.get(imgLink)
        file = open(pathImg+'/' + str(prod_code) +'_'+str(count)+'.jpg', 'wb')
        file.write(response.content)
        file.close()
        img_codes += str(prod_code) +'_'+str(count)+'.jpg;'      
        count += 1
        sleep(1)
        
    img_codes = img_codes[:-1]
    
    return img_codes


def pega_dado(url):
    global col_Produtos
    info = pd.DataFrame(columns = col_Produtos)
    
    soup = carrega_pagina(url)
    
    try:
        origem = soup.find('td',{'class' : 'value-field Origem'}).text
    except:
        origem = ''
    
    try:
        material = soup.find('td',{'class' : 'value-field Material'}).text
    except:
        material = ''
    
    cor = soup.find('span',{'class' : 'current-color'}).text
    marca = soup.find('div',{'id' : 'brand'}).text
    descricao = soup.find('div',{'class' : 'productDescription'}).text.replace('\n','')
    
    script = soup.find_all('script')
    for scr in script:
        if 'var skuJson_0' in str(scr):
            skuJson = str(scr)

        if 'vtex.events.addData' in str(scr):
            vtex = str(scr)

        if 'vtxctx =' in str(scr):
            vtxctx = str(scr)

    skuJson = skuJson.split('skuJson_0 = ')[1].split(';CATALOG_SDK')[0]
    d = json.loads(skuJson)

    vtex = vtex.split('vtex.events.addData(')[1].split(');\n<')[0]
    v = json.loads(vtex)

    vtxctx = vtxctx.split('vtxctx = ')[1].split(';</script')[0]
    tx = json.loads(vtxctx)
    
    productId = d['productId']
    nome = d['name']
    
    referencia = v['productReferenceId']
    departamento = v['pageDepartment']
    
    categoria = tx['categoryName']
    
    thumbs_cloned = soup.find('ul',{'class' : 'thumbs-cloned'})
    li = thumbs_cloned.find_all('li')
    img_links = ''
    imagens = []
    for tc in li:
        imglnk = tc.find('img')['data-zoom-image'].split('?')[0]
        img_links += str(imglnk) + ';'
        imagens.append(imglnk)
    img_links = img_links[:-1]
    
    img_codes = salva_imagens(imagens, referencia)
    
    for D in d['skus']:
        skuname = D['skuname']
        sku = D['sku']
        tamanho = D['dimensions']['Tamanho']
        
        info_dados = {
            'departamento' : departamento,
            'categoria' : categoria,
            'productId' : productId,
            'referencia' : referencia,
            'sku' : sku,
            'sku_nome' : skuname,
            'nome' : nome,
            'tamanho' : tamanho,
            'cor' : cor,
            'origem' : origem,
            'material' : material,
            'marca' : marca,
            'descricao' : descricao,
            'img_links' : img_links,
            'img_codes' : img_codes
        }

        info = info.append(info_dados, ignore_index = True)
        
    return info


# Verificação/Criação do arquivo 'produtos versionados'
fileProdVer = Path(name_file_ProdVer) 

if fileProdVer.is_file():
    prod_versionado = pd.read_csv(name_file_ProdVer)
else:
    prod_versionado = pd.DataFrame(columns = col_Produtos)
    prod_versionado.to_csv(name_file_ProdVer, index = False)
    

# Executando
#sam = sample(range(len(lidos)),20)
#count = 1
#for i in sam:
#    print(count)
#    count += 1

for i in range(len(lidos)):
    
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    url_product = lidos.loc[i, 'Link']
    
    if  url_product in list(coletados.Link) or url_product in list(quebrados.Link):
        continue
        
    else:
        print(i)
        print(url_product)
        
    try:
        info_dados = pega_dado(url_product)
    except:       
        quebrados = pd.read_csv(fileQuebrados)
        quebrados = quebrados.append({
            'Data' : now,
            'Link' : url_product
        }, ignore_index = True)
        quebrados.to_csv(name_file_Quebrados, index = False)
        print('Algo de errado aconteceu!')
        continue
        
    coletados = pd.read_csv(fileColetados)
    coletados = coletados.append({
        'Data' : now,
        'Link' : url_product
    }, ignore_index = True)
    coletados.to_csv(name_file_Coletados, index = False)
    
    produtos = pd.read_csv(fileProdutos)
    produtos = produtos.append(info_dados)
    produtos.to_csv(name_file_Produtos, index = False)
    
    prod_versionado = pd.read_csv(name_file_ProdVer)
    prod_versionado = prod_versionado.append(info_dados, ignore_index=True)
    prod_versionado.to_csv(name_file_ProdVer, index = False)