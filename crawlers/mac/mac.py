from libs.crawler_modelo import Crawler_Modelo
from libs.banco_de_dados import Banco_De_Dados
from datetime import datetime
from bs4 import BeautifulSoup
from pathlib import Path
import urllib.parse
import requests
import json

class Mac(Crawler_Modelo):
    def pegar_urls_produtos(self):
        try:
            site = 'https://www.maccosmetics.com.br'
            page = requests.get(site)
            soup = BeautifulSoup(page.text, 'html.parser')
            categorias_a = soup.find('div', {'class':'site-header-formatter__sections-main-links'}).find_all('a', {'class':'gnav-link'})
            categorias_links = []
            produtos_encontrados = []
            for a in categorias_a:
                categorias_links.extend([a.get('href')])
            for categoria in categorias_links:
                produtos_a = []
                page = requests.get(site + categoria)
                soup = BeautifulSoup(page.text, 'html.parser')
                print(categoria.split('/')[-1])
                produtos_a.extend(soup.find_all('a', {'class':'product-brief__image-link js-product__link-to-spp js-product-image'}))
                for a in produtos_a:
                    a_href = site + a.get('href')
                    if a_href not in produtos_encontrados:
                        produtos_encontrados.append(a_href)
        except:
            self.arq_csv.gravar_erro('erros_urls', url=categoria, crawler=self.pega_nome_classe())
        return produtos_encontrados

    def pegar_dados_produtos(self, urls, cur):
        site = 'https://www.maccosmetics.com.br'
        if type(urls) is not list:
            urls = [urls]
        for url in urls:
            try:
                if url in self.coletados or url == 'url':
                    continue
                page = requests.get(url)
                soup = BeautifulSoup(page.text, 'html.parser')
                try:
                    headers = {
                        'authority': 'www.maccosmetics.com.br',
                        'referer': url,
                    }
                    response = requests.get(url, headers=headers)
                except:
                    headers = {
                        'authority': 'www.maccosmetics.com.br',
                        'referer': url[:-len(url.split('/')[-1])] + urllib.parse.quote(url.split('/')[-1]),
                    }
                    response = requests.get(url[:-len(url.split('/')[-1])] + urllib.parse.quote(url.split('/')[-1]), headers=headers)
                soup2 = BeautifulSoup(response.text, 'html.parser')
                soup2_json = json.loads(str(soup2.find('script', {'id':'page_data'}))[47:-9])
                variacoes = len(soup2_json['consolidated-products']['products'][0]['skus'])
                for i in range(variacoes):
                    campos_obrigatorios = self.bd.criar_dicionario_campos_obrigatorios()
                    campos_adicionais = {}
                    try:
                        soup.find('h1', {'class':'product-full__name'}).text
                    except:
                        continue
                    campos_obrigatorios['codigo_match'] = soup2_json['consolidated-products']['products'][0]['skus'][i]['UPC_CODE']
                    campos_obrigatorios['marca'] = 'Mac'
                    campos_obrigatorios['nome'] = soup2_json['consolidated-products']['products'][0]['PROD_RGN_NAME']
                    campos_obrigatorios['cor'] = soup2_json['consolidated-products']['products'][0]['skus'][i]['SHADENAME']
                    campos_obrigatorios['descricao'] = soup2_json['consolidated-products']['products'][0]['DESCRIPTION']
                    imagens = soup2_json['consolidated-products']['products'][0]['skus'][i]['LARGE_IMAGE']
                    campos_obrigatorios['imagens'] = []
                    for imagem in imagens:
                        campos_obrigatorios['imagens'].append(site + imagem)
                    campos_obrigatorios['imagens'] = ', '.join(campos_obrigatorios['imagens'])
                    campos_obrigatorios['data_coleta'] = str(datetime.today())
                    campos_obrigatorios['url'] = url
                    campos_obrigatorios['codigo_interno'] = self.gerar_codigo_interno(campos_obrigatorios['marca'], campos_obrigatorios['codigo_match'])
                    self.bd.adicionar_dados_obrigatorios_produto_bd(cur, campos_obrigatorios, campos_adicionais)
            except:
                self.arq_csv.gravar_erro('erros_coleta', url=url)