from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium import webdriver
from datetime import datetime
from bs4 import BeautifulSoup
from time import sleep
from pathlib import Path
import requests
import os
import csv

my_path = os.getcwd()
my_path = my_path.replace('\\','/')

now = datetime.now()
now = str(now.year)+'-'+str(now.month)+'-'+str(now.day)

fileProducts = Path(my_path+'/produtos.csv')

#Verificação de coletas anteriores e criação de novo csv
if fileProducts.is_file():
    with open(my_path+'/produtos.csv', 'r', encoding="Latin1") as f:
        listaProdutos = list(csv.reader(f))
        for i in range(len(listaProdutos)):
            listaProdutos[i] = str(listaProdutos[i][0])
        print('Encontrados '+ str(len(listaProdutos)-1)+' produtos em arquivo')
        sleep(0.2)
else:
    with open(my_path+'/produtos.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['Código', 'EAN','Título','Descrição','Tamanho','Preço', 'Imagens'])

#Verificação de coleta realizada no dia ou criação de novo arquivo de coleta
fileProducts = Path(my_path+'/leituras/produtos'+now+'.csv')
if fileProducts.is_file():
    with open(my_path+'/leituras/produtos'+now+'.csv', 'r', encoding="Latin1") as f:
        listaProdutos = list(csv.reader(f))
        for i in range(len(listaProdutos)):
            listaProdutos[i] = str(listaProdutos[i][0])
        print('Encontrados '+ str(len(listaProdutos)-1)+' já lidos hoje')
        sleep(0.2)
else:
    with open(my_path+'/leituras/produtos'+now+'.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['Código', 'EAN','Título','Descrição','Tamanho','Preço', 'Imagens'])

#Inicialização dos serviços e navegador
service = Service('C:/Program Files/Google/Chrome/Application/chromedriver')
service.start()
driver = webdriver.Remote(service.service_url)
driver.get('https://www.jorgebischoff.com.br/')
coletados = []
produtos = []
linksCapturados = []

def xpath_soup(element):
    components = []
    child = element if element.name else element.parent 
    for parent in child.parents:  # type: bs4.element.Tag
        siblings = parent.find_all(child.name, recursive=False)
        components.append(
            child.name if 1 == len(siblings) else '%s[%d]' % (
                child.name,
                next(i for i, s in enumerate(siblings, 1) if s is child)
                )
            )
        child = parent
    components.reverse()
    return '/%s' % '/'.join(components)


#Atribuição dos itens já coletados na variável
try:
    with open(my_path+'/linksColetados.csv', 'r') as l:
        coletados = list(csv.reader(l))
        for i in range(len(coletados)):
            coletados[i] = str(coletados[i][0])
        print('Encontrados '+ str(len(coletados))+' links já coletados em arquivo')
        sleep(0.2)
except:      
    print('Dados anteriores não encontrados')   
                                #driver.findElement(By.xpath("//div[contains(text(),'"+expectedText+"')]")).click();

ul = driver.find_elements_by_xpath('./html/body/header/div[4]/div/div/nav/ul/li')
a = []
for i in range(len(ul)):
    a.append(ul[i].find_element_by_tag_name('a').get_attribute('href'))

posURL = '&pagina='
for link in a:
    url = link+posURL
    cont = 1
    
    while True:
        driver.get(url+str(cont))
        sleep(1)
        vitrine = driver.find_elements_by_xpath('./html/body/div[5]/div[1]/div[2]/div[3]/ul/li')
        if len(vitrine) == 0:
            break
        else:
            cont += 1
            linksCapturados = []
            for j in range(len(vitrine)):
                linksCapturados.append(vitrine[j].find_element_by_tag_name('a').get_attribute('href'))
            sleep(0.5)
            try:
                with open(my_path+'/linksLidos.csv', 'r') as f:
                    listaCapturados = list(csv.reader(f))
                    for j in range(len(listaCapturados)):
                        listaCapturados[j] = str(listaCapturados[j])
                print('Já foram armazenados '+ str(len(listaCapturados))+' links para coleta')
                sleep(0.2)
            except:
                print('Nenhum link separado para coleta ainda.')
            for l in linksCapturados:
                if l in produtos:
                    continue
                else:
                    produtos.append(l)
                    with open(my_path+'/linksLidos.csv', 'a', newline='') as f:
                        writer = csv.writer(f)
                        writer.writerow([l])

prodN = 1
for produto in produtos:
    if produto in coletados:
        print('Produto [' + produto +'] encontrado em arquivo!')
        prodN += 1
        continue
    
    driver.get(produto)
    sleep(2)
    
    soup = BeautifulSoup(driver.page_source)
    ulTam = soup.find("ul", {"id": "ctrEscolheTamanho"})

    tamList = driver.find_element_by_xpath(xpath_soup(ulTam)).find_elements_by_tag_name('a')
    
    codigo = driver.find_element_by_xpath('/html/body/section/div/div[2]/span').text
    codigo = codigo.split('cód: ')[1]
    
    titulo = driver.find_element_by_xpath('/html/body/section/div/div[2]/h1').text
    print(str(prodN),'-',titulo)
    
    preco = driver.find_element_by_xpath('/html/body/section/div/div[5]/div[3]/div[2]').text
    
    descricao = driver.find_element_by_xpath('/html/body/section/div/div[7]').text
    descricao = descricao.split('SAIBA MAIS\n')[1]
    try:
        dimensoes = descricao.split('Dimensões (LxAxP): ')[1]
        descricao = descricao.split('Dimensões (LxAxP): ')[0]
        dimensoes = dimensoes.split('.\n')[0]
    except:
        dimensoes = ''
    descricao = descricao.split('Dimensões (LxAxP): ')[0]
    tam = []
    ean = []
    for i in range(len(tamList)):
        ean.append(tamList[i].get_attribute('ean'))
        tam.append(tamList[i].get_attribute('medidas'))
    
    ulImg = driver.find_element_by_xpath('/html/body/section/div/div[3]/div/div/ul').find_elements_by_tag_name('a')
    imgLinks = []
    for i in range(len(ulImg)):
        img = ulImg[i].get_attribute('urlfoto')
        if img == None:
            continue
        else:
            imgLinks.append(img)
    
    cont = 0
    imagem = ''
    num = ''
    for l in range(len(imgLinks)):
        if cont > 0:
            num = '_' + str(cont)
        response = requests.get(imgLinks[l])
        file = open('imagens/' + codigo + num + '.jpg', 'wb')
        file.write(response.content)
        file.close()
        imagem += codigo + num + '.jpg, '
        sleep(0.5)
        cont += 1



    #Salvar produto na variável de coletados
    coletados.append(produto)
    prodN += 1
    #Salvar dados no arquivo
    for i in range(len(ean)):
        with open(my_path+'/produtos.csv', 'a', newline='') as f:
            writer = csv.writer(f)
            #               ['Código', 'EAN','Título','Descrição','Tamanho','Preço', 'Imagens']
            writer.writerow([codigo, ean[i], titulo, descricao, tam[i], preco, imagem])
            
        with open(my_path+'/leituras/produtos'+now+'.csv', 'a', newline='') as f:
            writer = csv.writer(f)
            writer.writerow([codigo, ean[i], titulo, descricao, tam[i], preco, imagem])

    #Salvar link na lista de produtos já coletados
    with open(my_path+'/linksColetados.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow([produto])
