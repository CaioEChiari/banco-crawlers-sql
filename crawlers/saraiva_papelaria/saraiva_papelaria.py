from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from time import sleep
from pathlib import Path
from bs4 import BeautifulSoup
import os
from datetime import date
from datetime import datetime
import json
import pandas as pd
import re
import requests

#TOR
from selenium import webdriver
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
import os

#Diversos
from IPython.display import clear_output

from random import sample


url_homepage = 'https://www.saraiva.com.br/papelaria'
col_Coletados = ['Data','Link']
col_Lidos = ['Data', 'Link']
col_Quebrados = ['Data', 'Link']
col_Produtos = [
        'categoria_pag',
        'departamento_pag',
        'ean',
        'sku',
        'produto_id',
        'nome',
        'marca_id',
        'marca',
        'departamento_id',
        'departamento',
        'categoria_id',
        'categoria',
        'descricao',
        'codigo_imagens',
        'links_imagens']

# Data e Hora para o Versionamento
diversao = date.today().strftime('%Y-%m-%d')

# Criação das Pastas
my_path = os.getcwd()
my_path = my_path.replace('\\','/')

# Criação da pasta 'leituras'
pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    print ("Diretorio %s já criado" % pathLeituras)
else:
    print ("Diretorio %s criado com sucesso " % pathLeituras)
            
# Criação da pasta 'imagens' 
pathImg = my_path+"/imagens/img "+diversao
try:
    os.makedirs(pathImg)
except OSError:
    print ("Diretorio %s já criado" % pathImg)
else:
    print ("Diretorio %s criado com sucesso " % pathImg)
    
# Pegar nome do site
name_site = url_homepage.split('.')[1]

# Criação dos nomes dos arquivos
name_file_Produtos = 'produtos_' + str(name_site) + '.csv'
name_file_Coletados = 'coletados_' + str(name_site) + '.csv'  # Dados coletados dos links
name_file_Lidos = 'lidos_' + str(name_site) + '.csv' # Lidos do site
name_file_Quebrados = 'quebrados_' + str(name_site) + '.csv' # Lidos do site
name_file_ProdVer = pathLeituras + '/' + diversao + '_' + 'produtos_' + str(name_site) + '.csv'

# Verificação/Criação do arquivo 'produtos'
fileProdutos = Path(my_path + '/' + name_file_Produtos) 

if fileProdutos.is_file():
    produtos = pd.read_csv(fileProdutos)
    
else:
    produtos = pd.DataFrame(columns = col_Produtos)
    produtos.to_csv(name_file_Produtos, index = False)
    
# Verificação/Criação do arquivo 'coletados'
fileColetados = Path(my_path + '/' + name_file_Coletados) 

if fileColetados.is_file():
    coletados = pd.read_csv(fileColetados)
    
else:
    coletados = pd.DataFrame(columns = col_Coletados)
    coletados.to_csv(name_file_Coletados, index = False)
     
# Verificação/Criação do arquivo 'linkLidos'
fileLidos = Path(my_path + '/' + name_file_Lidos) 

if fileLidos.is_file():
    lidos = pd.read_csv(fileLidos)
    
else:
    lidos = pd.DataFrame(columns = col_Lidos)
    lidos.to_csv(name_file_Lidos, index = False)
    
# Verificação/Criação do arquivo 'linkLidos'
fileQuebrados = Path(my_path + '/' + name_file_Quebrados) 

if fileQuebrados.is_file():
    quebrados = pd.read_csv(fileQuebrados)
    
else:
    quebrados = pd.DataFrame(columns = col_Quebrados)
    quebrados.to_csv(name_file_Quebrados, index = False)

torexe = os.popen(r'C:\Users\Artur\Documents\Tor Browser\Browser\TorBrowser\Tor\tor.exe')

profile = FirefoxProfile(r'C:\Users\Artur\Documents\Tor Browser\Browser\TorBrowser\Data\Browser\profile.default')
profile.set_preference('network.proxy.type', 1)
profile.set_preference('network.proxy.socks', '127.0.0.1')
profile.set_preference('network.proxy.socks_port', 9050)
profile.set_preference("network.proxy.socks_remote_dns", False)
profile.update_preferences()
driver = webdriver.Firefox(firefox_profile= profile, executable_path=r'C:\Users\Artur\Documents\Tor Browser\geckodriver.exe')
#driver.get("http://check.torproject.org")
#driver.get('https://whatismyipaddress.com/')
driver.get(url_homepage)
driver.maximize_window()

def carrega_pagina(url, sleep_time):
    driver.get(url)
    driver.switch_to.window(driver.current_window_handle)
    
    sleep(sleep_time)
    
    soup = BeautifulSoup(driver.page_source)
    
    return soup

def xpath_soup(element):
    components = []
    child = element if element.name else element.parent
    for parent in child.parents:  # type: bs4.element.Tag
        siblings = parent.find_all(child.name, recursive=False)
        components.append(
            child.name if 1 == len(siblings) else '%s[%d]' % (
                child.name,
                next(i for i, s in enumerate(siblings, 1) if s is child)
                )
            )
        child = parent
    components.reverse()
    return '/%s' % '/'.join(components)

soup = carrega_pagina('https://www.saraiva.com.br/papelaria',2)
li = soup.find_all('li',{'class' : re.compile('^btn btn--light icon-')})

fileCategorias = Path(my_path + '/' + 'categorias_' + str(name_site) + '.csv')

if  fileCategorias.is_file():
    my_cats = pd.read_csv(fileCategorias)
else:
    my_cats = pd.DataFrame(columns = ['Categoria','Link'])
    my_cats.to_csv('categorias_' + str(name_site) + '.csv', index = False)
    
soup = carrega_pagina(url_homepage,2)

li = soup.find_all('li',{'class' : re.compile('^btn btn--light icon-')})
    
for i in range(len(li)):
    my_cat_title = li[i].text
    my_cat_link = 'https://www.saraiva.com.br' + li[i].find('a')['href']
    
    if my_cat_link in list(my_cats.Link):
        continue
    else:
        my_cats = pd.read_csv(fileCategorias)
        my_cats = my_cats.append({
            'Categoria' : my_cat_title,
            'Link' : my_cat_link
        }, ignore_index = True)
        my_cats.to_csv('categorias_' + str(name_site) + '.csv', index = False)

def pega_produto(categoria):
    global lidos   

    soup = BeautifulSoup(driver.page_source)

    vitrine = soup.find_all('li',{'layout' : 'a2f406a3-3b36-4875-89c6-a213e7c5ca27'})
                
    for vit in vitrine:
        link_produto = vit.find('h3',{'class' : 'product__title limit-block'}).find('a')['href']
                    
        if link_produto in list(lidos.Link):
            continue
                        
        else:
            now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                        
            lidos = pd.read_csv(fileLidos)
                        
            lidos = lidos.append({
                'Data' : now,
                'Categoria' : categoria,
                'Link' : link_produto
            }, ignore_index = True)
                        
            lidos.drop_duplicates(subset=['Link'], inplace=True)
            lidos.to_csv(name_file_Lidos, index = False)
                        
    clear_output(wait=True)
    print(categoria)
    print(len(vitrine))
    print('Total de produtos: ' + str(len(lidos)))
    print(lidos.Categoria.value_counts())

for i in range(len(my_cats)):
    my_link = my_cats.Link[i]
    
    soup = carrega_pagina(my_link, 2)
    
    while True:
        pega_produto(my_cats.Categoria[i])
        try:
            carregar_mais = soup.find('a', {'class' : 'vejamais-vtexresearch btn btn--outline'})
            driver.find_element_by_xpath(xpath_soup(carregar_mais)).click()
            sleep(2)
        except:
            print('Fim da categoria!')
            break

def salva_imagens(imagens, prod_code):
    global pathImg
    # Coleta e download da imagem através do dicionário
    count = 1
    img_codes = ''
    for i in imagens:
        imgLink = i
        response = requests.get(imgLink)
        file = open(pathImg+'/' + str(prod_code) +'_'+str(count)+'.jpg', 'wb')
        file.write(response.content)
        file.close()
        img_codes += str(prod_code) +'_'+str(count)+'.jpg;'
        count += 1
        sleep(0.5)
        
    img_codes = img_codes[:-1] # Para tirar o último ';'   
    return img_codes

def pega_dado(url):
    
    soup = carrega_pagina(url,2)
    
    vtex_events = soup.find_all('script')
    for i in vtex_events:
        if 'vtex.events.addData' in str(i):
            vtex = str(i).split('.addData(')[1].split(');')[0]
    vtex_dict = json.loads(vtex)
    
    ean = vtex_dict['productEans'][0]
    
    sku = soup.find('span',{'id' : 'sku'}).text
    descricao = soup.find('div', {'id' : 'descricao'}).text.strip()
    
    nome = vtex_dict['productName']
    produto_id = vtex_dict['productId']
    marca = vtex_dict['productBrandName']
    marca_id = vtex_dict['productBrandId']
    departamento = vtex_dict['productDepartmentName']
    departamento_id = vtex_dict['productDepartmentId']
    categoria = vtex_dict['productCategoryName']
    categoria_id = vtex_dict['productCategoryId']
    
    categoria_pag = vtex_dict['pageCategory']
    departamento_pag = vtex_dict['pageDepartment']
    
    # imagens
    imagens = []
    img_links = ''
    
    nav_item = soup.find_all('li',{'class' : 'nav-item'})

    for i in nav_item:
        try:
            my_link = i.find('img')['src'].split('?')[0]
            imagens.append(i.find('img')['src'].split('?')[0])
            img_links += my_link + ';'
        except:
            continue

    img_links = img_links[:-1]

    img_codes = salva_imagens(imagens, ean)
    
    my_dict = {
        'categoria_pag' : categoria_pag,
        'departamento_pag' : departamento_pag,
        'ean' : ean,
        'sku' : sku,
        'produto_id' : produto_id,
        'nome' : nome,
        'marca_id' : marca_id,
        'marca' : marca,
        'departamento_id' : departamento_id,
        'departamento' : departamento,
        'categoria_id' : categoria_id,
        'categoria' : categoria,
        'descricao' : descricao,
        'codigo_imagens' : img_codes,
        'links_imagens' : img_links
    }
    
    return my_dict

# Verificação/Criação do arquivo 'produtos versionados'
fileProdVer = Path(name_file_ProdVer) 

if fileProdVer.is_file():
    prod_versionado = pd.read_csv(name_file_ProdVer)
    
else:
    prod_versionado = pd.DataFrame(columns = col_Produtos)
    prod_versionado.to_csv(name_file_ProdVer, index = False)
    
sam = sample(range(len(lidos)),10)
# Executando

#count = 1
#for i in sam:
#    print(count)
#    count += 1

for i in range(len(lidos)):
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    url_product = lidos.loc[i, 'Link']
    
    if  url_product in list(coletados.Link) or url_product in list(quebrados.Link):
        continue
        
    else:
        print(i)
        print(url_product)
        
    try:
        info_dados = pega_dado(url_product)
    except:       
        quebrados = pd.read_csv(fileQuebrados)
        quebrados = quebrados.append({
            'Data' : now,
            'Link' : url_product
        }, ignore_index = True)
        quebrados.to_csv(name_file_Quebrados, index = False)
        print('Algo de errado aconteceu!')
        continue
        
    coletados = pd.read_csv(fileColetados)
    coletados = coletados.append({
        'Data' : now,
        'Link' : url_product
    }, ignore_index = True)
    coletados.to_csv(name_file_Coletados, index = False)
    
    produtos = pd.read_csv(fileProdutos)
    produtos = produtos.append(info_dados, ignore_index=True)
    produtos.to_csv(name_file_Produtos, index = False)
    
    prod_versionado = pd.read_csv(name_file_ProdVer)
    prod_versionado = prod_versionado.append(info_dados, ignore_index=True)
    prod_versionado.to_csv(name_file_ProdVer, index = False)