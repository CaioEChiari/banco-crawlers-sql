from libs.crawler_modelo import Crawler_Modelo
from urllib.parse import unquote
from datetime import datetime
from bs4 import BeautifulSoup
from pathlib import Path
import requests

class Havaianas(Crawler_Modelo):
    def pegar_urls_produtos(self):
        try:
            site = 'https://havaianas.com.br'
            page = requests.get(site + '/comprar')
            soup = BeautifulSoup(page.text, 'html.parser')
            categorias = []
            produtos_encontrados = []
            cadegorias_box = soup.find('div', {'class': 'refinements'}).find_all('button')
            for categoria in cadegorias_box:
                link = categoria.get('data-href')
                if link is not None:
                    categorias.append(link)
            passo_tamanho = 100
            for categoria in categorias:
                page = requests.get(site + categoria)
                soup = BeautifulSoup(page.text, 'html.parser')
                headers = {
                    'authority': 'havaianas.com.br',
                    'referer': 'https://havaianas.com.br' + categoria,
                }
                total_resultados = int(soup.find('div', {'class': 'shopmode__header shopmode__header-search matrixTxtColor'}).find('span').text.replace('\n','').replace('.','').replace(' Resultados',''))
                cgid = soup.find('input', {'id': 'refinebar-url'}).get('data-url').split('cgid=')[-1]
                cgid = self.corrigir_encode_texto(cgid)
                print()
                print(cgid)
                print(total_resultados)
                passos = round(total_resultados / passo_tamanho) + 1
                for i in range(passos):
                    inicio = i * passo_tamanho
                    fim = (i * passo_tamanho) + passo_tamanho
                    print(inicio, '-', fim)
                    params = (
                        ('cgid', cgid),
                        ('start', inicio),
                        ('sz', fim),
                    )
                    response = requests.get('https://havaianas.com.br/on/demandware.store/Sites-Havaianas-BR-Site/pt_BR/Search-UpdateGrid', headers=headers, params=params)
                    soup = BeautifulSoup(response.text, 'html.parser')
                    for link in soup.find_all('a'):
                        a_href = site + link.get('href')
                        if a_href in produtos_encontrados or a_href == site + '#':
                            continue
                        produtos_encontrados.append(a_href)
        except:
            self.arq_csv.gravar_erro('erros_urls', url=categoria, crawler=self.pega_nome_classe())
        return produtos_encontrados

    def pegar_dados_produtos(self, urls, cur):
        if type(urls) is not list:
            urls = [urls]
        for url in urls:
            try:
                if url in self.coletados or url == 'url':
                    continue
                page = requests.get(url)
                campos_obrigatorios = self.bd.criar_dicionario_campos_obrigatorios()
                campos_adicionais = {}
                soup = BeautifulSoup(page.text, 'html.parser')
                campos_obrigatorios['marca'] = 'Havaianas'
                campos_obrigatorios['codigo_match'] = url.split('-')[-1].replace('.html','')
                campos_obrigatorios['product_id'] = campos_obrigatorios['codigo_match'].split('_')[0]
                campos_obrigatorios['sku'] = campos_obrigatorios['codigo_match']
                campos_obrigatorios['codigo_interno'] = self.gerar_codigo_interno(campos_obrigatorios['marca'], campos_obrigatorios['codigo_match'])
                campos_obrigatorios['nome'] = soup.find('h1', {'class':'product__title'}).text.replace('\n','')
                campos_obrigatorios['cor'] = ''
                campos_obrigatorios['descricao'] = ''
                descricao_box = soup.find('div', {'class':'product__info'}).find_all('p')
                for i in range(len(descricao_box)):
                    if descricao_box[i] == None:
                        continue
                    if 'cor' == descricao_box[i].text.replace('\n','').lower().replace(' ',''):
                        campos_obrigatorios['cor'] = descricao_box[i+1].text.replace('\n','')
                        break
                    else:
                        campos_obrigatorios['descricao'] += descricao_box[i].text.replace('\n','') + ' '
                headers_tamanhos = {
                    'authority': 'havaianas.com.br',
                    'referer': url,
                }
                params_tamanhos = (
                    ('pid', campos_obrigatorios['codigo_match']),
                    ('context', 'pay'),
                )
                response_tamanhos = requests.get('https://havaianas.com.br/on/demandware.store/Sites-Havaianas-BR-Site/pt_BR/Product-Variants', headers=headers_tamanhos, params=params_tamanhos)
                soup_tamanhos = BeautifulSoup(response_tamanhos.text, 'html.parser')
                tamanhos_box = soup_tamanhos.find_all('span')
                campos_obrigatorios['tamanho'] = ''
                for tamanho in tamanhos_box:
                    if tamanho.text.replace('\n','') not in campos_obrigatorios['tamanho']:
                        campos_obrigatorios['tamanho'] += tamanho.text.replace('\n','') + ', '
                campos_obrigatorios['tamanho'] = campos_obrigatorios['tamanho'][:-2]
                fotos_box = soup.find('div', {'class':'product__images'}).find_all('img')
                fotos_box2 = soup.find_all('img', {'class':'imageGridContainer__image lazyload'})
                fotos_box.extend(fotos_box2)
                campos_obrigatorios['imagens'] = []
                for foto in fotos_box:
                    data_desktop = str(foto.get('data-desktop'))
                    if data_desktop not in campos_obrigatorios['imagens']:
                        campos_obrigatorios['imagens'].append(data_desktop)
                campos_obrigatorios['imagens'] = ', '.join(campos_obrigatorios['imagens'])
                campos_obrigatorios['data_coleta'] = str(datetime.today())
                campos_obrigatorios['url'] = url
                self.bd.adicionar_dados_obrigatorios_produto_bd(cur, campos_obrigatorios, campos_adicionais)
            except:
                self.arq_csv.gravar_erro('erros_coleta', url=url)