from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
from time import sleep
from pathlib import Path
import os
import itertools
import requests
import csv

def remove_repetidos(lista):
    l = []
    for i in lista:
        if i not in l:
            l.append(i)
    l.sort()
    return l

my_path = os.getcwd()
my_path = my_path.replace('\\','/')
fileProducts = Path(my_path+'/produtos.csv')
driver = webdriver.Chrome()
driver.get('https://www.sephora.com.br/')
sleep(3)


if fileProducts.is_file():
    with open(my_path+'/produtos.csv', 'r', encoding='Latin1') as f:
        listaProdutos = list(csv.reader(f))
        for i in range(len(listaProdutos)):
            listaProdutos[i] = str(listaProdutos[i][0])
        print('Encontrados '+ str(len(listaProdutos)-1)+' produtos em arquivo')
        sleep(0.2)
else:
    with open(my_path+'/produtos.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['Código','REF','Título','Descrição','Cor','Indicação','Modo de uso','Benefícios','Categoria','Subcategoria','Variações','Preço','Foto'])
try:
    with open('linksColetados.csv', 'r', newline='') as l:
        coletados = list(csv.reader(l))
        for i in range(len(coletados)):
            coletados[i] = coletados[i][0]
except:
    #with open('linksColetados.csv', 'a', newline='') as l:
    coletados = []
    print('Dados anteriores não encontrados')

try:
    with open(my_path+'/linksLidos.csv', 'r') as f:
        produtos = []
        linkList = list(csv.reader(f))
        for l in range(len(linkList)):
            produtos.append(str(linkList[l][0]))
    sleep(0.2)
except:
    menus = ['https://www.sephora.com.br/maquiagem',
            'https://www.sephora.com.br/perfumes',
            'https://www.sephora.com.br/tratamento',
            'https://www.sephora.com.br/corpo-e-banho',
            'https://www.sephora.com.br/cabelos',]
    produtos = []
    for menu in menus:
        driver.get(menu)

        page = 1
        while True:
            vitrini = driver.find_element_by_class_name('products-grid').find_elements_by_tag_name('li')
            for i in range(len(vitrini)):
                produtos.append(vitrini[i].find_element_by_tag_name('a').get_attribute('href'))
                with open(my_path+'/linksLidos.csv', 'a', newline='') as f:
                    writer = csv.writer(f)
                    writer.writerow([produtos[i]])

            if 'Próximo' in driver.find_element_by_class_name('toolbar').text:
                page += 1
                driver.get(menu+'?p='+str(page))
            else:
                break

fail = 0
for i in range(len(produtos)):
    if produtos[i] in coletados:
        fail += 1
        continue
    driver.get(produtos[i])
    soup = BeautifulSoup(driver.page_source)
    sleep(10)
    try:
        codigo = driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div/div/section/section[1]/div[2]/div[2]/form/div[1]/input[3]').get_attribute('value')
    except:
        continue
    titulo = driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div/div/section/section[1]/div[2]/div[2]/form/div[2]/div[1]/h1').text
    categoria = driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div/div/section/section[1]/div[1]/div[1]/div[1]/ul/li[2]/a').text
    try:
        subcategoria = driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div/div/section/section[1]/div[1]/div[1]/div[1]/ul/li[4]/a').text
    except:
        try:
            subcategoria = driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div/div/section/section[1]/div[1]/div[1]/div[1]/ul/li[3]/a').text
        except:
            subcategoria = ''
    try:
        preco = driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div/div/section/section[1]/div[2]/div[2]/form/div[3]/div[2]/div[1]/div[2]/span').text
    except:
        preco = driver.find_element_by_xpath('/html/body/div[7]/div/div[2]/div/div/section/section[1]/div[2]/div[2]/form/div[3]/div[1]/div[2]/p/span[2]/span').text

    try:
        descricao = soup.find('div', {'class': 'productDescription'}).text
    except:
        try:
            descricao = str(soup.find('div', {'class': 'std'}).text).split('\n')
        except:
            pass
            for j in range(len(descricao)):
                if descricao[j] != '':
                    descricao = descricao[j]
                    break

    indicacao = ''
    modo_de_uso = ''
    variacoes = ''
    beneficios = ''
    incremento = 0
    try:
        detalhes = str(soup.find('div', {'class': 'std'}).text).split('\n')
        for j in range(len(detalhes)):
            if 'INDICAÇÃO' in detalhes[j] or 'indicad' in detalhes[j]:
                try:
                    indicacao = detalhes[j+1]
                except:
                    indicacao = ''
            elif 'VOLUME' in detalhes[j]:
                variacoes = detalhes[j+1]
            elif 'olumetria' in detalhes[j] or 'onteúdo' in detalhes[j]:
                variacoes = str(detalhes[j]).replace('Volumetria: ','').replace(' Conteúdo ','')
            elif 'MODO DE USO' in detalhes[j] or 'MODO DE USAR' in detalhes[j]:
                while True:
                    incremento += 1
                    if detalhes[j+incremento] == '' or detalhes[j+incremento] == '\xa0':
                        pass
                    else:
                        modo_de_uso = detalhes[j+1]
                        break
            elif 'sua pele vai amar' in detalhes[j] or 'benefícios' in detalhes[j]:
                try:
                    beneficios = detalhes[j+1]
                except:
                    beneficios = ''
    except:
        pass

    refs = soup.find_all('p', {'class': 'reference'})
    download = []
    try:
        fotos = driver.find_element_by_id('mCSB_8_container').find_elements_by_tag_name('li')
    except:
        continue
    cores = soup.find_all('p', {'class': 'reference info thumb'})
    for j in range(len(fotos)):
        download.append(str(fotos[j].get_attribute('data-base-image-modal')).replace('60x60/',''))
        foto = str(fotos[j].get_attribute('data-base-image-modal')).replace('60x60/','')
        try:
            cor = cores[j].text
            for k in range(len(refs)):
                if cor == refs[k].text:
                    try:
                        if 'REF' in refs[k+1].text:
                            ref = refs[k+1].text
                            break
                        else:
                            ref = ''
                    except:
                        ref = ''
        except:
            cor = ''
            ref = ''
        with open('produtos.csv', 'a', newline='') as f:
            writer = csv.writer(f)
            writer.writerow([codigo, ref, titulo, descricao, cor, indicacao, modo_de_uso, beneficios, categoria, subcategoria, variacoes, preco, foto])

    cont = 0
    num = ''
    download = remove_repetidos(download)
    for j in range(len(download)):
        if cont > 0:
            num = '_' + str(cont)
        response = requests.get(download[j])
        try:
            file = open('imagens/' + codigo + num + '_' + str(cores[j].text).replace('/','') + '.jpg', 'wb')
        except:
            file = open('imagens/' + codigo + num + '_' + '.jpg', 'wb')
        file.write(response.content)
        file.close()
        sleep(0.5)
        cont += 1

    coletados.append(produtos[i])
    with open('linksColetados.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow([produtos[i]])

