from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from time import sleep
from pathlib import Path
from bs4 import BeautifulSoup
import requests
import csv
import ast
import os
from datetime import datetime

#Função de conversão de elemento BS para Xpath
def xpath_soup(element):
    components = []
    child = element if element.name else element.parent 
    for parent in child.parents:  # type: bs4.element.Tag
        siblings = parent.find_all(child.name, recursive=False)
        components.append(
            child.name if 1 == len(siblings) else '%s[%d]' % (
                child.name,
                next(i for i, s in enumerate(siblings, 1) if s is child)
                )
            )
        child = parent
    components.reverse()
    return '/%s' % '/'.join(components)


now = datetime.now()
now = str(now.year)+'-'+str(now.month)+'-'+str(now.day)

my_path = os.getcwd()
my_path = my_path.replace('\\','/')

pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    print ("Diretorio %s já criado" % pathLeituras)
else:
    print ("Diretorio %s criado com sucesso " % pathLeituras)
  
pathImg = my_path+"/imagens/img "+now
try:
    os.makedirs(pathImg)
except OSError:
    print ("Diretorio %s já criado" % pathImg)
else:
    print ("Diretorio %s criado com sucesso " % pathImg)


#Verificação da presença do arquivo de produtos
fileProducts = Path(my_path+'/produtos.csv')
if fileProducts.is_file():
    with open(my_path+'/produtos.csv', 'r', encoding="Latin1") as f:
        listaProdutos = list(csv.reader(f))
        for i in range(len(listaProdutos)):
            listaProdutos[i] = str(listaProdutos[i][0])
        print('Encontrados '+ str(len(listaProdutos)-1)+' produtos em arquivo')
        sleep(0.2)
else:
    with open(my_path+'/produtos.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['Código','Título','Descricao', 'Tamanho', 'Preço', 'Link Imagem', 'Nome Imagem'])

#Verificação da presença do arquivo de produtos
fileProducts = Path(my_path+'/leituras/produtos.csv')
if fileProducts.is_file():
    with open(pathLeituras+'/produtos'+now+'.csv', 'r', encoding="Latin1") as f:
        listaProdutos = list(csv.reader(f))
        for i in range(len(listaProdutos)):
            listaProdutos[i] = str(listaProdutos[i][0])
        print('Encontrados '+ str(len(listaProdutos)-1)+' produtos em arquivo')
        sleep(0.2)
else:
    with open(pathLeituras+'/produtos'+now+'.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['Código','Título','Descricao', 'Tamanho', 'Preço', 'Link Imagem', 'Nome Imagem'])

#Inicialização dos serviços e navegador
service = Service('C:/Program Files/Google/Chrome/Application/chromedriver')
service.start()
driver = webdriver.Remote(service.service_url)
driver.get('https://www.thebodyshop.com.br/c/todos_os_produtos')
sleep(2)
#Criação da variável de produtos já coletados
coletados = []

#Atribuição dos itens já coletados na variável
try:
    with open(my_path+'/linksColetados.csv', 'r') as l:
        coletados = list(csv.reader(l))
        for i in range(len(coletados)):
            coletados[i] = str(coletados[i][0])
        print('Encontrados '+ str(len(coletados))+' links já coletados em arquivo')
        sleep(0.2)
except:      
    print('Dados anteriores não encontrados')   

while True:
    try:
        btnNext = driver.find_element_by_xpath('/html/body/div[1]/div[3]/div[2]/div[2]/span')
        btnNext.click()
        sleep(1)
    except:
        break
    
soup = BeautifulSoup(driver.page_source)
vitrines = soup.findAll('div', {'class':'MuiGrid-root MuiGrid-item MuiGrid-grid-xs-6 MuiGrid-grid-sm-4'})

linksCapturados = []
for i in range(len(vitrines)):
    link = driver.find_element_by_xpath(xpath_soup(vitrines[i])).find_element_by_tag_name('a').get_attribute('href')
    linksCapturados.append(link)

produtos = []
for link in linksCapturados:
    if link not in produtos:
        produtos.append(link)

k=1
for prod in produtos:
    if prod in coletados:
        print('Produtos ja coletado previamente')
        k += 1
        continue

    driver.get(prod)
    sleep(2)
    
    soup = BeautifulSoup(driver.page_source)
    info = soup.find("script", {"type": "application/ld+json"})
    info = str(info)
    try:
        info = info.split('<script type="application/ld+json">')[1]
        info = info.split('</script>')[0]
        dicInfo = ast.literal_eval(info)
    except:
        continue
    try:
        cod = soup.find('p',{'class':'MuiTypography-root MuiTypography-body2 MuiTypography-colorTextPrimary MuiTypography-alignLeft'}).text
    except:
        continue
    cod = cod.split('Cod: ')[1]
    codigo = cod.split(' -')[0]
    try:
        tam = cod.split(' -')[2]
    except:
        tam = ''

    titulo = dicInfo['name']
    print(str(k),'-',titulo)
    
    try:
        preco = str(dicInfo['offers']['price']).replace('.',',')
    except:
        preco = ''
    sleep(0.3)
    
    try:
        descricao = str(dicInfo['description'])
    except:
        descricao = ''
        
    imgLink = dicInfo['image']
    response = requests.get(imgLink)
    file = open(pathImg+'/' + codigo +'.jpg', 'wb')
    file.write(response.content)
    file.close()
    imagem = codigo + '.jpg, '
    sleep(0.5)
    
    coletados.append(prod)
    k += 1

    #Salvar dados no arquivo
    with open(my_path+'/produtos.csv', 'a', newline='') as f:
        writer = csv.writer(f)
                        #writer.writerow(['Código','Título','Descricao', 'Tamanho', 'Preço', 'Link Imagem', 'Nome Imagem'])
        writer.writerow([codigo, titulo, descricao, tam,  preco, imgLink, imagem])
    
    with open(pathLeituras+'/produtos'+now+'.csv', 'a', newline='') as f:
        writer = csv.writer(f)
                              #writer.writerow(['Código','Título','Descricao', 'Tamanho', 'Preço', 'Link Imagem', 'Nome Imagem'])
        writer.writerow([codigo, titulo, descricao, tam,  preco, imgLink, imagem])  

    #Salvar link na lista de produtos já coletados
    with open(my_path+'/linksColetados.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow([prod])
