from selenium.webdriver.chrome.service import Service
from curadoria.libs.busca_codigo import Busca_Codigo
from selenium import webdriver
from time import sleep

#sku, ean, produto, descricao, imagem, categoria, marca, fabricante
class Carrefour():
    def pega_dados(self, url):
        driver = webdriver.Chrome('C:\workspace\curadoria\webdriver\chromedriver.exe')
        driver.maximize_window()

        driver.get(url)
        sleep(3)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        sleep(3)
        # try:
        #     driver.find_element_by_class_name('push-container__btn-close').click()
        #     sleep(2)
        # except:
        #     driver.refresh()

        ean = Busca_Codigo.busca_codigo('ean', driver)
        nome = driver.find_element_by_class_name('vtex-store-components-3-x-productBrand').text
        descricao = driver.find_element_by_css_selector('.vtex-store-components-3-x-productDescriptionText.c-muted-1').text
        categoria = ' > '.join(driver.find_element_by_css_selector('.vtex-breadcrumb-1-x-container.pv3').text.split('\n')[:-1])
        atributos = driver.find_elements_by_css_selector('.flex.mt0.mb0.pt0.pb0.justify-start.vtex-flex-layout-0-x-flexRowContent.vtex-flex-layout-0-x-flexRowContent--productSpecification.items-stretch.w-100')

        imgs = driver.find_elements_by_class_name('swiper-wrapper')[2].find_elements_by_tag_name('img')
        fotos = []
        for i in range(len(imgs)):
            foto = imgs[i].get_attribute('src').replace('150-auto','1000-auto').replace('','')
            if foto not in fotos:
                fotos.append(foto)
        print(fotos)

        campos_adicionais = {}
        campos_adicionais['ean'] = ean
        campos_adicionais['nome'] = nome
        for i in range(len(atributos)):
            campos_adicionais[atributos[i].text.split('\n')[0]] = atributos[i].text.split('\n')[1]

        print(ean)
        print(nome)
        print(descricao)
        print(categoria)
        print(atributos)
        print(imgs)
        print(campos_adicionais)