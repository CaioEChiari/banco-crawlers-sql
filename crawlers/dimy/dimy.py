from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
from time import sleep
from pathlib import Path
from datetime import datetime
import os
import itertools
import requests
import csv

#Função de conversão de elemento BS para Xpath
def xpath_soup(element):
    components = []
    child = element if element.name else element.parent 
    for parent in child.parents:  # type: bs4.element.Tag
        siblings = parent.find_all(child.name, recursive=False)
        components.append(
            child.name if 1 == len(siblings) else '%s[%d]' % (
                child.name,
                next(i for i, s in enumerate(siblings, 1) if s is child)
                )
            )
        child = parent
    components.reverse()
    return '/%s' % '/'.join(components)



#Aquisição da data atual
now = datetime.now()
now = str(now.year)+'-'+str(now.month)+'-'+str(now.day)
#Aquisição do caminho do arquivo
my_path = os.getcwd()
my_path = my_path.replace('\\','/')

#Criação da pasta 'leituras'
pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    print ("Diretorio %s já criado" % pathLeituras)
else:
    print ("Diretorio %s criado com sucesso " % pathLeituras)
 #Criação da pasta 'imagens' 
pathImg = my_path+"/imagens/img "+now
try:
    os.makedirs(pathImg)
except OSError:
    print ("Diretorio %s já criado" % pathImg)
else:
    print ("Diretorio %s criado com sucesso " % pathImg)

#Verificação da presença do arquivo de produtos
fileProducts = Path(my_path+'/produtos.csv')


#Verificação da presença do arquivo de produtos anterior
fileProducts = Path(my_path+'/produtos.csv')
if fileProducts.is_file():
    with open(f'{my_path}/produtos.csv', 'r', encoding="utf-8") as f:
        listaProdutos = list(csv.reader(f))
        for i in range(len(listaProdutos)):
            listaProdutos[i] = str(listaProdutos[i][0])
        print('Encontrados '+ str(len(listaProdutos)-1)+' produtos em arquivo')
        sleep(0.2)
else:
    with open(f'{my_path}/produtos.csv', 'a', newline='',encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow(['Código','Título','Descrição','Tecido','Detalhe','Modelagem', 'Fotos'])


fileProducts = Path(my_path+'/leituras/produtos.csv')
if fileProducts.is_file():
    with open(f'{pathLeituras}/produtos{now}.csv', 'r', encoding="utf-8") as f:
        listaProdutos = list(csv.reader(f))
        for i in range(len(listaProdutos)):
            listaProdutos[i] = str(listaProdutos[i][0])
        print('Encontrados '+ str(len(listaProdutos)-1)+' produtos em arquivo')
        sleep(0.2)
else:
    with open(f'{pathLeituras}/produtos{now}.csv', 'a', newline='',encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow(['Código','Título','Descrição','Tecido','Detalhe','Modelagem', 'Fotos'])


#Inicialização dos serviços e navegador
service = Service('C:/Program Files/Google/Chrome/Application/chromedriver')
service.start()
driver = webdriver.Remote(service.service_url)
driver.get('https://www.dimyoficial.com/')
actions = webdriver.ActionChains(driver)
touchActions = webdriver.TouchActions(driver)
soup = BeautifulSoup(driver.page_source)
#Criação da variável de produtos já coletados
coletados = []
produtos = []
linksCapturados = []


#Atribuição dos itens já coletados na variável
try:
    with open(f'{my_path}/linksColetados.csv', 'r') as l:
        coletados = list(csv.reader(l))
        for i in range(len(coletados)):
            coletados[i] = str(coletados[i][0])
        print('Encontrados '+ str(len(coletados))+' links já coletados em arquivo')
        sleep(0.2)
except:
    print('Dados anteriores não encontrados')   


a = driver.find_elements_by_xpath('./html/body/div[2]/header/div[2]/div[3]/div/div[2]/nav/ul/li')


#Ler quantidade de categorias no site
categorias = []
for i in range(len(a)-1):
    categorias.append(a[i].find_element_by_tag_name('a').get_attribute('href'))
categorias.append("https://www.dimyoficial.com/outlet")



#Criar lista de produtos das categorias
for i in range(len(categorias)):
    url = f'{categorias[i]}?p='
    page = 1
    
    #driver.get(url+str(page))

    while True:
        urlPage = url+str(page)
        driver.get(urlPage)
        soup = BeautifulSoup(driver.page_source)
        vitrine = soup.findAll("div",{"class":"product-item-info"})
        links = []
        for item in vitrine:
            links.append(item.find('a')['href'])
        
        if len(links) == 0:
            break
        else:
            page +=1
            for l in links:
                if l not in produtos:
                    produtos.append(l)
                    with open(f'{my_path}/linksLidos.csv', 'a', encoding='utf-8', newline='') as f:
                        writer = csv.writer(f)
                        writer.writerow([l])
                else:
                    continue
            print(f'{len(produtos)} coletados')


#Coleta dos dados dos produtos
for k in range(len(produtos)):
    #Se o link já constar na lista de coletados, pular
    if produtos[k] in coletados:
        print(f'Produto [{produtos[k]}] encontrado em arquivo!')
        continue
    
    driver.get(produtos[k])
    sleep(0.6)
    soup = BeautifulSoup(driver.page_source)
    
    codigo = soup.find("div",{"itemprop":"sku"}).text
    titulo = soup.find("h1", {"class":"page-title"}).text
    titulo = titulo.replace('\n', '')
        
    print(str(k+1),'-',titulo)
    soup = BeautifulSoup(driver.page_source)

    try:
        categoria = soup.find('li', {'class':'item category'}).find('a').text
    except:
        categoria = ''
    links = []
    while len(links) == 0:
        soup = BeautifulSoup(driver.page_source)
        divImg = soup.findAll('img',{'class':'fotorama__img'})
        for img in divImg:
            links.append(img['src'])
        
    # if len(links) == 0:
    #     while len(links) == 0:
    #         print("Tentando encontrar imagens...")
    #         soup = BeautifulSoup(driver.page_source)
    #         sleep(0.3)
    #         links = []
    #         divImg = soup.findAll('img',{'class':'fotorama__img'})
    #         for img in divImg:
    #             links.append(img['src'])
        

    descricao = str(soup.find('div',{'id':'product-description'}))
    descricao = descricao.split('</h2>')[1]
    descricao = descricao.split('</div>')[0]
    descricao = descricao.replace('\n','')
    

    try:
        tecido = soup.find('td',{'data-th':'Tecido'}).text
    except:
        tecido = ''
    try:
        modelagem = soup.find('td',{'data-th':'Modelagem'}).tex
    except:
        modelagem = ''
    try:
        detalhe = soup.find('td',{'data-th':'Detalhe'}).text
    except:
        detalhe = ''
    
    imgLink = ''
    for img in links:
        imgLink = f'{imgLink}{img};'
    imgLink = imgLink[:-1]
    
    # cont = 0
    # num = ''
    # for l in range(len(links)):
    #     if cont > 0:
    #         num = '_' + str(cont)
    #     response = requests.get(links[l])
    #     file = open('imagens/' + codigo + num + '.jpg', 'wb')
    #     file.write(response.content)
    #     file.close()
    #     sleep(0.5)
    #     cont += 1

    #Salvar produto na variável de coletados
    coletados.append(produtos[k])
    #Salvar dados no arquivo
    with open(f'{my_path}/produtos.csv', 'a', newline='',encoding='utf-8') as f:
        writer = csv.writer(f)
        #               ['Código','Título','Descrição','Tecido','Detalhe','Modelagem', 'Fotos']
        writer.writerow([codigo, titulo, descricao, tecido, detalhe, modelagem, imgLink])
    with open(f'{pathLeituras}/produtos{now}.csv', 'a', newline='',encoding='utf-8') as f:
        writer = csv.writer(f)
        #               ['Código','Título','Descrição','Tecido','Detalhe','Modelagem', 'Fotos']
        writer.writerow([codigo, titulo, descricao, tecido, detalhe, modelagem, imgLink])
    #Salvar link na lista de produtos já coletados
    with open(my_path+'/linksColetados.csv', 'a', newline='',encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow([produtos[k]])