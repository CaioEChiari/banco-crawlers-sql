from libs.crawler_modelo import Crawler_Modelo
from libs.banco_de_dados import Banco_De_Dados
from selenium import webdriver
from datetime import datetime
from time import sleep

class Arezzo(Crawler_Modelo):
    def pegar_urls_produtos(self):
        try:
            site = 'https://www.arezzo.com.br/'
            driver = webdriver.Chrome('C:\workspace\curadoria\webdriver\chromedriver.exe')
            driver.maximize_window()
            driver.get(site)
            sleep(2)
            ul_menu = driver.find_element_by_css_selector('.navigation__list.ty-container')
            ul_menu = ul_menu.find_elements_by_tag_name('a')
            produtos_encontrados = []
            menus = []
            for i in range(len(ul_menu)):
                menu_link = ul_menu[i].get_attribute('href')
                if menu_link == 'javascript:void(0);':
                    continue
                if menu_link not in menus:
                    menus.append(menu_link)
            coletados = []
            for i in range(len(menus)):
                driver.get(menus[i])
                print(menus[i])
                cont = 0
                while True:
                    cont += 1
                    print(cont)
                    sleep(1)
                    a = driver.find_element_by_class_name('arz-storefront-products').find_elements_by_tag_name('a')
                    for j in range(len(a)):
                        driver.execute_script('arguments[0].scrollIntoView();', a[j])
                        a_href = a[j].get_attribute('href')
                        if a_href not in menus:
                            if a_href not in produtos_encontrados:
                                produtos_encontrados.append(a_href)
                    try:
                        btn_prox = driver.find_elements_by_class_name('arz-pagination-control')[1]
                    except:
                        break
                    if btn_prox.get_attribute('style') != 'display: none;':
                        sleep(0.5)
                        try:
                            btn_prox.click()
                        except:
                            continue
                    else:
                        break
        except:
            self.arq_csv.gravar_erro('erros_urls', url=menus[i], crawler=self.pega_nome_classe())
        return produtos_encontrados

    def pegar_dados_produtos(self, urls, cur):
        if type(urls) is not list:
            urls = [urls]
        driver = webdriver.Chrome('C:\workspace\curadoria\webdriver\chromedriver.exe')
        driver.maximize_window()
        for url in urls:
            try:
                if url in self.coletados or url == 'url':
                    continue
                campos_obrigatorios = self.bd.criar_dicionario_campos_obrigatorios()
                campos_adicionais = {}
                driver.get(url)
                sleep(4)
                for g in driver.find_elements_by_css_selector('.ty-accordion__label.ty-accordion__label--plus.ty-title.ty-title--xsmall'):
                    try:
                        driver.execute_script('arguments[0].scrollIntoView();', g)
                        driver.execute_script('window.scrollBy(0, -200)')
                        g.click()
                        sleep(0.1)
                    except:
                        pass
                codigo = driver.find_element_by_id('aut-product-reference').text
                campos_obrigatorios['codigo_match'] = codigo.replace('Referência: ','')
                campos_obrigatorios['marca'] = 'Arezzo'
                campos_obrigatorios['codigo_interno'] = self.gerar_codigo_interno(campos_obrigatorios['marca'], campos_obrigatorios['codigo_match'])
                campos_adicionais['codigo_interno'] = campos_obrigatorios['codigo_interno']
                try:
                    ul_tamanho = driver.find_element_by_css_selector('.ty-list.ty-list--size.arz-btn-group.arz-combined-sizes-8').text.split('\n')
                    tamanho = ''
                    for l in ul_tamanho:
                        if 'resta' in l.lower():
                            continue
                        tamanho += l + ', '
                    campos_obrigatorios['tamanho'] = tamanho.replace('product-stock-level-','')[:-2]
                except:
                    campos_obrigatorios['tamanho'] = ''
                campos_obrigatorios['nome'] = driver.find_element_by_id('js-product-name').text.strip()
                campos_obrigatorios['categoria'] = driver.find_element_by_id('breadcrumb').text.replace('\n',' > ').replace('Home > ','').replace(' > ' + campos_obrigatorios['nome'],'')
                try:
                    tamanho_salto = driver.find_element_by_id('aut-product-size').text
                    campos_adicionais['tamanho_salto'] = tamanho_salto.replace('Tamanho do salto: ','')
                except:
                    campos_adicionais['tamanho_salto'] = ''
                try:
                    cor = driver.find_element_by_id('aut-product-color').text
                    campos_obrigatorios['cor'] = cor.replace('Cor: ','')
                except:
                    campos_obrigatorios['cor'] = ''
                try:
                    material = driver.find_element_by_id('aut-product-material').text
                    campos_adicionais['material'] = material.replace('Material: ','')
                except:
                    campos_adicionais['material'] = ''
                try:
                    descricao = driver.find_elements_by_css_selector('.ty-description.ty-description--left')[1].text
                    descricao = descricao.replace('\n',' ')
                    campos_obrigatorios['descricao'] = descricao.replace('  ',' ')
                except:
                    campos_obrigatorios['descricao'] = ''
                n_imagens = driver.find_element_by_css_selector('.product-gallery--image.slick-initialized.slick-slider.slick-dotted').find_elements_by_tag_name('li')
                campos_obrigatorios['imagens'] = []
                img_box = driver.find_elements_by_class_name('slick-track')[1].find_elements_by_tag_name('img')
                for img in img_box:
                    url_image = img.get_attribute('data-zoom-image')
                    if url_image == None:
                        continue
                    if url_image not in campos_obrigatorios['imagens']:
                        campos_obrigatorios['imagens'].append(url_image)
                campos_obrigatorios['imagens'] = ', '.join(campos_obrigatorios['imagens'])
                cont = 0
                num = ''
                campos_obrigatorios['data_coleta'] = str(datetime.today())
                campos_obrigatorios['url'] = url
                self.bd.adicionar_dados_obrigatorios_produto_bd(cur, campos_obrigatorios, campos_adicionais)
            except:
                self.arq_csv.gravar_erro('erros_coleta', url=url, crawler=self.pega_nome_classe())