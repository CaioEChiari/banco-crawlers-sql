from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
from time import sleep
from pathlib import Path
from datetime import datetime
import random
import os
import requests
import csv

my_path = os.getcwd()
my_path = my_path.replace('\\','/')

now = datetime.now()
now = str(now.year)+'-'+str(now.month)+'-'+str(now.day)

pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    print ("Diretorio %s já criado" % pathLeituras)
else:
    print ("Diretorio %s criado com sucesso " % pathLeituras)
  
pathImg = my_path+"/imagens/img "+now
try:
    os.makedirs(pathImg)
except OSError:
    print ("Diretorio %s já criado" % pathImg)
else:
    print ("Diretorio %s criado com sucesso " % pathImg)


#Verificação da presença do arquivo de produtos
fileProducts = Path(my_path+'/produtos.csv')
if fileProducts.is_file():
    with open(my_path+'/produtos.csv', 'r', encoding="Latin1") as f:
        listaProdutos = list(csv.reader(f))
        for i in range(len(listaProdutos)):
            listaProdutos[i] = str(listaProdutos[i][0])
        print('Encontrados '+ str(len(listaProdutos)-1)+' produtos em arquivo')
        sleep(0.2)
else:
    with open(my_path+'/produtos.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['Codigo', 'nome', 'descricao', 'tamanho', 'marca', 'imagem', 'img'])
        
#Verificação da presença do arquivo de produtos
fileProducts = Path(my_path+'/leituras/produtos.csv')
if fileProducts.is_file():
    with open(pathLeituras+'/produtos'+now+'.csv', 'r', encoding="Latin1") as f:
        listaProdutos = list(csv.reader(f))
        for i in range(len(listaProdutos)):
            listaProdutos[i] = str(listaProdutos[i][0])
        print('Encontrados '+ str(len(listaProdutos)-1)+' produtos em arquivo')
        sleep(0.2)
else:
    with open(pathLeituras+'/produtos'+now+'.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['Codigo', 'nome', 'descricao', 'tamanho', 'marca', 'imagem', 'img'])

#Verificação de coleta realizada no dia ou criação de novo arquivo de coleta
fileProducts = Path(my_path+'/leituras/produtos'+now+'.csv')
if fileProducts.is_file():
    with open(my_path+'/leituras/produtos'+now+'.csv', 'r', encoding="Latin1") as f:
        listaProdutos = list(csv.reader(f))
        for i in range(len(listaProdutos)):
            listaProdutos[i] = str(listaProdutos[i][0])
        print('Encontrados '+ str(len(listaProdutos)-1)+' já lidos hoje')
        sleep(0.2)
else:
    with open(my_path+'/leituras/produtos'+now+'.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['Codigo', 'nome', 'descricao', 'tamanho', 'marca', 'imagem', 'img'])



#Inicialização dos serviços e navegador
service = Service('C:/Program Files/Google/Chrome/Application/chromedriver')
service.start()
driver = webdriver.Remote(service.service_url)
driver.get('https://www.dpcnet.com.br/catalogo')
produtos = []
coletados = []
try:
    with open(my_path+'/coletados.csv', 'r',encoding='UTF-8') as f:
        coletados = list(csv.reader(f))
        for i in range(len(coletados)):
            coletados[i] = coletados[i][0]
except:
    print('Itens anteriores não encontrados')

page = 1
url = 'https://www.dpcnet.com.br/catalogo?page='
urlProd = 'https://www.dpcnet.com.br'
while True:
    driver.get(url+str(page))
    sleep(1.3)
    soup = BeautifulSoup(driver.page_source)
    links = []
    vitrines = soup.findAll("div", {"class":"produto-content col-lg-12 col-md-12 col-sm-12 col-xs-12"})
    if len(vitrines) == 0:
        break
    for item in vitrines:
        link = urlProd+item.find("a", href=True)['href']
        if link not in produtos:
            produtos.append(link)
    page += 1
    print(str(len(produtos))+' links lidos')
    sleep(0.7)

for i in range(len(produtos)):
    if produtos[i] in coletados:
        print('Produto ['+str(i+1)+'] coletado')
        continue
    
    driver.get(produtos[i])
    sleep(1)
    soup = BeautifulSoup(driver.page_source)
    
    codigo = soup.find("span", {"class":"codigo-produto"}).text.strip('\n ').strip('\n            |')
    nome = soup.find("h1", {"class":"title"}).text
    print(str(i+1)+' - '+nome)
    descricao = soup.find("span", {"class":"text-justify"}).text
    marca = ''
    try:
        marca = soup.find("img", {"class":"img-parceiro"})['src']
    except:
        pass

    td = soup.findAll({"td":"data-v-16c460ba"})
    for j in range(len(td)):
        tdText = td[j].text
        if tdText == "Quantidade embalagem venda":
            tam = td[j+1].text
            try:
                tam = int(tam)
                tamanho = tam
                break
            except:
                tamanho = ''
                break
    
    imagem = soup.find("img", {"class":"img-responsive img-produto center-block"})['src']
    try:
        response = requests.get(imagem)
        file = open(pathImg+'/' + codigo + '.jpg', 'wb')
        file.write(response.content)
        file.close()
        img = codigo+'.jpg'
    except:
        img = ''
        imagem = ''
    
    coletados.append(produtos[i])
    
    with open(my_path+'/produtos.csv', 'a', newline='',encoding='UTF-8') as f:
        writer = csv.writer(f)
        writer.writerow([codigo, nome, descricao, tamanho, marca, imagem, img])
    
    with open(pathLeituras+'/produtos'+now+'.csv', 'a', newline='',encoding='UTF-8') as f:
        writer = csv.writer(f)
        writer.writerow([codigo, nome, descricao, tamanho, marca, imagem, img])
        
    with open(my_path+'/linksColetados.csv', 'a', newline='',encoding='UTF-8') as f:
        writer = csv.writer(f)
        writer.writerow([produtos[i]])