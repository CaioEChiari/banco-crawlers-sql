from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from time import sleep
from pathlib import Path
from bs4 import BeautifulSoup
import os
from datetime import date
from datetime import datetime
import json
import pandas as pd
import re
import requests

#TOR
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile

#Diversos
from IPython.display import clear_output
from random import sample

url_homepage = 'https://www.lupo.com.br/'
col_Coletados = ['Data','Link']
col_Lidos = ['Data', 'Categoria','Link']
col_Quebrados = ['Data', 'Link']
col_Produtos = [
            'categoria',
            'subcategoria',
            'codigo',
            'ean',
            'sku',
            'productId',
            'produto',
            'skuname',
            'tamanho',
            'cor',
            'descricao',
            'composicao',
            'link imagem',
            'codigo imagem'
             ]

# Data e Hora para o Versionamento
diversao = date.today().strftime('%Y-%m-%d')

# Criação das Pastas
my_path = os.getcwd()
my_path = my_path.replace('\\','/')

# Criação da pasta 'leituras'
pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    print ("Diretorio %s já criado" % pathLeituras)
else:
    print ("Diretorio %s criado com sucesso " % pathLeituras)

# Criação da pasta 'imagens' 
pathImg = my_path+"/imagens/img "+diversao
try:
    os.makedirs(pathImg)
except OSError:
    print ("Diretorio %s já criado" % pathImg)
else:
    print ("Diretorio %s criado com sucesso " % pathImg)
    
# Pegar nome do site
name_site = url_homepage.split('.')[1]

# Criação dos nomes dos arquivos
name_file_Produtos = 'produtos_' + str(name_site) + '.csv'

name_file_Coletados = 'coletados_' + str(name_site) + '.csv'  # Dados coletados dos links

name_file_Lidos = 'lidos_' + str(name_site) + '.csv' # Lidos do site

name_file_Quebrados = 'quebrados_' + str(name_site) + '.csv' # Lidos do site

name_file_ProdVer = pathLeituras + '/' + diversao + '_' + 'produtos_' + str(name_site) + '.csv'

# Verificação/Criação do arquivo 'produtos'
fileProdutos = Path(my_path + '/' + name_file_Produtos) 

if fileProdutos.is_file():
    produtos = pd.read_csv(fileProdutos)
else:
    produtos = pd.DataFrame(columns = col_Produtos)
    produtos.to_csv(name_file_Produtos, index = False)
    

# Verificação/Criação do arquivo 'coletados'
fileColetados = Path(my_path + '/' + name_file_Coletados) 

if fileColetados.is_file():
    coletados = pd.read_csv(fileColetados)
else:
    coletados = pd.DataFrame(columns = col_Coletados)
    coletados.to_csv(name_file_Coletados, index = False)

# Verificação/Criação do arquivo 'linkLidos'
fileLidos = Path(my_path + '/' + name_file_Lidos) 

if fileLidos.is_file():
    lidos = pd.read_csv(fileLidos)
else:
    lidos = pd.DataFrame(columns = col_Lidos)
    lidos.to_csv(name_file_Lidos, index = False)
    
# Verificação/Criação do arquivo 'linkLidos'
fileQuebrados = Path(my_path + '/' + name_file_Quebrados) 

if fileQuebrados.is_file():
    quebrados = pd.read_csv(fileQuebrados)
    
else:
    quebrados = pd.DataFrame(columns = col_Quebrados)
    quebrados.to_csv(name_file_Quebrados, index = False)

torexe = os.popen(r'C:\Users\Artur\Documents\Tor Browser\Browser\TorBrowser\Tor\tor.exe')

profile = FirefoxProfile(r'C:\Users\Artur\Documents\Tor Browser\Browser\TorBrowser\Data\Browser\profile.default')
profile.set_preference('network.proxy.type', 1)
profile.set_preference('network.proxy.socks', '127.0.0.1')
profile.set_preference('network.proxy.socks_port', 9050)
profile.set_preference("network.proxy.socks_remote_dns", False)
profile.update_preferences()

driver = webdriver.Firefox(firefox_profile= profile, executable_path=r'C:\Users\Artur\Documents\Tor Browser\geckodriver.exe')
#driver.get("http://check.torproject.org")
#driver.get('https://whatismyipaddress.com/')
driver.get(url_homepage)
driver.maximize_window()

def carrega_pagina(url, sleep_time = 2):
    driver.get(url)
    #driver.switch_to.window(driver.current_window_handle)
    sleep(sleep_time)
    soup = BeautifulSoup(driver.page_source)
    
    return soup

fileCategorias = Path(my_path + '/' + 'categorias_' + str(name_site) + '.csv') 
if  fileCategorias.is_file():
    my_cats = pd.read_csv(fileCategorias)
else:
    my_cats = pd.DataFrame(columns = ['Categoria','Link'])
    my_cats.to_csv('categorias_' + str(name_site) + '.csv', index = False)

soup = carrega_pagina(url_homepage,2)

mx_itens = soup.find('nav', {'class' : 'mx-itens_menu'})
menu = ['_items masc', '_items fem', '_items meninos', '_items meninas']

for i in menu:
    my_cat_title = soup.find('li',{'class', i}).find('a').text
    my_cat_link = 'https://www.lupo.com.br' + soup.find('li',{'class', i}).find('a')['href']
    
    if my_cat_link in list(my_cats.Link):
        continue
    else:
        my_cats = pd.read_csv(fileCategorias)
        my_cats = my_cats.append({
            'Categoria' : my_cat_title,
            'Link' : my_cat_link
        }, ignore_index = True)
        
        my_cats.to_csv('categorias_' + str(name_site) + '.csv', index = False)

def pega_produto(categoria, prateleira):
    global lidos   
    
    for i in prateleira:
        vitrine = i.find_all('li')
        for vit in vitrine:
            try:
                link_produto = vit.find('div',{'class' : 'product-name'}).find('a')['href']
            except:
                continue
           
            if link_produto in list(lidos.Link):
                continue

            else:
                now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

                lidos = pd.read_csv(fileLidos)

                lidos = lidos.append({
                    'Data' : now,
                    'Categoria' : categoria,
                    'Link' : link_produto
                }, ignore_index = True)

                lidos.drop_duplicates(subset=['Link'], inplace=True)
                lidos.to_csv(name_file_Lidos, index = False)

        clear_output(wait=True)
        print(categoria)
        print(len(vitrine))
        print('Total de produtos: ' + str(len(lidos)))
        print(lidos.Categoria.value_counts())

# Verificação/Criação do arquivo 'paginas_lidas'
    
for i in range(len(my_cats)):
    prat_count = 0
    my_link = my_cats.Link[i]
    soup = carrega_pagina(my_link)
    while True:                      
        for _ in range(5):
            driver.execute_script("window.scrollTo(0, window.scrollY + 500)")
            sleep(2)
            
        soup = BeautifulSoup(driver.page_source)
            
        prateleira = soup.find_all('div',{'class' : 'prateleira vitrine n12colunas'})
                        
        if len(prateleira) != prat_count:
            prat_count = len(prateleira)
            pega_produto(my_cats.Categoria[i], prateleira)
        else:
            prat_count = 0
            pega_produto(my_cats.Categoria[i], prateleira)
            break

def salva_imagens(imgLink, prod_code):
    global pathImg
    # Coleta e download da imagem através do dicionário
    response = requests.get(imgLink)
    file = open(pathImg+'/' + str(prod_code) +'.jpg', 'wb')
    file.write(response.content)
    file.close()    

def pega_dado(url):
    global col_Produtos
    
    info = pd.DataFrame(columns = col_Produtos)
    
    soup = carrega_pagina(url)
    
    itemprop = soup.find_all('li', {'itemprop' : 'itemListElement'})
    cats = []

    for i in itemprop:
        cats.append(i.text)
        
    categoria = cats[-2]
    subcategoria = cats[-1]
    
    script = soup.find_all('script')
    
    for i in script:
        if 'var skuJson_0' in str(i):
            skuJson = str(i)
            
        if 'vtex.events.addData' in str(i):
            vtex = str(i)
            
    skuJson = skuJson.split('var skuJson_0 = ')[1].split(';CATALOG')[0]
    d1 = json.loads(skuJson)
    
    vtex = vtex.split('<script>\nvtex.events.addData(')[1].split(');\n</script>')[0]
    d2 = json.loads(vtex)
    skus_ean = list(d2['skuStocks'].keys())
    eans = d2['productEans']
    
    productId = d1['productId']
    produto = d1['name']
    
    codigo = soup.find('div',{'class' : 'skuReference'}).text
    
    try:
        try:
            description = soup.find('div',{'class' : 'productDescription'}).text.strip().replace('\xa0','').split('Composição:')
            descricao = description[0].strip()
            composicao = description[1].strip()
        except:
            description = soup.find('div',{'class' : 'productDescription'}).text.strip().replace('\xa0','').split('composição:')
            descricao = description[0].strip()
            composicao = description[1].strip()
    except:
        descricao = soup.find('div',{'class' : 'productDescription'}).text.strip().replace('\xa0','')
        composicao = ''
    
    
    skus = d1['skus']
    for s in skus:
        ean = eans[skus_ean.index(str(s['sku']))]
        sku = s['sku']
        skuname = s['skuname']
        tamanho = s['dimensions']['Tamanho']
        cor = s['dimensions']['Cor']
        link_imagem = s['image']
        img_code = str(sku) + '.jpg'
        
        salva_imagens(link_imagem, sku)
        
        info_dados = {
            'categoria' : categoria,
            'subcategoria' : subcategoria,
            'codigo' : codigo,
            'ean' : ean,
            'sku' : sku,
            'productId' : productId,
            'produto' : produto,
            'skuname' : skuname,
            'tamanho' : tamanho,
            'cor' : cor,
            'descricao' : descricao,
            'composicao' : composicao,
            'link imagem' : link_imagem,
            'codigo imagem' : img_code
        }
        
        info = info.append(info_dados, ignore_index = True)
        
    return info
    

# Verificação/Criação do arquivo 'produtos versionados'
fileProdVer = Path(name_file_ProdVer) 

if fileProdVer.is_file():
    prod_versionado = pd.read_csv(name_file_ProdVer)
    
else:
    prod_versionado = pd.DataFrame(columns = col_Produtos)
    prod_versionado.to_csv(name_file_ProdVer, index = False)
    

# Executando
#sam = sample(range(len(lidos)),5)
#count = 1
#for i in sam:
#    print(count)
#    count += 1

for i in range(len(lidos)):
    
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    url_product = lidos.loc[i, 'Link']
    
    if  url_product in list(coletados.Link) or url_product in list(quebrados.Link):
        continue
        
    else:
        print(i)
        print(url_product)
        
    try:
        info_dados = pega_dado(url_product)
    except:       
        quebrados = pd.read_csv(fileQuebrados)
        quebrados = quebrados.append({
            'Data' : now,
            'Link' : url_product
        }, ignore_index = True)
        quebrados.to_csv(name_file_Quebrados, index = False)
        print('Algo de errado aconteceu!')
        continue
        
    coletados = pd.read_csv(fileColetados)
    coletados = coletados.append({
        'Data' : now,
        'Link' : url_product
    }, ignore_index = True)
    coletados.to_csv(name_file_Coletados, index = False)
    
    produtos = pd.read_csv(fileProdutos)
    produtos = produtos.append(info_dados)
    produtos.to_csv(name_file_Produtos, index = False)
    
    prod_versionado = pd.read_csv(name_file_ProdVer)
    prod_versionado = prod_versionado.append(info_dados, ignore_index=True)
    prod_versionado.to_csv(name_file_ProdVer, index = False)