from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from bs4 import BeautifulSoup
from time import sleep
import pandas as pd
#import pyautogui
import requests
import csv

#url = 'https://www.magazineluiza.com.br/bola-divertida-yes-toys-winfun-778/p/180898500/br/jomo/?&force=2&seller_id=magazineluiza&&utm_source=google&utm_medium=pla&utm_campaign=&partner_id=58935&gclid=EAIaIQobChMIhtux5LOq8gIVKgytBh0VaAb4EAsYASABEgIbmvD_BwE'

#def coleta_dados(url_product):
service = Service('C:\Program Files\Google\Chrome\Application\chromedriver.exe')
service.start()
driver = webdriver.Remote(service.service_url)
driver.maximize_window()

driver.get(url)

nome = driver.find_element_by_class_name('header-product__title').text
descricao = driver.find_element_by_class_name('description__container-text').text
atributos = driver.find_element_by_id('anchor-description').find_elements_by_tag_name('table')

for i in range(len(atributos)):
    print(atributos[i].text)


imgs = driver.find_element_by_class_name('showcase-product__container-thumbs').find_elements_by_tag_name('img')
fotos = []
for i in range(len(imgs)):
    foto = imgs[i].get_attribute('src').replace('88x66','1000x1000')
    if foto not in fotos:
        fotos.append(imgs[i].get_attribute('src').replace('88x66','1000x1000'))
print(fotos)