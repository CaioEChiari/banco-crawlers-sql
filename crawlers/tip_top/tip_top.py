from libs.crawler_modelo import Crawler_Modelo
from libs.banco_de_dados import Banco_De_Dados
from urllib.parse import unquote
from datetime import datetime
from bs4 import BeautifulSoup
from pathlib import Path
import urllib.parse
import requests
import json

class Tip_Top(Crawler_Modelo):
    def pegar_urls_produtos(self):
        try:
            site = 'https://www.tiptop.com.br'
            headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36',
                'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            }
            response = requests.get(site, headers=headers)
            soup = BeautifulSoup(response.text, 'html.parser')
            categorias_a = soup.find('ul',{'class':'section level-1'}).find_all('a')
            categorias_links = []
            produtos_encontrados = []
            for a in categorias_a:
                print(a.get('href'))
                categorias_links.extend([a.get('href')])
            categorias_links = categorias_links[:-1]
            produtos_a = []
            for categoria in categorias_links:
                print(categoria)
                proxima_pagina = True
                pagina = 1
                while proxima_pagina:
                    headers = {
                        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36',
                        'referer': site + categoria,
                    }
                    params = (
                        ('pg', str(pagina)),
                        ('fc', 'false'),
                    )
                    response = requests.get(site + categoria + '.partial', headers=headers, params=params)
                    print(pagina)
                    pagina += 1
                    soup = BeautifulSoup(response.text, 'html.parser')

                    try:
                        if soup.find('div', {'class':'wd-browsing-grid-list wd-widget-js'}).find_all('a') == []:
                            break
                    except:
                        if soup.find('div', {'class':'wd-browsing-grid-list wd-widget-js'}) == None:
                            break
                    produtos_a.extend(soup.find('div', {'class':'wd-browsing-grid-list wd-widget-js'}).find_all('a'))
                    for a in produtos_a:
                        a_href = site + a.get('href')
                        if a_href not in produtos_encontrados:
                            produtos_encontrados.append(a_href)
        except:
            self.arq_csv.gravar_erro('erros_urls', url=categoria, crawler=self.pega_nome_classe())
        return produtos_encontrados

    def pegar_dados_produtos(self, urls, cur):
        site = 'https://www.tiptop.com.br'
        foto_url_prefixo = 'https://d3mstcthfjpw3m.cloudfront.net'
        if type(urls) is not list:
            urls = [urls]
        for url in urls:
            try:
                if url in self.coletados or url == 'url':
                    continue
                headers = {
                    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebcontent_wrapperit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36',
                    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
                }
                response = requests.get(url, headers=headers)
                soup = BeautifulSoup(response.text, 'html.parser')
                content_wrapper = str(soup.find('div', {'id' : 'content-wrapper'}).find('script'))
                dict_info = json.loads(content_wrapper.split('= ', 1)[1].split('; </script>')[0])
                soup2 = BeautifulSoup(dict_info['Descriptions'][0]['Value'], 'html.parser')
                campos_obrigatorios = self.bd.criar_dicionario_campos_obrigatorios()
                campos_adicionais = {}
                campos_obrigatorios['marca'] = 'Tip Top'
                campos_obrigatorios['codigo_referencia'] = soup.find('small',{'class','code'}).text.replace('Cód.: ','')
                campos_obrigatorios['url'] = url
                campos_obrigatorios['data_coleta'] = str(datetime.now())
                campos_obrigatorios['descricao'] = soup2.text.replace('\n',' ').replace('\xa0','').replace('\r','').replace(':',': ')
                campos_obrigatorios['descricao'] = campos_obrigatorios['descricao'].replace('  ', ' ').strip()
                for i in range(1, len(dict_info['Items'])):
                    campos_obrigatorios['sku'] = dict_info['Items'][i]['SKU']
                    campos_obrigatorios['codigo_match'] = campos_obrigatorios['codigo_referencia'].split('.')[-1] + '_' + campos_obrigatorios['sku'].split('.')[-1]
                    campos_obrigatorios['codigo_interno'] = self.gerar_codigo_interno(campos_obrigatorios['marca'], campos_obrigatorios['codigo_match'])
                    campos_obrigatorios['product_id'] = str(dict_info['Items'][i]['ProductID'])
                    campos_obrigatorios['nome'] = dict_info['Items'][i]['Name'].split(' - ')[0]
                    campos_obrigatorios['cor'] = dict_info['Items'][i]['ExtendedMetadatas'][0]['Title']
                    try:
                        campos_obrigatorios['tamanho'] = dict_info['Items'][i]['ExtendedMetadatas'][1]['Title'].upper()
                    except:
                        campos_obrigatorios['tamanho'] = ''
                    foto_id = dict_info['Items'][i]['ExtendedMetadatas'][0]['PropertyPath']
                    campos_obrigatorios['imagens'] = []
                    for j in range(len(dict_info['Medias'])):
                        if dict_info['Medias'][j]['MediaSizeType'] == 'Large' and dict_info['Medias'][j]['VariationPath'] == foto_id:
                            if foto_url_prefixo + dict_info['Medias'][j]['MediaPath'] not in campos_obrigatorios['imagens']:
                                campos_obrigatorios['imagens'].append(foto_url_prefixo + dict_info['Medias'][j]['MediaPath'])
                    campos_obrigatorios['imagens'] = ', '.join(campos_obrigatorios['imagens'])
                    self.bd.adicionar_dados_obrigatorios_produto_bd(cur, campos_obrigatorios, campos_adicionais)
            except:
                self.arq_csv.gravar_erro('erros_coleta', url=url)