from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
from time import sleep
from pathlib import Path
import os
import itertools
import requests
import csv

def remove_repetidos(lista):
    l = []
    for i in lista:
        if i not in l:
            l.append(i)
    l.sort()
    return l

my_path = os.getcwd()
my_path = my_path.replace('\\','/')
fileProducts = Path(my_path+'/produtos.csv')
driver = webdriver.Chrome()
driver.get('https://www.ikesaki.com.br/')
sleep(3)

if fileProducts.is_file():
    with open(my_path+'/produtos.csv', 'r', encoding='Latin1') as f:
        listaProdutos = list(csv.reader(f))
        for i in range(len(listaProdutos)):
            listaProdutos[i] = str(listaProdutos[i][0])
        print('Encontrados '+ str(len(listaProdutos)-1)+' produtos em arquivo')
        sleep(0.2)
else:
    with open(my_path+'/produtos.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['Código', 'Ean', 'Título', 'Descrição', 'Tipo', 'Cor', 'Sugestão de uso', 'Característica/Ação', 'Resultado', 'Categoria', 'Subcategoria', 'Marca', 'Preço', 'Foto'])
try:
    with open('linksColetados.csv', 'r', newline='') as l:
        coletados = list(csv.reader(l))
        for i in range(len(coletados)):
            coletados[i] = coletados[i][0]
except:
    coletados = []
    print('Dados anteriores não encontrados')

try:
    with open(my_path+'/linksLidos.csv', 'r') as f:
        produtos = []
        linkList = list(csv.reader(f))
        for l in range(len(linkList)):
            produtos.append(str(linkList[l][0]))
    sleep(0.2)
except:
    menus = driver.find_element_by_xpath('/html/body/section/header/div/nav/div[1]/div/div/div[1]/div').find_elements_by_tag_name('a')
    links = []
    for i in range(len(menus)):
        links.append(menus[i].get_attribute('href'))
    
    submenuslinks = []
    for i in range(len(links)):
        driver.get(links[i])
        sleep(2)
        try:
            submenus = driver.find_element_by_xpath('/html/body/main/div/section[4]/div/section').find_elements_by_tag_name('a')
        except:
            submenus = driver.find_element_by_xpath('/html/body/main/div/div[1]/section[1]').find_elements_by_tag_name('a')
        for j in range(len(submenus)):
            if '#' in str(submenus[j].get_attribute('href')) or None == submenus[j].get_attribute('href'):
                pass
            else:
                submenuslinks.append(submenus[j].get_attribute('href'))
    produtos = []
    for i in range(13,len(submenuslinks)):
        driver.get(submenuslinks[i])
        sleep(3)
        try:
            try:
                total = int(driver.find_element_by_xpath('/html/body/main/div/div/div[2]/section/div/section/div[1]/p[2]/span/span[2]').text)
            except:
                total = int(driver.find_element_by_xpath('/html/body/main/div/div/div[2]/section/div/section/div/nav/div/p[2]/span/span[2]').text)

            for j in range(int((total*2) / 96) + 1):
                sleep(3)
                driver.find_element_by_class_name('carregar-mais').click()
                sleep(3)
        except:
            print(i, '-', submenuslinks[i])
            continue

        vitrini = driver.find_element_by_xpath('/html/body/main/div/div/div[2]/section/div/section/div[2]/div[2]/div[3]').find_elements_by_tag_name('a')
        for j in range(len(vitrini)):
            if 'javascript' in str(vitrini[j].get_attribute('href')):
                pass
            else:
                produtos.append(vitrini[j].get_attribute('href'))

        produtos += remove_repetidos(produtos)

for i in range(len(produtos)):
    print(i)
    if produtos[i] in coletados:
        continue

    driver.get(produtos[i])
    sleep(3)

    try:
        if driver.find_element_by_xpath('/html/body/main/div/div[1]/div/p').text == 'NÃO ENCONTRAMOS NENHUM RESULTADO.':
            continue
    except:
        pass

    codigo = driver.find_element_by_xpath('/html/body/main/section[3]/div/div/div[2]/div/div[1]/div').text
    titulo = driver.find_element_by_xpath('/html/body/main/section[3]/div/div/div[2]/div/div[1]/h1/div').text
    descricao = driver.find_element_by_class_name('productDescription').text
    detalhes = driver.find_element_by_id('caracteristicas')
    th = detalhes.find_elements_by_tag_name('th')
    td = detalhes.find_elements_by_tag_name('td')
    tipo = ''
    cor = ''
    caracteristica_acao = ''
    sugestao_de_uso = ''
    resultado = ''
    for j in range(len(th)):
        if th[j].text == '':
            continue
        elif th[j].text == 'Tipo':
            tipo = td[j].text
        elif th[j].text == 'Cores':
            cor = td[j].text
        elif th[j].text == 'Característica/Ação':
            caracteristica_acao = td[j].text
        elif th[j].text == 'Sugestão de uso':
            sugestao_de_uso = td[j].text
        elif th[j].text == 'Resultado':
            resultado = td[j].text
        else:
            pass
    categoria = driver.find_element_by_xpath('/html/body/main/section[2]/div/div/ul/li[2]/a/span').text
    try:
        subcategoria = driver.find_element_by_xpath('/html/body/main/section[2]/div/div/ul/li[3]/a/span').text
    except:
        try:
            subcategoria = driver.find_element_by_xpath('/html/body/main/section[2]/div/div/ul/li[3]').text
        except:
            pass
    try:
        marca = driver.find_element_by_xpath('/html/body/main/section[3]/div/div/div[1]/div/div/div[1]/div[2]/div').find_element_by_tag_name('img').get_attribute('alt')
    except:
        marca = driver.find_element_by_xpath('/html/body/main/section[3]/div/div/div[1]/div/div/div[1]/div[2]/div/a').text
    try:
        preco = driver.find_element_by_xpath('/html/body/main/section[3]/div/div/div[2]/div/div[3]/div[1]/div/div/p[1]/em[1]/strong').text
    except:
        preco = ''
    fotos = driver.find_element_by_class_name('thumbs').find_elements_by_tag_name('img')
    foto = ''
    ean = ''
    for j in range(len(fotos)):
        if 'youtube' in fotos[j].get_attribute('src'):
            continue
        foto += str(fotos[j].get_attribute('src')).split('?v=')[0] + ', '
        if titulo.split(' ')[0] in fotos[j].get_attribute('alt'):
            pass
        elif len(fotos[j].get_attribute('alt')) == 13:
            ean = fotos[j].get_attribute('alt')
        else:
            pass
    foto += foto[:-2]

    with open('produtos.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow([codigo, ean, titulo, descricao, tipo, cor, sugestao_de_uso, caracteristica_acao, resultado, categoria, subcategoria, marca, preco, foto])

    cont = 0
    num = ''
    download = foto.split(', ')
    download = remove_repetidos(download)
    for j in range(len(download)):
        if cont > 0:
            num = '_' + str(cont)
        response = requests.get(download[j])
        if '.jpg' in download[j]:
            file = open('imagens/' + codigo.replace('.','') + num + '_' + '.jpg', 'wb')
        else:
            file = open('imagens/' + codigo.replace('.','') + num + '_' + '.png', 'wb')
        file.write(response.content)
        file.close()
        sleep(1)
        cont += 1

    coletados.append(produtos[i])
    with open('linksColetados.csv', 'a', newline='') as f:
        writer = csv.writer(f)
        writer.writerow([produtos[i]])

