from bs4 import BeautifulSoup
from time import sleep
from pathlib import Path
from datetime import datetime
import random
import os
import requests
import csv
import ast


userList = [
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'
]

#Aquisição da data atual
now = datetime.now()
now = str(now.year)+'-'+str(now.month)+'-'+str(now.day)
#Aquisição do caminho do arquivo
my_path = os.getcwd().replace('\\','/')

#Criação da pasta 'leituras'
pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    print ("Diretorio %s já criado" % pathLeituras)
else:
    print ("Diretorio %s criado com sucesso " % pathLeituras)
 #Criação da pasta 'imagens' 
pathImg = my_path+"/imagens/img "+now
try:
    os.makedirs(pathImg)
except OSError:
    print ("Diretorio %s já criado" % pathImg)
else:
    print ("Diretorio %s criado com sucesso " % pathImg)

#Verificação da presença do arquivo de produtos
fileProducts = Path(my_path+'/produtos.csv')


#Verificação da presença do arquivo de produtos anterior
fileProducts = Path(my_path+'/produtos.csv')
if fileProducts.is_file():
    with open(f'{my_path}/produtos.csv', 'r', encoding="utf-8") as f:
        listaProdutos = list(csv.reader(f))
        for i in range(len(listaProdutos)):
            listaProdutos[i] = str(listaProdutos[i][0])
        print('Encontrados '+ str(len(listaProdutos)-1)+' produtos em arquivo')
        sleep(0.2)
else:
    with open(f'{my_path}/produtos.csv', 'a', newline='',encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow(['productID', 'referenceID', 'ean', 'nome', 'descricao', 'categoria', 'img'])


fileProducts = Path(my_path+'/leituras/produtos.csv')
if fileProducts.is_file():
    with open(f'{pathLeituras}/produtos{now}.csv', 'r', encoding="utf-8") as f:
        listaProdutos = list(csv.reader(f))
        for i in range(len(listaProdutos)):
            listaProdutos[i] = str(listaProdutos[i][0])
        print('Encontrados '+ str(len(listaProdutos)-1)+' produtos em arquivo')
        sleep(0.2)
else:
    with open(f'{pathLeituras}/produtos{now}.csv', 'a', newline='',encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow(['productID', 'referenceID', 'ean', 'nome', 'descricao', 'categoria', 'img'])


#Criação da variável de produtos já coletados
coletados = []
produtos = []
linksCapturados = []

url = 'https://www.ohboy.com.br/'
headers = {'User-Agent': random.choice(userList)}
# r = requests.get(f'{url}', headers).text
# soup = BeautifulSoup(r, 'html.parser')

categ = [
    "https://www.ohboy.com.br/ohshop-/roupas/?PageNumber=",
    "https://www.ohboy.com.br/yoboh?PageNumber="
]

for cat in categ:
    print(f'Lendo categoria {cat}')
    page = 1
    while True:
        print(f'Lendo pagina {page}')
        r = requests.get(f'{cat}{page}', headers).text
        soup = BeautifulSoup(r, 'html.parser')
        if cat == "https://www.ohboy.com.br/ohshop-/roupas/?PageNumber=": 
            vitrine = soup.find('div',{'class':'prateleira vitrine n16colunas'})
            try:
                vitrine = vitrine.findAll('a',{'class':'x-image'})
            except AttributeError:
                break
        elif cat == "https://www.ohboy.com.br/yoboh?PageNumber=":
            vitrine = soup.find('div',{'class':'prateleira vitrine n4colunas'})
            try:
                vitrine = vitrine.findAll('a', {'class':'x-image'})
            except AttributeError:
                break

        if len(vitrine) == 0:
            break
        for a in vitrine:
            link = a['href']
            if link not in produtos:
                produtos.append(link)
                with open(f'{my_path}/linksLidos.csv', 'a', encoding='utf-8', newline='') as f:
                    writer = csv.writer(f)
                    writer.writerow([link])
        print(f'{len(produtos)} links lidos')
        page += 1

i = 1
for p in produtos:
    if p in coletados:
        i += 1
        print(f'Produto já coletado!')
        continue
    r = requests.get(p, headers).text
    soup = BeautifulSoup(r, 'html.parser')

    info = r.split("vtex.events.addData(")[1].split(");")[0]
    info = ast.literal_eval(info)
    
    try:
        referenceID = str(info['productReferenceId'])
    except:
        i += 1
        print(f'Produto inválido')
        continue
    productID = str(info['productId'])
    ean = ''
    try:
        eans = info['productEans']
        for e in eans:
            ean += f'{e};'
        ean = ean[:-1]
    except KeyError:
        pass
  
    nome = info['productName']
    categoria = info['productCategoryName']
    descricao = soup.find('div',{'class':'productDescription'}).text

    img = ''
    images = soup.findAll('a',{'id':'botaoZoom'})
    for im in images:
        imgLink = im.find('img')['src']
        img = f'{img}{imgLink};'
    img = img[:-1]
    
    print(f'{i} -  [{referenceID} - {nome}]')
    
    #Salvar produto na variável de coletados
    coletados.append(p)
    #Salvar dados no arquivo
    with open(f'{my_path}/produtos.csv', 'a', newline='',encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow([productID, referenceID, ean, nome, descricao, categoria, img])
    with open(f'{pathLeituras}/produtos{now}.csv', 'a', newline='',encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow([productID, referenceID, ean, nome, descricao, categoria, img])
    #Salvar link na lista de produtos já coletados
    with open(my_path+'/produtosColetados.csv', 'a', newline='',encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow([p])
    i += 1
