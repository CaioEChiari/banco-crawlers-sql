from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from time import sleep
from pathlib import Path
from bs4 import BeautifulSoup
import os
from datetime import date
from datetime import datetime
import json
import pandas as pd
import re
import requests
import random

#TOR
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile

#Diversos
from IPython.display import clear_output
from random import sample


url_homepage = 'https://www.nivea.com.br/'
col_Coletados = ['Data','Link']
col_Lidos = ['Data', 'Categoria','Link']
col_Quebrados = ['Data', 'Link']
col_Produtos = ['ean']

# Data e Hora para o Versionamento
diversao = date.today().strftime('%Y-%m-%d')

# Criação das Pastas
my_path = os.getcwd()
my_path = my_path.replace('\\','/')

# Criação da pasta 'leituras'
pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    pass
    #print ("Diretorio %s já criado" % pathLeituras)
#else:
#    print ("Diretorio %s criado com sucesso " % pathLeituras)

# Criação da pasta 'imagens' 
pathImg = my_path+"/imagens/img "+diversao
try:
    os.makedirs(pathImg)
except OSError:
    #print ("Diretorio %s já criado" % pathImg)
    pass
#else:
    #print ("Diretorio %s criado com sucesso " % pathImg)
    
# Pegar nome do site
name_site = url_homepage.split('.')[1]

# Criação dos nomes dos arquivos
name_file_Produtos = 'produtos_' + str(name_site) + '.csv'

name_file_Coletados = 'coletados_' + str(name_site) + '.csv'  # Dados coletados dos links

name_file_Lidos = 'lidos_' + str(name_site) + '.csv' # Lidos do site

name_file_Quebrados = 'quebrados_' + str(name_site) + '.csv' # Lidos do site

name_file_ProdVer = pathLeituras + '/' + diversao + '_' + 'produtos_' + str(name_site) + '.csv'

# Verificação/Criação do arquivo 'produtos'
fileProdutos = Path(my_path + '/' + name_file_Produtos) 

if fileProdutos.is_file():
    produtos = pd.read_csv(fileProdutos)
else:
    produtos = pd.DataFrame(columns = col_Produtos)
    produtos.to_csv(name_file_Produtos, index = False)
    

# Verificação/Criação do arquivo 'coletados'
fileColetados = Path(my_path + '/' + name_file_Coletados) 

if fileColetados.is_file():
    coletados = pd.read_csv(fileColetados)
else:
    coletados = pd.DataFrame(columns = col_Coletados)
    coletados.to_csv(name_file_Coletados, index = False)

# Verificação/Criação do arquivo 'linkLidos'
fileLidos = Path(my_path + '/' + name_file_Lidos) 

if fileLidos.is_file():
    lidos = pd.read_csv(fileLidos)
else:
    lidos = pd.DataFrame(columns = col_Lidos)
    lidos.to_csv(name_file_Lidos, index = False)
    
# Verificação/Criação do arquivo 'linkLidos'
fileQuebrados = Path(my_path + '/' + name_file_Quebrados) 

if fileQuebrados.is_file():
    quebrados = pd.read_csv(fileQuebrados)
    
else:
    quebrados = pd.DataFrame(columns = col_Quebrados)
    quebrados.to_csv(name_file_Quebrados, index = False)


def carrega_pagina(url, sleep_time = 2):
    userList = [
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'
    ]

    headers = {'User-Agent': random.choice(userList)}  
    
    r = requests.get(url, headers).text
    soup = BeautifulSoup(r, 'html.parser')
    return soup


def pega_produto(categoria, vitrine):
    global lidos   

    for vit in vitrine:
        link_produto = 'https://www.nivea.com.br' + str(vit)
                    
        if link_produto in list(lidos.Link):
            continue
                        
        else:
            now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                        
            lidos = pd.read_csv(fileLidos)
                        
            lidos = lidos.append({
                'Data' : now,
                'Categoria' : categoria,
                'Link' : link_produto
            }, ignore_index = True)
                        
            lidos.drop_duplicates(subset=['Link'], inplace=True)
            lidos.to_csv(name_file_Lidos, index = False)
                        
    clear_output(wait=True)
    print(categoria)
    print(len(vitrine))
    print('Total de produtos: ' + str(len(lidos)))
    print(lidos.Categoria.value_counts())


count = 0

url = 'https://www.nivea.com.br/xajax/ProductOverviewPageGrid?uri=sitecore%3A%2F%2Fweb%2F%7B5C0ACBD8-C8EE-4525-91D8-0FD328652ABB%7D%3Flang%3Dpt-BR%26ver%3D2&skip='

while True:
    my_link = url + str(count)
    
    soup = carrega_pagina(my_link)
    a = soup.find_all('a')
    
    if len(a) == 0:
        break
    
    categoria = 'Produtos'
    vitrine = []
    
    for i in a:
        if '.html' in str(i['href']):
            vitrine.append(i['href'])
        
    pega_produto(categoria, vitrine)   
    
    count += 1  


def salva_imagem(imgLink, prod_code):
    global pathImg
    # Coleta e download da imagem através do dicionário
    response = requests.get(imgLink)
    file = open(pathImg+'/' + str(prod_code) +'.jpg', 'wb')
    file.write(response.content)
    file.close()


def pega_dado(url_produto):
    soup = carrega_pagina(url_produto)
    
    nome = soup.find('h1',{'class' : 'nx-product-stage__headline'}).text.strip()
    
    script = soup.find_all('script')
    for scr in script:
        if 'pageNameVirtual' in str(scr):
            datalayer = str(scr)
    #datalayer = datalayer.replace('\\','')

    ean = str(datalayer.split('productID\':"')[1].split('"')[0])
    
    descricao = soup.find('p',{'class' : 'nx-product-information__description'}).text.strip()
    
    
    imagem = soup.find('div',{'class' : 'nx-product-stage__item-360-wrapper'}).find('img')['src']
    salva_imagem(imagem,ean)
    
    img_code = ean + str('.jpg')
    
    
    info_dados = {
        'link' : url_produto,
        'ean' : ean,
        'nome' : nome,
        'descricao' : descricao,
        'link_imagem' : imagem,
        'img_code' : img_code
    }
    
    try:
        medidas = soup.find_all('span',{'class' : 'nx-pas__product-attribute'})
        count = 1
        for m in medidas:
            name_v = 'info ' + str(count)
            info_dados[name_v] = m.text.strip()
            count += 1
    except:
        pass
    
    cat = soup.find_all('li',{'class' : 'nx-breadcrumb__item'})
    categoria = cat[1].text.strip()
    count = 1
    info_dados['categoria'] = categoria
    for c in range(2,len(cat)-1):
        name_c = 'subcat_' + str(count)
        info_dados[name_c] = cat[c].text.strip()
        count += 1
        
    
    return info_dados



# Verificação/Criação do arquivo 'produtos versionados'
fileProdVer = Path(name_file_ProdVer) 

if fileProdVer.is_file():
    prod_versionado = pd.read_csv(name_file_ProdVer)
    
else:
    prod_versionado = pd.DataFrame(columns = col_Produtos)
    prod_versionado.to_csv(name_file_ProdVer, index = False)
    

# Executando
sam = sample(range(len(lidos)),20)
count = 1
#for i in sam:
#    print(count)
#    count += 1

for i in range(len(lidos)):
    
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    url_product = lidos.loc[i, 'Link']
    
    if  url_product in list(coletados.Link) or url_product in list(quebrados.Link):
        continue
        
    else:
        print(i)
        print(url_product)
        
    try:
        info_dados = pega_dado(url_product)
    except:       
        quebrados = pd.read_csv(fileQuebrados)
        quebrados = quebrados.append({
            'Data' : now,
            'Link' : url_product
        }, ignore_index = True)
        quebrados.to_csv(name_file_Quebrados, index = False)
        print('Algo de errado aconteceu!')
        continue
        
    coletados = pd.read_csv(fileColetados)
    coletados = coletados.append({
        'Data' : now,
        'Link' : url_product
    }, ignore_index = True)
    coletados.to_csv(name_file_Coletados, index = False)
    
    produtos = pd.read_csv(fileProdutos)
    produtos = produtos.append(info_dados, ignore_index=True)
    produtos.to_csv(name_file_Produtos, index = False)
    
    prod_versionado = pd.read_csv(name_file_ProdVer)
    prod_versionado = prod_versionado.append(info_dados, ignore_index=True)
    prod_versionado.to_csv(name_file_ProdVer, index = False)


