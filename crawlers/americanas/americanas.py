from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from time import sleep
from pathlib import Path
from bs4 import BeautifulSoup
import requests
import os
from datetime import datetime
import json
import pandas as pd
import re

#TOR
from selenium import webdriver
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
import os

#Diversos
from IPython.display import clear_output

from random import sample

url_homepage = 'https://www.americanas.com.br/'

col_Coletados = ['Data','EAN', 'Link']

col_Lidos = ['Data', 'EAN', 'Link']

col_Quebrados = ['Data', 'EAN', 'Link']

col_Produtos = ['ean', 'nome', 'marca', 'fabricante', 'cor', 'volume', 'img_link', 'img_codes']

global col_Produtos



# Data e Hora para o Versionamento
diversao = datetime.now()
diversao = str(diversao.year)+'-'+str(diversao.month)+'-'+str(diversao.day)




# Criação das Pastas
my_path = os.getcwd()
my_path = my_path.replace('\\','/')



# Criação da pasta 'leituras'
pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    print ("Diretorio %s já criado" % pathLeituras)
else:
    print ("Diretorio %s criado com sucesso " % pathLeituras)
    
    
    
# Criação da pasta 'imagens' 
pathImg = my_path+"/imagens/img "+diversao
try:
    os.makedirs(pathImg)
except OSError:
    print ("Diretorio %s já criado" % pathImg)
else:
    print ("Diretorio %s criado com sucesso " % pathImg)
    
    
    
# Pegar nome do site
name_site = url_homepage.split('.')[1]


# Criação dos nomes dos arquivos
name_file_Produtos = 'produtos_' + str(name_site) + '.csv'

name_file_Coletados = 'coletados_' + str(name_site) + '.csv'  # Dados coletados dos links

name_file_Lidos = 'lidos_' + str(name_site) + '.csv' # Lidos do site

name_file_Quebrados = 'quebrados_' + str(name_site) + '.csv' # Lidos do site

name_file_ProdVer = pathLeituras + '/' + diversao + '_' + 'produtos_' + str(name_site) + '.csv'


# Verificação/Criação do arquivo 'produtos'
fileProdutos = Path(my_path + '/' + name_file_Produtos) 

if fileProdutos.is_file():
    produtos = pd.read_csv(fileProdutos)
    
else:
    produtos = pd.DataFrame(columns = col_Produtos)
    produtos.to_csv(name_file_Produtos, index = False)
    

# Verificação/Criação do arquivo 'coletados'
fileColetados = Path(my_path + '/' + name_file_Coletados) 

if fileColetados.is_file():
    coletados = pd.read_csv(fileColetados)
    
else:
    coletados = pd.DataFrame(columns = col_Coletados)
    coletados.to_csv(name_file_Coletados, index = False)
    
    
    
# Verificação/Criação do arquivo 'linkLidos'
fileLidos = Path(my_path + '/' + name_file_Lidos) 

if fileLidos.is_file():
    lidos = pd.read_csv(fileLidos)
    
else:
    lidos = pd.DataFrame(columns = col_Lidos)
    lidos.to_csv(name_file_Lidos, index = False)
    
    
# Verificação/Criação do arquivo 'linkLidos'
fileQuebrados = Path(my_path + '/' + name_file_Quebrados) 

if fileQuebrados.is_file():
    quebrados = pd.read_csv(fileQuebrados)
    
else:
    quebrados = pd.DataFrame(columns = col_Quebrados)
    quebrados.to_csv(name_file_Quebrados, index = False)

# Create a new instance of the Firefox driver (or Chrome)
driver = webdriver.Chrome()

# Create a request interceptor
def interceptor(request):
    del request.headers['Referer']  # Delete the header first
    request.headers['Referer'] = 'some_referer'

# Set the interceptor on the driver
driver.request_interceptor = interceptor

# All requests will now use 'some_referer' for the referer
driver.get(url_homepage)

driver.maximize_window()

sleep(2)

soup = BeautifulSoup(driver.page_source)

def salva_imagens(imagens, pathImg, prod_code):

    # Coleta e download da imagem através do dicionário
    count = 1
    img_codes = ''
    for i in imagens:
        imgLink = i
        response = requests.get(imgLink)
        file = open(pathImg+'/' + str(prod_code) +'_'+str(count)+'.jpg', 'wb')
        file.write(response.content)
        file.close()
        if count != len(imagens):
            img_codes += str(prod_code) +'_'+str(count)+'.jpg;'
        else:
            img_codes += str(prod_code) +'_'+str(count)+'.jpg'
        count += 1
        sleep(0.5)
        
    return img_codes

def pega_dado(url_product):
    driver.get(url_product)
    sleep(1.5)

    soup = BeautifulSoup(driver.page_source)
    
    nome = soup.find('span', {'class' : 'src__Text-sc-154pg0p-0 product-title__Title-sc-1hlrxcw-0 hoBeMD'}).text

    detalhes = soup.findAll('td', {'class':'src__Text-sc-70o4ee-8 hayRRx'})
    descricao = soup.find('div', {'class':'src__Container-sc-13f3i2j-3 eUhMnK'}).text

    ean = ''
    marca = ''
    fabricante = ''
    cor = ''
    volume = ''
    img_link = ''
    imgs = []


    for i in range(len(detalhes)):
        if ean == '':
            ean = (detalhes[i+1].text if detalhes[i].text.replace(' ','') == 'Códigodebarras' else '')

        if marca == '':
            marca = (detalhes[i+1].text if detalhes[i].text == 'Marca' else '')

        if fabricante == '':
            fabricante = (detalhes[i+1].text if detalhes[i].text == 'Fabricante' else '')

        if cor == '':
            cor = (detalhes[i+1].text if detalhes[i].text == 'Cor' else '')

        if volume == '':
            volume = (detalhes[i+1].text.replace(' ','') if detalhes[i].text.replace(' ','') == 'Volume(ml)' else '')

            if volume == '':
                volume = (detalhes[i+1].text.replace(' ','') if detalhes[i].text.replace(' ','') == 'Capacidadeemvolume' else '')


    try:
        count = 1
        imagens = soup.find('div', {'class' : 'thumb-gallery__Container-sc-1cm9lgf-0 cgyBix'}).findAll('div', {'class' : 'thumb__Container-sc-183f83g-0'})
        for i in imagens:
            if count != len(imagens):
                img_link = img_link + i['src'] + ';'
            else:
                img_link = img_link + i['src']
            imgs.append(i['src'])
            count+= 1

    except:
        img_link = soup.find('div', {'class' : 'src__Container-dda50e-0 ihwNf'}).find('img', {'loading' : 'lazy'})['src']
        imgs.append(img_link)
        
        
    img_codes = salva_imagens(imgs, pathImg, ean)

    info_dados = {
        'ean' : ean,
        'nome' : nome,
        'marca' : marca,
        'fabricante' : fabricante,
        'cor' : cor,
        'volume' : volume,
        'img_link' : img_link,
        'img_codes' : img_codes
    }
        
        
    return info_dados
        