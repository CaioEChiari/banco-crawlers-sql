from libs.crawler_modelo import Crawler_Modelo
from libs.banco_de_dados import Banco_De_Dados
from datetime import datetime
from bs4 import BeautifulSoup
from pathlib import Path
import urllib.parse
import requests
import json

class Zee_Dog(Crawler_Modelo):
    def pegar_urls_produtos(self):
        lidos = []

        page_number = 1
        while True:
            url = f'https://www.zeedog.com.br/buscapagina?PS=48&cc=12&sm=0&PageNumber={page_number}&fq=H:144&sl=8d721571-7da0-4577-ab79-ded71f38bbd5&O=OrderByReleaseDateDESC'
            response = requests.get(url)
            soup = BeautifulSoup(response.text, 'html.parser')
            vitrine = soup.find_all('li', {'layout': '8d721571-7da0-4577-ab79-ded71f38bbd5'})

            if page_number == 3:
                break

            if len(vitrine) == 0:
                break

            for vit in vitrine:
                link_produto = vit.find('a')['href']

                if link_produto in lidos:
                    continue

                lidos.append(link_produto)

            page_number += 1
            print(len(lidos))

            return lidos

    def pegar_dados_produtos(self, urls, cur):
        for url in urls:
            response = requests.get(url)
            soup = BeautifulSoup(response.text, 'html.parser')

            scripts = soup.find_all('script')
            for script in scripts:
                if 'vtex.events.addData' in str(script):
                    addData = str(script)
                if 'var skuJson_0' in str(script):
                    skuJson = str(script)

            addData = addData.split('vtex.events.addData(')[1].split(');\n</script')[0]
            d_addData = json.loads(addData)

            skuJson = skuJson.split('skuJson_0 = ')[1].split(';CATALOG_SD')[0]
            d_skuJson = json.loads(skuJson)

            productId = d_addData['productId']
            referencia = d_addData['productReferenceId']
            nome = d_addData['productName']
            marca = d_addData['productBrandName']

            try:
                eans = d_addData['productEans']
            except:
                pass

            skus_eans = list(d_addData['skuStocks'].keys())
            skus = d_skuJson['skus']

            url = f'https://www.zeedog.com.br/api/catalog_system/pub/products/search/?fq=productId:{productId}'
            response = requests.get(url)
            d_response = response.json()

            detalhes = BeautifulSoup(d_response[0]['Atributos_Especiais'][0], 'html.parser')
            detalhes = detalhes.text.strip().replace('\n', ' ')
            descricao = d_response[0]['Descricao_Produto'][0]

            imagens = []
            for item in d_response[0]['Fotos_Produto'][0].split(';\r\n'):
                try:
                    img_link = item.split("['urlImagem':'")[1].split("',")[0]
                except:
                    continue

                if img_link in imagens:
                    continue
                else:
                    imagens.append(img_link)
            imagens = ';'.join(imagens)

            for i in range(len(skus_eans)):
                try:
                    ean = eans[i]
                except:
                    ean = ''
                sku = skus_eans[i]
                for s in skus:
                    if str(sku) == str(s['sku']):
                        tamanho = s['dimensions']['Tamanho'].strip()
                        skuname = s['skuname']

                campos_obrigatorios = self.bd.criar_dicionario_campos_obrigatorios()
                campos_adicionais = {}
                campos_obrigatorios['url'] = url
                campos_obrigatorios['data_coleta'] = str(datetime.today())
                campos_obrigatorios['referencia'] = referencia
                campos_obrigatorios['product_id'] = productId
                campos_obrigatorios['ean'] = ean
                campos_obrigatorios['sku'] = sku
                campos_obrigatorios['nome_crawler'] = nome
                campos_obrigatorios['marca'] = marca
                campos_obrigatorios['tamanho'] = tamanho
                campos_obrigatorios['descricao'] = descricao
                campos_obrigatorios['imagens'] = imagens
                
                campos_adicionais['detalhes'] = detalhes
                campos_adicionais['skuname'] = skuname

                self.bd.adicionar_dados_obrigatorios_produto_bd(cur, campos_obrigatorios, campos_adicionais)
