from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from time import sleep
from pathlib import Path
from bs4 import BeautifulSoup
import os
from datetime import date
from datetime import datetime
import json
import pandas as pd
import re
import requests

#TOR
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile

#Diversos
from IPython.display import clear_output
from random import sample

url_homepage = 'https://www.drogariasaopaulo.com.br/'
col_Coletados = ['Data','Link']
col_Lidos = ['Data', 'Categoria','Link']
col_Quebrados = ['Data', 'Link']
col_Produtos = [
        'departamento',
        'categoria',
        'sku',
        'ean',
        'nome',
        'marca',
        'descricao',
        'principio ativo',
        'registro ms',
        'tipo de medicamento',
        'links imagem',
        'códigos imagem']

# Data e Hora para o Versionamento
diversao = date.today().strftime('%Y-%m-%d')

# Criação das Pastas
my_path = os.getcwd()
my_path = my_path.replace('\\','/')

# Criação da pasta 'leituras'
pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    print ("Diretorio %s já criado" % pathLeituras)
else:
    print ("Diretorio %s criado com sucesso " % pathLeituras)

# Criação da pasta 'imagens' 
pathImg = my_path+"/imagens/img "+diversao
try:
    os.makedirs(pathImg)
except OSError:
    print ("Diretorio %s já criado" % pathImg)
else:
    print ("Diretorio %s criado com sucesso " % pathImg)
    
# Pegar nome do site
name_site = url_homepage.split('.')[1]

# Criação dos nomes dos arquivos
name_file_Produtos = 'produtos_' + str(name_site) + '.csv'

name_file_Coletados = 'coletados_' + str(name_site) + '.csv'  # Dados coletados dos links

name_file_Lidos = 'lidos_' + str(name_site) + '.csv' # Lidos do site

name_file_Quebrados = 'quebrados_' + str(name_site) + '.csv' # Lidos do site

name_file_ProdVer = pathLeituras + '/' + diversao + '_' + 'produtos_' + str(name_site) + '.csv'

# Verificação/Criação do arquivo 'produtos'
fileProdutos = Path(my_path + '/' + name_file_Produtos) 

if fileProdutos.is_file():
    produtos = pd.read_csv(fileProdutos)
else:
    produtos = pd.DataFrame(columns = col_Produtos)
    produtos.to_csv(name_file_Produtos, index = False)
    

# Verificação/Criação do arquivo 'coletados'
fileColetados = Path(my_path + '/' + name_file_Coletados) 

if fileColetados.is_file():
    coletados = pd.read_csv(fileColetados)
else:
    coletados = pd.DataFrame(columns = col_Coletados)
    coletados.to_csv(name_file_Coletados, index = False)

# Verificação/Criação do arquivo 'linkLidos'
fileLidos = Path(my_path + '/' + name_file_Lidos) 

if fileLidos.is_file():
    lidos = pd.read_csv(fileLidos)
else:
    lidos = pd.DataFrame(columns = col_Lidos)
    lidos.to_csv(name_file_Lidos, index = False)
    
# Verificação/Criação do arquivo 'linkLidos'
fileQuebrados = Path(my_path + '/' + name_file_Quebrados) 

if fileQuebrados.is_file():
    quebrados = pd.read_csv(fileQuebrados)
    
else:
    quebrados = pd.DataFrame(columns = col_Quebrados)
    quebrados.to_csv(name_file_Quebrados, index = False)

torexe = os.popen(r'C:\Users\Artur\Documents\Tor Browser\Browser\TorBrowser\Tor\tor.exe')

profile = FirefoxProfile(r'C:\Users\Artur\Documents\Tor Browser\Browser\TorBrowser\Data\Browser\profile.default')
profile.set_preference('network.proxy.type', 1)
profile.set_preference('network.proxy.socks', '127.0.0.1')
profile.set_preference('network.proxy.socks_port', 9050)
profile.set_preference("network.proxy.socks_remote_dns", False)
profile.update_preferences()

driver = webdriver.Firefox(firefox_profile= profile, executable_path=r'C:\Users\Artur\Documents\Tor Browser\geckodriver.exe')
driver.get(url_homepage)
driver.maximize_window()

def carrega_pagina(url, sleep_time = 2):
    driver.get(url)
    sleep(sleep_time)
    soup = BeautifulSoup(driver.page_source)
    
    return soup

fileCategorias = Path(my_path + '/' + 'categorias_' + str(name_site) + '.csv') 
if  fileCategorias.is_file():
    my_cats = pd.read_csv(fileCategorias)
else:
    my_cats = pd.DataFrame(columns = ['Categoria','Link'])
    my_cats.to_csv('categorias_' + str(name_site) + '.csv', index = False)

soup = carrega_pagina(url_homepage,2)

menu = soup.find('div',{'class' : 'col-12 rnk-componente-departamentos-em-destaque'}).find_all('li')

categorias = []

for i in menu:
    categorias.append('https://www.drogariasaopaulo.com.br' + i.find('a')['href']) 

for i in categorias:
    try:
        soup = carrega_pagina(i)
  
        single_navigator = soup.find('div',{'class' : 'search-single-navigator'})
        ul = single_navigator.find_all('ul')

        for z in ul:
            a = z.find_all('a')
            for y in a:
                if not 'specificationFilter' in str(y['href']):
                    my_cat_title = y['title']
                    my_cat_link = y['href']

                    if my_cat_link in list(my_cats.Link):
                        continue
                    else:
                        my_cats = pd.read_csv(fileCategorias)
                        my_cats = my_cats.append({
                            'Categoria' : my_cat_title,
                            'Link' : my_cat_link
                        }, ignore_index = True)

                        my_cats.to_csv('categorias_' + str(name_site) + '.csv', index = False)
                        
    except:
        continue

def xpath_soup(element):
    components = []
    child = element if element.name else element.parent
    for parent in child.parents:  # type: bs4.element.Tag
        siblings = parent.find_all(child.name, recursive=False)
        components.append(
            child.name if 1 == len(siblings) else '%s[%d]' % (
                child.name,
                next(i for i, s in enumerate(siblings, 1) if s is child)
                )
            )
        child = parent
    components.reverse()
    return '/%s' % '/'.join(components)

def pega_produto(categoria, prateleira):
    global lidos   
    
    for i in prateleira:
        vitrine = i.find_all('li')
        for vit in vitrine:
            try:
                link_produto = vit.find('div',{'class' : 'img-prateleira'}).find('a')['href']
            except:
                continue
           
            if link_produto in list(lidos.Link):
                continue

            else:
                now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

                lidos = pd.read_csv(fileLidos)

                lidos = lidos.append({
                    'Data' : now,
                    'Categoria' : categoria,
                    'Link' : link_produto
                }, ignore_index = True)

                lidos.drop_duplicates(subset=['Link'], inplace=True)
                lidos.to_csv(name_file_Lidos, index = False)

        clear_output(wait=True)
        print(categoria)
        print(len(vitrine))
        print('Total de produtos: ' + str(len(lidos)))
        print(lidos.Categoria.value_counts())

# Verificação/Criação do arquivo 'paginas_lidas'
filePaginasLidas = Path(my_path + '/' + 'paginas_lidas_' + str(name_site) + '.csv') 

if  filePaginasLidas.is_file():
    paginas_lidas = pd.read_csv(filePaginasLidas)
else:
    paginas_lidas = pd.DataFrame(columns = ['Data', 'Categoria','Link_Pagina'])
    paginas_lidas.to_csv('paginas_lidas_' + str(name_site) + '.csv', index = False)
    
    
for i in range(len(my_cats)):
    prat_count = 0
    my_link = my_cats.Link[i]
    
    if my_link in list(paginas_lidas.Link_Pagina):
        continue
    try:
        soup = carrega_pagina(my_link)
    except:
        continue
    
    y = 0
    
    while y != 10:
        driver.execute_script("window.scrollTo(0, window.scrollY + 300)")
        sleep(1)
        soup = BeautifulSoup(driver.page_source)
            
        prateleira = soup.find_all('div',{'class' : re.compile('^prateleira vitrine default n')})
            
        pega_produto(my_cats.Categoria[i], prateleira)
                   
        try:
            b = soup.find('button',{'class' : 'btn btn-primary'})
            driver.find_element_by_xpath(xpath_soup(b)).click()
            y = 0
        except:
            y += 1
                
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    paginas_lidas = pd.read_csv(filePaginasLidas)
    paginas_lidas = paginas_lidas.append({
        'Data' : now,
        'Link_Pagina' : my_link
    }, ignore_index = True)
    paginas_lidas.to_csv('paginas_lidas_' + str(name_site) + '.csv', index = False)

def salva_imagens(imagens, prod_code):
    global pathImg
    # Coleta e download da imagem através do dicionário
    count = 1
    img_codes = ''
    for imgLink in imagens:
        response = requests.get(imgLink)
        file = open(pathImg+'/' + str(prod_code) +'_'+str(count)+'.jpg', 'wb')
        file.write(response.content)
        file.close()
        img_codes += str(prod_code) +'_'+str(count)+'.jpg;'      
        count += 1
        sleep(1)
        
    img_codes = img_codes[:-1]
    
    return img_codes

def pega_dado(url):
    soup = carrega_pagina(url)
    
    vtex = soup.find_all('script')
    for v in vtex:
        if 'vtex.events.addData' in str(v):
            addData = str(v)

    addData = addData.split('.addData(')[1].split(');\n')[0]
    d = json.loads(addData)
    
    sku = d['productId']
    
    try:
        ean = d['productEans'][0]
        int(ean)
    except:
        ean = ''    
    
    nome = d['productName']
    marca = d['productBrandName']
    departamento = d['productDepartmentName']
    categoria = d['productCategoryName']
    
    descricao = soup.find('div',{'class' : 'productDescription'}).text
    descricao = str(descricao.split('Princípio Ativo')[0])
    
    prin_ativo = ''
    reg_ms = ''
    tipo_med = ''
    mt_3 = soup.find_all('p',{'class','mt-3'})
    for i in mt_3:
        if 'Princípio Ativo: ' in i.text:
            prin_ativo = i.text.split('Princípio Ativo: ')[1]
        if 'Registro MS: ' in i.text:
            reg_ms = i.text.split('Registro MS: ')[1]
        if 'Tipo de Medicamento: ' in i.text:
            tipo_med = i.text.split('Tipo de Medicamento: ')[1]
            
    apresentacao = soup.find('div',{'class' : 'apresentacao'})
    li = apresentacao.find('ul',{'class' : 'thumbs'}).find_all('li')

    imagens = []
    img_links = ''
    for i in li:
        imagens.append(i.find('a')['rel'][0].split('?v=')[0])
        img_links += str(i.find('a')['rel'][0].split('?v=')[0]) + ';'
    img_links = img_links[:-1]
    
    img_codes = salva_imagens(imagens, ean)
    
    info_dados = {
        'departamento' : departamento,
        'categoria' : categoria,
        'sku' : str(sku),
        'ean' : str(ean),
        'nome' : nome,
        'marca' : marca,
        'descricao' : descricao,
        'principio ativo' : prin_ativo,
        'registro ms' : reg_ms,
        'tipo de medicamento' : tipo_med,
        'links imagem' : img_links,
        'códigos imagem' : img_codes
    }
    
    return info_dados

# Verificação/Criação do arquivo 'produtos versionados'
fileProdVer = Path(name_file_ProdVer) 

if fileProdVer.is_file():
    prod_versionado = pd.read_csv(name_file_ProdVer)
    
else:
    prod_versionado = pd.DataFrame(columns = col_Produtos)
    prod_versionado.to_csv(name_file_ProdVer, index = False)
    

# Executando
#sam = sample(range(len(lidos)),50)
#count = 1
#for i in sam:
#    print(count)
#    count += 1

for i in range(len(lidos)):
    
    now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    url_product = lidos.loc[i, 'Link']
    
    if  url_product in list(coletados.Link) or url_product in list(quebrados.Link):
        continue
        
    else:
        print(i)
        print(url_product)
        
    try:
        info_dados = pega_dado(url_product)
    except:       
        quebrados = pd.read_csv(fileQuebrados)
        quebrados = quebrados.append({
            'Data' : now,
            'Link' : url_product
        }, ignore_index = True)
        quebrados.to_csv(name_file_Quebrados, index = False)
        print('Algo de errado aconteceu!')
        continue
        
    coletados = pd.read_csv(fileColetados)
    coletados = coletados.append({
        'Data' : now,
        'Link' : url_product
    }, ignore_index = True)
    coletados.to_csv(name_file_Coletados, index = False)
    
    produtos = pd.read_csv(fileProdutos)
    produtos = produtos.append(info_dados, ignore_index=True)
    produtos.to_csv(name_file_Produtos, index = False)
    
    prod_versionado = pd.read_csv(name_file_ProdVer)
    prod_versionado = prod_versionado.append(info_dados, ignore_index=True)
    prod_versionado.to_csv(name_file_ProdVer, index = False)