from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from time import sleep
from pathlib import Path
from bs4 import BeautifulSoup
import requests
import csv
import os
from datetime import datetime
import json
import pandas as pd

from IPython.display import clear_output

from random import sample


url_homepage = 'https://www.hering.com.br/'

col_Coletados = ['Data','Categoria', 'Link']

col_Lidos = ['Data', 'Categoria', 'Link']

col_Quebrados = ['Data', 'Categoria', 'Link']

col_Produtos = ['sku[cod_produto]', 'nome', 'marca', 'categoria', 'tamanho', 'genero', 'cor',
                'peso', 'composicao', 'preco', 'preco_parc', 'qtd_parc', 'imagens', 'descricao', 'cuidados']

global col_Produtos


# Data e Hora para o Versionamento
diversao = datetime.now()
diversao = str(diversao.year)+'-'+str(diversao.month)+'-'+str(diversao.day)




# Criação das Pastas
my_path = os.getcwd()
my_path = my_path.replace('\\','/')



# Criação da pasta 'leituras'
pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    print ("Diretorio %s já criado" % pathLeituras)
else:
    print ("Diretorio %s criado com sucesso " % pathLeituras)
    
    
    
# Criação da pasta 'imagens' 
pathImg = my_path+"/imagens/img "+diversao
try:
    os.makedirs(pathImg)
except OSError:
    print ("Diretorio %s já criado" % pathImg)
else:
    print ("Diretorio %s criado com sucesso " % pathImg)


# Pegar nome do site
name_site = url_homepage.split('.')[1]


# Criação dos nomes dos arquivos
name_file_Produtos = 'produtos_' + str(name_site) + '.csv'

name_file_Coletados = 'coletados_' + str(name_site) + '.csv'  # Dados coletados dos links

name_file_Lidos = 'lidos_' + str(name_site) + '.csv' # Lidos do site

name_file_Quebrados = 'quebrados_' + str(name_site) + '.csv' # Lidos do site

name_file_ProdVer = pathLeituras + '/' + diversao + '_' + 'produtos_' + str(name_site) + '.csv'


# Verificação/Criação do arquivo 'produtos'
fileProdutos = Path(my_path + '/' + name_file_Produtos) 

if fileProdutos.is_file():
    produtos = pd.read_csv(fileProdutos)
    
else:
    produtos = pd.DataFrame(columns = col_Produtos)
    produtos.to_csv(name_file_Produtos, index = False)
    

# Verificação/Criação do arquivo 'coletados'
fileColetados = Path(my_path + '/' + name_file_Coletados) 

if fileColetados.is_file():
    coletados = pd.read_csv(fileColetados)
    
else:
    coletados = pd.DataFrame(columns = col_Coletados)
    coletados.to_csv(name_file_Coletados, index = False)
    
    
    
# Verificação/Criação do arquivo 'linkLidos'
fileLidos = Path(my_path + '/' + name_file_Lidos) 

if fileLidos.is_file():
    lidos = pd.read_csv(fileLidos)
    
else:
    lidos = pd.DataFrame(columns = col_Lidos)
    lidos.to_csv(name_file_Lidos, index = False)
    
    
# Verificação/Criação do arquivo 'linkLidos'
fileQuebrados = Path(my_path + '/' + name_file_Quebrados) 

if fileQuebrados.is_file():
    quebrados = pd.read_csv(fileQuebrados)
    
else:
    quebrados = pd.DataFrame(columns = col_Quebrados)
    quebrados.to_csv(name_file_Quebrados, index = False)


service = Service('C:/Program Files/Google/Chrome/Application/chromedriver')
service.start()
driver = webdriver.Remote(service.service_url)
driver.get(url_homepage)
driver.maximize_window() # Maximizar a tela para aparecer os menus

sleep(2)

soup = BeautifulSoup(driver.page_source)


links_menu = pd.DataFrame(columns = ['Categoria', 'Link'])

for i in href_menu:
    if '/d/' in i['href']:
        links_menu = links_menu.append({
            'Categoria' : i.text,
            'Link' : 'https://www.hering.com.br'+i['href']
        },ignore_index = True)

# Pega Links de Produtos
for i in range(202, len(links_menu)):
    
    page_count = 0
    
    vitrine = []
    
    aux = ''
    
    while True:
        driver.get(links_menu.iloc[i, 1]+'?&page='+str(page_count))
        sleep(2)
        
        soup = BeautifulSoup(driver.page_source)
        
        vitrine = soup.findAll('div', {'class' : 'box-product__footer'})
        
        if str(type(vitrine)) == "<class 'NoneType'>" or len(vitrine) == 0:
            break;
            
        if page_count == 300:
            break
            
        if aux == '':
            aux = vitrine
        elif aux == vitrine:
            aux = ''
            break
        else:
            aux = ''
            
        lidos = pd.read_csv(fileLidos)
            
        for vit in vitrine:
            link_produto = url_homepage[:-1]+vit.find('a', href = True)['href']
                      
            if link_produto in list(lidos.Link):
                continue
            
            else:
                now = datetime.now()
                now = str(now.year)+'-'+str(now.month)+'-'+str(now.day)+'-'+str(now.hour)+'h-'+str(now.minute)+'m'

                lidos = lidos.append({
                    'Data' : now,
                    'Categoria' : links_menu.iloc[i, 0],
                    'Link' : link_produto
                }, ignore_index = True)

                lidos.to_csv(name_file_Lidos, index = False)
                

        page_count += 1
        
        # lidos = pd.read_csv(fileLidos)            
        lidos.drop_duplicates(subset=['Link'], inplace=True)
        lidos.to_csv(name_file_Lidos, index = False)
        clear_output(wait=True)
        print(str(i) + ' de ' + str(len(links_menu)))
        print(len(vitrine))
        print(lidos.Categoria.value_counts())
        
def salva_imagens(imagens, pathImg, prod_code):

    # Coleta e download da imagem através do dicionário
    count = 1
    img_codes = ''
    for i in imagens:
        imgLink = i
        response = requests.get(imgLink)
        file = open(pathImg+'/' + str(prod_code) +'_'+str(count)+'.jpg', 'wb')
        file.write(response.content)
        file.close()
        img_codes += str(prod_code) +'_'+str(count)+'.jpg; '
        count += 1
        sleep(0.5)
        
    return img_codes


def pega_dado(url_product):
    driver.get(url_product)
    sleep(1.5)

    soup = BeautifulSoup(driver.page_source)
    
# Parte 1 ------------------------------------------------------
    detalhes = soup.find('div', {'class' : 'productData'}).findAll('input')
     
    nome = [x['value'] for x in detalhes if ('productName' in x['class'])]
    nome = ('' if nome == [] else nome[0])
    
    descricao = [x['value'] for x in detalhes if ('productDescription' in x['class'])]
    descricao = ('' if descricao == [] else descricao[0])
    
    preco = [x['value'] for x in detalhes if ('productValue' in x['class'])]
    preco = ('' if preco == [] else preco[0])
    
    categoria = [x['value'] for x in detalhes if ('productCategories' in x['class'])]
    categoria = ('' if categoria == [] else categoria[0])
    
    genero = [x['value'] for x in detalhes if ('productGender' in x['class'])]
    genero = ('' if genero == [] else genero[0])
    
    cor = soup.find('meta', {'itemprop' : 'color'})['content']
    
    marca = soup.find('input', {'type' : 'hidden', 'class' : '_brand'})['value']
    
# Parte 2 ------------------------------------------------------
    detalhes = soup.find('ul', {'class' : 'features'}).findAll('span')
    
    flag_p = 0
    flag_c = 0

    for i in range(0, len(detalhes)):
        if 'Peso' in detalhes[i].text:
            flag_p = 1
            try:
                peso = detalhes[i+1].text
            except:
                peso = ''
                composicao = ''
                break

        if 'Comp' in detalhes[i].text:
            flag_c = 1
            try:
                composicao = detalhes[i+1].text
            except:
                peso = ''
                composicao = ''
                break

        if flag_p == 0:
            peso = ''

        if flag_p == 0:
            composicao = ''
            
# Parte 3 ------------------------------------------------------
    try:
        detalhes = soup.find('ul', {'class' : 'conservations'}).findAll('p', {'class' : 'text'})
        cuidados = ''

        for i in detalhes:
            cuidados = cuidados + i.text.strip() + ' '
    except:
        cuidados = ''
        

# Parte 4 ------------------------------------------------------
    try:
        detalhes = soup.find('div', {'class' : 'box-sizes d-sm-flex align-items-center'}).findAll('li')
    
        cod_prods = []
        tamanhos = []

        for i in detalhes:
            cod_prods.append(i['data-variantcode'])
            tamanhos.append(i.find('a').text)
            
    except:
        detalhes = soup.find('div', {'class' : 'product-code mobile-product-code'}) 
        tamanhos = ['']
        cod_prods = [detalhes.text.strip()]
        

        
# Parte 5 ----------------------------------------------------
    detalhes = soup.find('span', {'class' : 'installments'})
    try:
        valor_parc = detalhes.text.split('\n')[2].strip()
        parcelas = detalhes.text.split('\n')[1]
    except:
        valor_parc = ''
        parcelas = ''
        
# Parte 6 ----------------------------------------------------
    detalhes = soup.find('div', {'class' : 'slick-list draggable'}).findAll('img')
    imagens = []
    for i in detalhes:
        try: # no carrocel de imagens tem vídeos
            imagens.append('https:' + str(i['data-src']).split('?')[0])
        except:
            continue
        
    img_name = cod_prods[0]
    img_codes = salva_imagens(imagens, pathImg, img_name)
        

# Parte 7 ----------------------------------------------------    
    info_prod = pd.DataFrame(columns = col_Produtos)
    
    for i in range(len(tamanhos)): 
        info_prod_dict = {
            'sku[cod_produto]' : str(cod_prods[i]),
            'nome' : str(nome),
            'marca' : str(marca),
            'categoria' : str(categoria),
            'tamanho' : str(tamanhos[i]),
            'genero' : str(genero),
            'cor' : str(cor),
            'peso' : str(peso),
            'composicao' : str(composicao),
            'preco' : str(preco),
            'preco_parc' : str(valor_parc),
            'qtd_parc' : str(parcelas),
            'imagens' : str(img_codes),
            'descricao' : str(descricao),
            'cuidados' : str(cuidados),
        }
        
        info_prod = info_prod.append(info_prod_dict, ignore_index = True)
        
        
    return info_prod


# Verificação/Criação do arquivo 'produtos versionados'
fileProdVer = Path(name_file_ProdVer) 

if fileProdVer.is_file():
    prod_versionado = pd.read_csv(name_file_ProdVer)
    
else:
    prod_versionado = pd.DataFrame(columns = col_Produtos)
    prod_versionado.to_csv(name_file_ProdVer, index = False)
    
#sam = sample(range(len(lidos)),100)
    
    
# Executando
for i in range(len(lidos)):
#for i in sam:
    
    now = datetime.now()
    now = str(now.year)+'-'+str(now.month)+'-'+str(now.day)+'-'+str(now.hour)+'h-'+str(now.minute)+'m'
        
    if lidos.loc[i, 'Link'] in list(coletados.Link):
        continue
        
    else:
        url_product = lidos.loc[i, 'Link']
        print(i)
        print(url_product)
        
    try:
        info_dados = pega_dado(url_product)
    except:       
        quebrados = pd.read_csv(fileQuebrados)
        quebrados = quebrados.append({
            'Data' : now,
            'Categoria' : lidos.loc[i, 'Categoria'],
            'Link' : lidos.loc[i, 'Link']
        }, ignore_index = True)
        quebrados.to_csv(name_file_Quebrados, index = False)
        print('Algo de errado aconteceu!')
        continue
        

    
    
    coletados = pd.read_csv(fileColetados)
    coletados = coletados.append({
        'Data' : now,
        'Categoria' : lidos.loc[i, 'Categoria'],
        'Link' : lidos.loc[i, 'Link']
    }, ignore_index = True)
    coletados.to_csv(name_file_Coletados, index = False)
    
    produtos = pd.read_csv(fileProdutos)
    produtos = produtos.append(info_dados, ignore_index=True)
    produtos.to_csv(name_file_Produtos, index = False)
    
    prod_versionado = pd.read_csv(name_file_ProdVer)
    prod_versionado = prod_versionado.append(info_dados, ignore_index=True)
    prod_versionado.to_csv(name_file_ProdVer, index = False)