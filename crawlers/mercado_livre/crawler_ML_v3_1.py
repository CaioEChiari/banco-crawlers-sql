from bs4 import BeautifulSoup
import requests
import csv
from pandas import read_csv

def pega_dado(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    
    nome = soup.find('h1',{'class' : 'ui-pdp-title'}).text
    #item_id = soup.find('input',{'name' : 'item_id'})['value']
    
    ul = soup.find('ul',{'class' : 'andes-breadcrumb'})
    a = ul.find_all('a')
    categoria = a[0].text
    sub_cat_1 = a[1].text
    sub_cat_2 = a[2].text
    
    try:
        sub_cat_3 = a[3].text
    except:
        sub_cat_3 = ''
    
    try:
        sub_cat_4 = a[4].text
    except:
        sub_cat_4 = ''
        
    link_imagens = ''
    figure = soup.find_all('figure',{'class' : 'ui-pdp-gallery__figure'})
    for i in figure:
        try:
            link_imagens += str(i.find('img')['data-zoom']) + ';'
        except:
            continue
    link_imagens = link_imagens[:-1]
    
    try:
        descricao = soup.find('p',{'class', 'ui-pdp-description__content'}).text
    except:
        descricao = ''
        
    script = soup.find_all('script')
    for scr in script:
        if 'window.__PRELOADED_STATE' in str(scr):
            my_script = str(scr)
    my_list = my_script.split('"')
    #marca = my_list[my_list.index('brandId') + 2]
    item_id = my_list[my_list.index('itemId') + 2]
    
    info_dados = {
        'categoria' : categoria,
        'sub categoria 1' : sub_cat_1,
        'sub categoria 2' : sub_cat_2,
        'sub categoria 3' : sub_cat_3,
        'sub categoria 4' : sub_cat_4,
        'item_id' : item_id,
        'nome' : nome,
        'descricao' : descricao,
        'link imagens' : link_imagens
    }
    
    tr = soup.find_all('tr',{'class' : 'andes-table__row'})

    for i in tr:
        th = i.find_all('th')
        td = i.find_all('td')

        if len(td) >= len(tr):
            for y in range(len(tr)):
                info_dados[th[y].text] = td[y].text
        else:
            for y in range(len(td)):
                info_dados[th[y].text] = td[y].text
                
    return info_dados



print("Bem-vindo ao Extrator de dados do Mercado Livre!\n")
print('Este programa recebe uma lista de produtos do Mercado Livre (produtos.csv) e retorna uma planilha com os dados deles (dados_ML.csv).\n')

print('Instrução 01:')
print('A lista com os links dos produtos deve estar salva no mesmo diretório que este executável em um arquivo chamado "produtos.csv".')

print('\nInstrução 02:')
print('O arquivo de saída "dados_ML.csv" deve estar fechado antes da execução.')

print('\nAntes de começar, o arquivo "produtos.csv" está no mesmo diretório que este executável?')
resposta = input('Digite S para Sim ou N para não. ')
print('\nO arquivo "dados_ML.csv" está fechado?')
resposta2 = input('Digite S para Sim ou N para não. ')

if (resposta == 'S' or resposta == 's') and (resposta2 == 'S' or resposta2 == 's'):
    print('\nAguarde! Processando ...\n')
    try:
        with open('dados_ML.csv', 'x') as f:
            f.write('categoria,sub categoria 1,sub categoria 2,sub categoria 3,sub categoria 4,item_id,nome,descricao,link imagens')
            mensagem = '\nO arquivo "dados_ML.csv" foi criado com sucesso! \nPressione ENTER para fechar.'
    except:
        mensagem = '\nO arquivo "dados_ML.csv" foi atualizado com sucesso! \nPressione ENTER para fechar.'

    df = read_csv('dados_ML.csv')
    
    try:
        count = 1
        with open('produtos.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                print(count)
                print(row[0])
                try:
                    my_dict = pega_dado(row[0])
                except:
                    try:
                        with open('erros.csv', 'x') as f:
                            f.write('link quebrado')
                    except:
                        pass
                    dr = read_csv('erros.csv')
                    dr = dr.append({'link quebrado' : row[0]}, ignore_index = True)
                    dr.to_csv('erros.csv', index = False)
                    print('Algo de errado aconteceu!\n')
                    count += 1
                    continue
                          
                if my_dict['item_id'] in list(df.item_id):
                    print('Produto já foi lido.\n')
                    count += 1
                    continue
                df = df.append(pega_dado(row[0]), ignore_index = True)
                print('Dados extraídos.\n')
                count += 1
        df.to_csv('dados_ML.csv',index = False)

        final = input(mensagem)
    except Exception as e:
        if 'Errno 2' in getattr(e, 'message', str(e)):
            final = input('Erro!\nO arquivo "produtos.csv" não foi encontrado.\nPressione ENTER para fechar.')
        if 'Errno 13' in getattr(e, 'message', str(e)):
            final = input('Erro!\nO arquivo "dados_ML.csv" deve ser fechado antes da execução.\nPressione ENTER para fechar.')
    
else:
    final = input('\nFavor verificar as Instruções 01 e 02 no início da execução.\nPressione ENTER para fechar.')
        