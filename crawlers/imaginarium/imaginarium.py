from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from time import sleep
from pathlib import Path
from bs4 import BeautifulSoup
import requests
import csv
import os
from datetime import datetime
import json
import pandas as pd

url_homepage = 'https://loja.imaginarium.com.br/'

col_Coletados = ['Data','Categoria', 'Link']

col_Lidos = ['Data', 'Categoria', 'Link']

col_Produtos = ['ean', 'sku', 'referencia', 'nome', 'id_produto', 'marca', 'id_marca', 'categoria', 'id_categoria',
               'departamento', 'id_departamento', 'cor', 'funcao', 'largura_cm', 'altura_cm', 'comprimento_cm',
               'profundidade', 'capacidade', 'embalagem', 'materia_prima', 'info_complementares', 'instru_conservacao',
               'exclusivo', 'voltagem', 'diametro', 'preco', 'preco_promo', 'imagens', 'descricao']

# Data e Hora para o Versionamento
diversao = datetime.now()
diversao = str(diversao.year)+'-'+str(diversao.month)+'-'+str(diversao.day)




# Criação das Pastas
my_path = os.getcwd()
my_path = my_path.replace('\\','/')

# Criação da pasta 'leituras'
pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    print ("Diretorio %s já criado" % pathLeituras)
else:
    print ("Diretorio %s criado com sucesso " % pathLeituras)
    
    
    
# Criação da pasta 'imagens' 
pathImg = my_path+"/imagens/img "+diversao
try:
    os.makedirs(pathImg)
except OSError:
    print ("Diretorio %s já criado" % pathImg)
else:
    print ("Diretorio %s criado com sucesso " % pathImg)

# Pegar nome do site
name_site = url_homepage.split('.')[1]


# Criação dos nomes dos arquivos
name_file_Produtos = 'produtos_' + str(name_site) + '.csv'

name_file_Coletados = 'coletados_' + str(name_site) + '.csv'  # Dados coletados dos links

name_file_Lidos = 'lidos_' + str(name_site) + '.csv' # Lidos do site

name_file_ProdVer = pathLeituras + '/' + diversao + '_' + 'produtos_' + str(name_site) + '.csv'


# Verificação/Criação do arquivo 'produtos'
fileProdutos = Path(my_path + '/' + name_file_Produtos) 

if fileProdutos.is_file():
    produtos = pd.read_csv(fileProdutos)
    
else:
    produtos = pd.DataFrame(columns = col_Produtos)
    produtos.to_csv(name_file_Produtos, index = False)
    

# Verificação/Criação do arquivo 'coletados'
fileColetados = Path(my_path + '/' + name_file_Coletados) 

if fileColetados.is_file():
    coletados = pd.read_csv(fileColetados)
    
else:
    coletados = pd.DataFrame(columns = col_Coletados)
    coletados.to_csv(name_file_Coletados, index = False)
    
    
    
# Verificação/Criação do arquivo 'linkLidos'
fileLidos = Path(my_path + '/' + name_file_Lidos) 

if fileLidos.is_file():
    lidos = pd.read_csv(fileLidos)
    
else:
    lidos = pd.DataFrame(columns = col_Lidos)
    lidos.to_csv(name_file_Lidos, index = False)


service = Service('C:/Program Files/Google/Chrome/Application/chromedriver')
service.start()
driver = webdriver.Remote(service.service_url)
driver.get(url_homepage)
driver.maximize_window() # Maximizar a tela para aparecer os menus

sleep(2)

soup = BeautifulSoup(driver.page_source)

menus = soup.findAll('li', {'class' : 'first-level'})

my_cat = pd.DataFrame(columns = ['Categoria', 'Link'])

for m in menus:
    my_cat = my_cat.append({'Categoria' : m.find('a', href = True)['title'],
                   'Link' : m.find('a', href = True)['href'].split('?')[0]}, ignore_index = True)

for i in range(len(my_cat)):
    
    j = 1
    vitrine = []
    
    while True:    
        driver.get(my_cat.iloc[i, 1]+'?&page='+str(j))
        sleep(2)

        soup = BeautifulSoup(driver.page_source)

        vitrine = soup.findAll('div', {'class' : 'nm-product-img-container'})
                
        
        if str(type(vitrine)) == "<class 'NoneType'>" or len(vitrine) == 0:
            break;
        

        for vit in vitrine:
            link_produto = 'https:'+vit.find('a', href = True)['href']
                      
            if link_produto in list(lidos.Link):
                continue
            
            else:
                now = datetime.now()
                now = str(now.year)+'-'+str(now.month)+'-'+str(now.day)+'-'+str(now.hour)+'h-'+str(now.minute)+'m'

                lidos = lidos.append({
                    'Data' : now,
                    'Categoria' : my_cat.iloc[i, 0],
                    'Link' : link_produto
                }, ignore_index = True)

                lidos.to_csv(name_file_Lidos, index = False)
                

        j += 1
        
        lidos = pd.read_csv(fileLidos)            
        lidos.drop_duplicates(subset=['Link'], inplace=True)
        lidos.to_csv(name_file_Lidos, index = False)


def pega_dado(url_product):
    driver.get(url_product)
    sleep(1.5)

    soup = BeautifulSoup(driver.page_source)

# Parte 1 -------------------------------------------------
    detalhes_name = soup.findAll('td', {'class' : 'title'})
    detalhes_cont = soup.findAll('td' , {'class' : 'info'})

    detalhes_dict = {
        'Referência' : '',
        'Cor' : '',
        'Função' : '',
        'Largura (cm)' : '',
        'Altura (cm)' : '',
        'Comprimento (cm)' : '',
        'Capacidade' : '',
        'Profundidade' : '',
        'Embalagem' : '',
        'Matéria-prima' : '',
        'Informações complementares' : '',
        'Instruções de conservação' : '',
        'Exclusivo' : '',
        'Voltagem' : '',
        'Diâmetro' : ''
    }

    for i in range(len(detalhes_name)):
        for j in detalhes_dict.keys():
            if j == detalhes_name[i].text:
                detalhes_dict[j] = detalhes_cont[i].text
                
# Parte 2 ------------------------------------------------
    descricao = soup.find('div', {'class' : 'produto-descricao'})
    
    descricao = descricao.text.replace('\xa0', ' ').replace('\n', ' ').replace("\'",'')
    
    nome = soup.find('h1', {'class' : 'produto-nome'}).text
    
# Parte 3 -------------------------------------------------
    img_detalhes = soup.find('ul', {'class' : 'slick-dots slick-thumb'}).findAll('a')

    imagens = []

    for i in img_detalhes:
        img = str(i).split('"')[1].split('?')[0]
        imagens.append(img)
                
# Parte 4 -------------------------------------------------
    prod_info = soup.findAll('script')

    for i in prod_info:
        if 'vtex.events.addData' in str(i):
            prod_info = str(i)

    prod_info = prod_info.split('(')[1].split(')')[0]

    prod_info = json.loads(prod_info)
    
    try:
        ean = prod_info['productEans'][0]
        img_name = ean
    except:
        ean = ''
        img_name = prod_info['productId']
        
    
    img_codes = salva_imagens(imagens, pathImg, img_name)
    
    informacoes = {
        'ean' : ean,
        'sku' : str(list(prod_info['skuStocks'].keys())[0]),
        'nome' : nome,
        'id_produto' : prod_info['productId'],
        'marca' : prod_info['productBrandName'],
        'id_marca' : prod_info['productBrandId'],
        'categoria' : prod_info['productCategoryName'],
        'id_categoria' : prod_info['productCategoryId'],
        'departamento' : prod_info['productDepartmentName'],
        'id_departamento' : prod_info['productDepartmentId'],
        'preco' : prod_info['productListPriceFrom'],
        'preco_promo' : prod_info['productPriceFrom'],
        
        'referencia' : detalhes_dict['Referência'],
        'cor' : detalhes_dict['Cor'],
        'funcao' : detalhes_dict['Função'],
        'largura_cm' : detalhes_dict['Largura (cm)'],
        'altura_cm' : detalhes_dict['Altura (cm)'],
        'comprimento_cm' : detalhes_dict['Comprimento (cm)'],
        'profundidade' : detalhes_dict['Profundidade'],
        'capacidade' : detalhes_dict['Capacidade'],
        'embalagem' : detalhes_dict['Embalagem'],
        'materia_prima' : detalhes_dict['Matéria-prima'],
        'info_complementares' : detalhes_dict['Informações complementares'],
        'instru_conservacao' : detalhes_dict['Instruções de conservação'],
        'exclusivo' : detalhes_dict['Exclusivo'],
        'voltagem' : detalhes_dict['Voltagem'],
        'diametro' : detalhes_dict['Diâmetro'],
        
        'imagens' : img_codes,
        'descricao' : descricao
    }
    
    
    return informacoes


def salva_imagens(imagens, pathImg, prod_code):

    # Coleta e download da imagem através do dicionário
    count = 1
    img_codes = ''
    for i in imagens:
        imgLink = i
        response = requests.get(imgLink)
        file = open(pathImg+'/' + str(prod_code) +'_'+str(count)+'.jpg', 'wb')
        file.write(response.content)
        file.close()
        img_codes += str(prod_code) +'_'+str(count)+'.jpg; '
        count += 1
        sleep(0.5)
        
    return img_codes


# Verificação/Criação do arquivo 'produtos versionados'
fileProdVer = Path(name_file_ProdVer) 

if fileProdVer.is_file():
    prod_versionado = pd.read_csv(name_file_ProdVer)
    
else:
    prod_versionado = pd.DataFrame(columns = col_Produtos)
    prod_versionado.to_csv(name_file_ProdVer, index = False)
    
    
# for i in range(len(lidos)):
for i in range(len(lidos)):
    
    if lidos.loc[i, 'Link'] in list(coletados.Link):
        continue
        
    else:
        url_product = lidos.loc[i, 'Link']
        print(url_product)
        
    try:
        info_dados = pega_dado(url_product)
    except:
        continue
    
    now = datetime.now()
    now = str(now.year)+'-'+str(now.month)+'-'+str(now.day)+'-'+str(now.hour)+'h-'+str(now.minute)+'m'
    
    
    coletados = pd.read_csv(fileColetados)
    coletados = coletados.append({
        'Data' : now,
        'Categoria' : lidos.loc[i, 'Categoria'],
        'Link' : lidos.loc[i, 'Link']
    }, ignore_index = True)
    coletados.to_csv(name_file_Coletados, index = False)
    
    produtos = pd.read_csv(fileProdutos)
    produtos = produtos.append(info_dados, ignore_index=True)
    produtos.to_csv(name_file_Produtos, index = False)
    
    prod_versionado = pd.read_csv(name_file_ProdVer)
    prod_versionado = prod_versionado.append(info_dados, ignore_index=True)
    prod_versionado.to_csv(name_file_ProdVer, index = False)
