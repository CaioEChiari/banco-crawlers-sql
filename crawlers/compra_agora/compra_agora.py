from bs4 import BeautifulSoup
from time import sleep
from pathlib import Path
from datetime import datetime
import random
import os
import requests
import csv


userList = [
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.88 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36'
]

#Aquisição da data atual
now = datetime.now()
now = str(now.year)+'-'+str(now.month)+'-'+str(now.day)
#Aquisição do caminho do arquivo
my_path = os.getcwd()
my_path = my_path.replace('\\','/')

#Criação da pasta 'leituras'
pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    print ("Diretorio %s já criado" % pathLeituras)
else:
    print ("Diretorio %s criado com sucesso " % pathLeituras)
 #Criação da pasta 'imagens' 
pathImg = my_path+"/imagens/img "+now
try:
    os.makedirs(pathImg)
except OSError:
    print ("Diretorio %s já criado" % pathImg)
else:
    print ("Diretorio %s criado com sucesso " % pathImg)

#Verificação da presença do arquivo de produtos
fileProducts = Path(my_path+'/produtos.csv')


#Verificação da presença do arquivo de produtos anterior
fileProducts = Path(my_path+'/produtos.csv')
if fileProducts.is_file():
    with open(f'{my_path}/produtos.csv', 'r', encoding="utf-8") as f:
        listaProdutos = list(csv.reader(f))
        for i in range(len(listaProdutos)):
            listaProdutos[i] = str(listaProdutos[i][0])
        print('Encontrados '+ str(len(listaProdutos)-1)+' produtos em arquivo')
        sleep(0.2)
else:
    with open(f'{my_path}/produtos.csv', 'a', newline='',encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow(['ean', 'nome', 'marca', 'embalagem', 'medida', 'img'])


fileProducts = Path(my_path+'/leituras/produtos.csv')
if fileProducts.is_file():
    with open(f'{pathLeituras}/produtos{now}.csv', 'r', encoding="utf-8") as f:
        listaProdutos = list(csv.reader(f))
        for i in range(len(listaProdutos)):
            listaProdutos[i] = str(listaProdutos[i][0])
        print('Encontrados '+ str(len(listaProdutos)-1)+' produtos em arquivo')
        sleep(0.2)
else:
    with open(f'{pathLeituras}/produtos{now}.csv', 'a', newline='',encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow(['ean', 'nome', 'marca', 'embalagem', 'medida', 'img'])


#Criação da variável de produtos já coletados
coletados = []
produtos = []
linksCapturados = []

url = 'https://www.compra-agora.com/'

headers = {'User-Agent': random.choice(userList)}
r = requests.get(url, headers).text
soup = BeautifulSoup(r, 'html.parser')
menu = soup.findAll('li',{'class':'lista-menu-itens'})
categorias = []
for li in menu:
    categorias.append(li.find('a')['href'])

urlSufix = '?ordenacao=0&filtro_principal=p&limit=24&p='
for cat in categorias:
    catName = cat.split('loja/')[1]
    catName = catName.split('/')[0]
    print(f'Coletando links da categoria {catName}')
    url = cat+urlSufix
    page = 1
    while True:
        print(f'Lendo pagina {page}')
        r = requests.get(f'{url}{page}', headers).text
        soup = BeautifulSoup(r, 'html.parser')
        vitrine = soup.findAll('a',{'class':'container-information'})
        if len(vitrine) == 0:
            break
        for a in vitrine:
            link = a['href']
            link = link.split('?site_id')[0]
            if link not in produtos:
                produtos.append(link)
                with open(f'{my_path}/linksColetados.csv', 'a', encoding='utf-8', newline='') as f:
                    writer = csv.writer(f)
                    writer.writerow([link])
        print(f'{len(produtos)} links coletados')
        page += 1

i = 1
headers = {'User-Agent': random.choice(userList)}
for p in produtos:
    if p in coletados:
        i += 1
        print(f'Produto já coletado!')
        continue
    r = requests.get(p, headers).text
    soup = BeautifulSoup(r, 'html.parser')
    
    ean = soup.find('div',{'class':'product-ean'})['data-ean']
    nome = soup.find('div',{'class':'product-title'}).text
    marca = soup.find('div',{'class':'logo-marca d-llg-none'}).text
    try:
        caracteristicas = soup.find('div',{'class':'product-features'}).text
        embalagem = caracteristicas.split(' | ')[0].replace('\n', '').strip()
        medida = caracteristicas.split(' | ')[1].strip()
    except IndexError:
        embalagem = ''
        medida = ''

    img = soup.find('div',{'class':'miniaturas-container'})
    img = img.find('img')['src']
    print(f'{i} -  [{ean} - {nome}]')
    
    #Salvar produto na variável de coletados
    coletados.append(p)
    #Salvar dados no arquivo
    with open(f'{my_path}/produtos.csv', 'a', newline='',encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow([ean, nome, marca, embalagem, medida, img])
    with open(f'{pathLeituras}/produtos{now}.csv', 'a', newline='',encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow([ean, nome, marca, embalagem, medida, img])
    #Salvar link na lista de produtos já coletados
    with open(my_path+'/produtosColetados.csv', 'a', newline='',encoding='utf-8') as f:
        writer = csv.writer(f)
        writer.writerow([p])
    i += 1