from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.keys import Keys
from time import sleep
from pathlib import Path
from bs4 import BeautifulSoup
import requests
import os
from datetime import datetime
import json
import pandas as pd

#Diversos
from IPython.display import clear_output

from random import sample

col_Produtos = ['ean',
        'sku',
        'mpn',
        'nome',
        'marca',
        'categoria',
        'subcategoria1',
        'subcategoria2',
        'cor',
        'peso (g)',
        'preco',
        'memoria interna (gb)',
        'polegadas',
        'voltagem',
        'capacidade de resfriamento (BTU/h)',
        'sistema operacional',
        'processador',
        'memória ram',
        'descricao1',
        'descricao2',
        'img_codes',
        'img_links',

        'Velocidade do Processador',
        'Peso da embalagem',
        'Capacidade da Bateria (mAh, Typical)',
        'Tamanho (Tela Principal)',
        'Resolução (Tela Principal)',
        'Tecnologia (Tela Principal)',
        'Resolução - Câmera Traseira',
        'Resolução - Câmera Frontal',
        'Resolução - Gravação de Vídeos',
        'Número de Chip',
        'USB Interface',
        'Modelos Compatíveis',
        'Itens inclusos',
        'Potência',
        'Microfone',
        'Alto-falante',
        'Impedância',
        'Versão de Bluetooth',
        'Controle',
        'Resolução',
        'Frequência da tela',
        'Número de Canais',
        'Capacidade de Lavagem (kg)',
        'Capacidade de Secagem (kg)',
        'Total (L)',
        'Tecnologia de Refrigeração',
        'Peso Líquido - Unidade Interna',
        'Peso Líquido - Unidade Externa',
        'Capacidade de armazenamento de pó',
        'Desempenho',
        'Proporção de Tela']

url_homepage = 'https://shop.samsung.com/br/'

col_Coletados = ['Data','Link']

col_Lidos = ['Data', 'Link']

col_Quebrados = ['Data', 'Link']


# Data e Hora para o Versionamento
diversao = datetime.now()
diversao = str(diversao.year)+'-'+str(diversao.month)+'-'+str(diversao.day)

# Criação das Pastas
my_path = os.getcwd()
my_path = my_path.replace('\\','/')

# Criação da pasta 'leituras'
pathLeituras = my_path+"/leituras"
try:
    os.makedirs(pathLeituras)
except OSError:
    print ("Diretorio %s já criado" % pathLeituras)
else:
    print ("Diretorio %s criado com sucesso " % pathLeituras)
    
    
    
# Criação da pasta 'imagens' 
pathImg = my_path+"/imagens/img "+diversao
try:
    os.makedirs(pathImg)
except OSError:
    print ("Diretorio %s já criado" % pathImg)
else:
    print ("Diretorio %s criado com sucesso " % pathImg)
    
    
    
# Pegar nome do site
name_site = url_homepage.split('.')[1]


# Criação dos nomes dos arquivos
name_file_Produtos = 'produtos_' + str(name_site) + '.csv'

name_file_Coletados = 'coletados_' + str(name_site) + '.csv'  # Dados coletados dos links

name_file_Lidos = 'lidos_' + str(name_site) + '.csv' # Lidos do site

name_file_Quebrados = 'quebrados_' + str(name_site) + '.csv' # Lidos do site

name_file_ProdVer = pathLeituras + '/' + diversao + '_' + 'produtos_' + str(name_site) + '.csv'


# Verificação/Criação do arquivo 'produtos'
fileProdutos = Path(my_path + '/' + name_file_Produtos) 

if fileProdutos.is_file():
    produtos = pd.read_csv(fileProdutos)
    
else:
    produtos = pd.DataFrame(columns = col_Produtos)
    produtos.to_csv(name_file_Produtos, index = False)
    

# Verificação/Criação do arquivo 'coletados'
fileColetados = Path(my_path + '/' + name_file_Coletados) 

if fileColetados.is_file():
    coletados = pd.read_csv(fileColetados)
    
else:
    coletados = pd.DataFrame(columns = col_Coletados)
    coletados.to_csv(name_file_Coletados, index = False)
    
    
    
# Verificação/Criação do arquivo 'linkLidos'
fileLidos = Path(my_path + '/' + name_file_Lidos) 

if fileLidos.is_file():
    lidos = pd.read_csv(fileLidos)
    
else:
    lidos = pd.DataFrame(columns = col_Lidos)
    lidos.to_csv(name_file_Lidos, index = False)
    
    
# Verificação/Criação do arquivo 'linkLidos'
fileQuebrados = Path(my_path + '/' + name_file_Quebrados) 

if fileQuebrados.is_file():
    quebrados = pd.read_csv(fileQuebrados)
    
else:
    quebrados = pd.DataFrame(columns = col_Quebrados)
    quebrados.to_csv(name_file_Quebrados, index = False)

# Create a new instance of the Firefox driver (or Chrome)
driver = webdriver.Chrome()

# Create a request interceptor
def interceptor(request):
    del request.headers['Referer']  # Delete the header first
    request.headers['Referer'] = 'some_referer'

# Set the interceptor on the driver
driver.request_interceptor = interceptor

# All requests will now use 'some_referer' for the referer
driver.get(url_homepage)

driver.maximize_window()

sleep(2)

soup = BeautifulSoup(driver.page_source)

# Pega a categoria

fileCategorias = Path(my_path + '/' + 'categorias_' + str(name_site) + '.csv') 

if  fileCategorias.is_file():
    my_cats = pd.read_csv(fileCategorias)

else:
    my_cats = pd.DataFrame(columns = ['Categoria','Link'])
    my_cats.to_csv('categorias_' + str(name_site) + '.csv', index = False)

driver.get('https://shop.samsung.com/br/')
sleep(2)
soup = BeautifulSoup(driver.page_source)

strong = soup.find_all('strong',{'class' : 'samsungbr-samsung-store-0-x-gnb__depth2-title'})

for i in strong:
    my_cat_link = 'https://shop.samsung.com/br' + i.find('a')['href'].split('?')[0]
    
    if my_cat_link in list(my_cats.Link):
        continue
    else:
        my_cats = pd.read_csv(fileCategorias)
        my_cats = my_cats.append({
            'Categoria' : i.find('a').text,
            'Link' : my_cat_link
        }, ignore_index = True)
        
        my_cats.to_csv('categorias_' + str(name_site) + '.csv', index = False)


def agora():
    now = datetime.now()
    return str(now.year)+'-'+str(now.month)+'-'+str(now.day)+'-'+str(now.hour)+'h-'+str(now.minute)+'m'

def pega_produto(categoria):
    global lidos   

    soup = BeautifulSoup(driver.page_source)
            
    vit = []

    vitrine = soup.find_all('section' , {'class' : 'vtex-product-summary-2-x-container vtex-product-summary-2-x-containerNormal overflow-hidden br3 h-100 w-100 flex flex-column justify-between center tc'})
                
    for vit in vitrine:
        link_produto = 'https://shop.samsung.com' + vit.find('a')['href']
                    
        if link_produto in list(lidos.Link):
            continue
                        
        else:
            now = datetime.now()
            now = str(now.year)+'-'+str(now.month)+'-'+str(now.day)+'-'+str(now.hour)+'h-'+str(now.minute)+'m'
                        
            lidos = pd.read_csv(fileLidos)
                        
            lidos = lidos.append({
                'Data' : now,
                'Categoria' : categoria,
                'Link' : link_produto
            }, ignore_index = True)
                        
            lidos.drop_duplicates(subset=['Link'], inplace=True)
            lidos.to_csv(name_file_Lidos, index = False)
                        
    clear_output(wait=True)
    print(categoria)
    print(len(vitrine))
    print('Total de produtos: ' + str(len(lidos)))
    print(lidos.Categoria.value_counts())

# Verificação/Criação do arquivo 'paginas_lidas'
filePaginasLidas = Path(my_path + '/' + 'paginas_lidas_' + str(name_site) + '.csv') 

if  filePaginasLidas.is_file():
    paginas_lidas = pd.read_csv(filePaginasLidas)

else:
    paginas_lidas = pd.DataFrame(columns = ['Data', 'Categoria','Link_Principal','Link_Pagina'])
    paginas_lidas.to_csv('paginas_lidas_' + str(name_site) + '.csv', index = False)
    
    
for i in range(len(my_cats)):
    page_count = 1
    while True:        
        my_link = my_cats.Link[i] + '?page=' + str(page_count)
        
        if my_link in list(paginas_lidas.Link_Pagina):
            page_count += 1
            continue
        
        driver.get(my_link)
        sleep(5)
        
        soup = BeautifulSoup(driver.page_source)
        notfound = soup.find_all('div',{'class' : 'samsungbr-samsung-store-0-x-SearchNotFoundContainerNew tc'})
        if len(notfound) != 0:
            print('Fim da categoria!')
            break
        
        for _ in range(10):
            pega_produto(my_cats.Categoria[i])
            sleep(1)
            driver.execute_script("window.scrollTo(0, window.scrollY + 300)")
  
        paginas_lidas = pd.read_csv(filePaginasLidas)
        paginas_lidas = paginas_lidas.append({
            'Data' : agora(),
            'Categoria' : my_cats.Categoria[i],
            'Link_Principal' : my_cats.Link[i],
            'Link_Pagina' : my_link
        }, ignore_index = True)
        paginas_lidas.to_csv('paginas_lidas_' + str(name_site) + '.csv', index = False)
        
        print('Acabou a página!')
        page_count += 1

def xpath_soup(element):
    components = []
    child = element if element.name else element.parent
    for parent in child.parents:  # type: bs4.element.Tag
        siblings = parent.find_all(child.name, recursive=False)
        components.append(
            child.name if 1 == len(siblings) else '%s[%d]' % (
                child.name,
                next(i for i, s in enumerate(siblings, 1) if s is child)
                )
            )
        child = parent
    components.reverse()
    return '/%s' % '/'.join(components)

def carrega_pagina(url, sleep_time):
    driver.get(url)
    driver.switch_to.window(driver.current_window_handle)
    
    sleep(sleep_time)
    
    driver.switch_to.window(driver.current_window_handle)
    sleep(2)
    driver.execute_script("window.scrollTo(0, window.scrollY + 2000)")
    sleep(2)
    driver.execute_script("window.scrollTo(0, window.scrollY + 2000)")
    sleep(2)
    
    soup = BeautifulSoup(driver.page_source)
    
    return soup

def salva_imagens(soup, prod_code):
    global pathImg

    soup = BeautifulSoup(driver.page_source)
    swiper_wrapper = soup.find('div', {'class' : 'swiper-container swiper-container-initialized swiper-container-horizontal vtex-store-components-3-x-productImagesGallerySwiperContainer'})
    sw = swiper_wrapper.find_all('img')

    imagens = []
    img_links = ''
    
    for i in range(len(sw)):
        imagens.append(sw[i]['srcset'].split('?')[0])
        if i != len(sw)-1:
            img_links += sw[i]['srcset'].split('?')[0] + ';'
        else:
            img_links += sw[i]['srcset'].split('?')[0]
    
    
    count = 1
    img_codes = ''
    for i in imagens:
        imgLink = i
        response = requests.get(imgLink)
        file = open(pathImg+'/' + str(prod_code) +'_'+str(count)+'.jpg', 'wb')
        file.write(response.content)
        file.close()
        if count != len(imagens):
            img_codes += str(prod_code) +'_'+str(count)+'.jpg;'
        else:
            img_codes += str(prod_code) +'_'+str(count)+'.jpg'
        count += 1
        sleep(0.5)
        
    return [img_codes, img_links]


def milhoes_de_dados(soup):
    
    inpage_h5 = soup.find_all('div',{'class' : 'inpage_h5 flix-d-h6'})
    flix = soup.find_all('div',{'class' : 'inpage_desc flix-d-p'})
    
    velocidade_processador = ''
    peso = ''
    tipo_processador = ''
    tamanho_tela_principal = ''
    resolucao_tela_principal = ''
    tecnologia_tela_principal = ''
    resolucao_camera_traseira = ''
    resolucao_camera_frontal = ''
    memoria_ram = ''
    numero_chip = ''
    usb_interface = ''
    capacidade_bateria = ''
    resolucao_grav_videos = ''
    modelos_compativeis = ''
    itens_inclusos = ''
    peso_fone = ''
    peso_case = ''
    potencia = ''
    microfone = ''
    alto_falante = ''
    impedancia = ''
    versao_bluetooth = ''
    controle = ''
    memoria_interna = ''
    resolucao = ''
    freq_tela = ''
    peso_embalagem = ''
    numero_canais = ''
    cap_lavagem = ''
    cap_secagem = ''
    cor = ''
    total_l = ''
    tec_refrigeracao = ''
    peso_interno = ''
    peso_externo = ''
    desempenho = ''
    capacidade_po = ''
    tela = ''
    prop_tela = ''
    tamanho_tela = ''
    capacidade_resf = ''

    for i in range(len(inpage_h5)):
        questao = inpage_h5[i].text.strip()
        resposta = flix[i].text.strip()
        if questao == 'Velocidade do Processador':
            velocidade_processador = resposta

        if questao == 'Peso (g)' or questao == 'Peso' or questao == 'Peso do conjunto com suporte' or questao == 'Peso do fone de ouvido' or questao == 'Peso do conjunto (kg)' or questao == 'Peso Bruto (Embalagem)' or questao == 'Peso Líquido' or questao == 'Peso Líquido (kg)':
            peso = resposta

        if questao == 'Tipo de Processador' or questao == 'Processador':
            tipo_processador = resposta

        if questao == 'Tamanho (Tela Principal)':
            tamanho_tela_principal = resposta

        if questao == 'Resolução (Tela Principal)':
            resolucao_tela_principal = resposta

        if questao == 'Tecnologia (Tela Principal)':
            tecnologia_tela_principal = resposta

        if questao == 'Resolução - Câmeras Traseiras (Múltiplas)' or questao == 'Resolução - Câmera Traseira':
            resolucao_camera_traseira = resposta

        if questao == 'Resolução - Câmera Frontal':
            resolucao_camera_frontal = resposta

        if questao == 'Memória RAM(GB)' or questao == 'Memória RAM (GB)' or questao == 'Memória':
            memoria_ram = resposta

        if questao == 'Número de Chip':
            numero_chip = resposta

        if questao == 'USB Interface' or questao == 'Interface':
            usb_interface = resposta

        if questao == 'Capacidade da Bateria (mAh, Typical)' or questao == 'Capacidade':
            capacidade_bateria = resposta

        if questao == 'Resolução - Gravação de Vídeos':
            resolucao_grav_videos = resposta

        if questao == 'Modelos Compatíveis':
            modelos_compativeis = resposta

        if questao == 'Itens inclusos':
            itens_inclusos = resposta

        if questao == 'Potência' or questao == 'Potência (RMS)' or questao == 'Potência Total':
            potencia = resposta

        if questao == 'Microfone':
            microfone = resposta

        if questao == 'Alto-falante':
            alto_falante = resposta

        if questao == 'Impedância':
            impedancia = resposta

        if questao == 'Versão de Bluetooth':
            versao_bluetooth = resposta

        if questao == 'Controle':
            controle = resposta

        if questao == 'Memória Total Interna (GB)' or questao == 'Memória Total Interna (GB)*' or questao == 'Armazenamento':
            memoria_interna = resposta

        if questao == 'Resolução':
            resolucao = resposta

        if questao == 'Frequência da tela':
            freq_tela = resposta

        if questao == 'Peso da embalagem':
            peso_embalagem = resposta

        if questao == 'Número de Canais':
            numero_canais = resposta

        if questao == 'Capacidade de Lavagem (kg)':
            cap_lavagem = resposta

        if questao == 'Capacidade de Secagem (kg)':
            cap_secagem = resposta

        if questao == 'Cor' or questao == 'Cor pontual' or questao == 'Cor do aparelho':
            cor = resposta

        if questao == 'Total (L)':
            total_l = resposta

        if questao == 'Tecnologia de Refrigeração':
            tec_refrigeracao = resposta

        if questao == 'Peso Líquido - Unidade Interna':
            peso_interno = resposta

        if questao == 'Peso Líquido - Unidade Externa':
            peso_externo = resposta

        if questao == 'Capacidade de armazenamento de pó':
            capacidade_po = resposta

        if questao == 'Desempenho':
            desempenho = resposta

        if questao == 'Tela' or questao == 'Tamanho de Tela (polegadas)':
            tela = resposta

        if questao == 'Proporção de Tela':
            prop_tela = resposta

        if questao == 'Capacidade de Resfriamento (BTU/h)':
            capacidade_resf = resposta



    my_dict_specs = {
        'Velocidade do Processador' : velocidade_processador,
        'Tipo de Processador' : tipo_processador,
        'Memória RAM(GB)' : memoria_ram,
        'Peso (g)' : peso,
        'Peso da embalagem' : peso_embalagem,
        'Capacidade da Bateria (mAh, Typical)' : capacidade_bateria,
        'Tamanho (Tela Principal)' : tamanho_tela_principal,
        'Resolução (Tela Principal)' : resolucao_tela_principal,
        'Tecnologia (Tela Principal)' : tecnologia_tela_principal,
        'Resolução - Câmera Traseira' : resolucao_camera_traseira,
        'Resolução - Câmera Frontal' : resolucao_camera_frontal,
        'Resolução - Gravação de Vídeos' : resolucao_grav_videos,
        'Memória Total Interna (GB)' : memoria_interna,
        'Número de Chip' : numero_chip,
        'USB Interface' : usb_interface,
        'Modelos Compatíveis' : modelos_compativeis,
        'Itens inclusos' : itens_inclusos,
        'Potência' : potencia,
        'Microfone' : microfone,
        'Alto-falante' : alto_falante,
        'Impedância' : impedancia,
        'Versão de Bluetooth' : versao_bluetooth,
        'Controle' : controle,
        'Resolução' : resolucao,
        'Frequência da tela' : freq_tela,
        'Número de Canais' : numero_canais,
        'Capacidade de Lavagem (kg)' : cap_lavagem,
        'Capacidade de Secagem (kg)' : cap_secagem,
        'Cor' : cor,
        'Total (L)' : total_l,
        'Tecnologia de Refrigeração' : tec_refrigeracao,
        'Peso Líquido - Unidade Interna' : peso_interno,
        'Peso Líquido - Unidade Externa' : peso_externo,
        'Capacidade de armazenamento de pó' : capacidade_po,
        'Desempenho' : desempenho,
        'Tela' : tela,
        'Proporção de Tela' : prop_tela,
        'Capacidade de Resfriamento (BTU/h)' : capacidade_resf
    }
    return my_dict_specs


def pega_dados(url, dict_json, sku, preco):
    soup = carrega_pagina(url, 2)

    milhoes_dados = milhoes_de_dados(soup)
    
    # Parte 1 - nome ean cor
    nome = soup.find('h1',{'class' : 'samsungbr-samsung-store-0-x-ProductTitle'}).text.split('Compartilhe')[0]
    ean = soup.find('h3',{'class' : 'samsungbr-samsung-store-0-x-ProductEan'}).text
    
    try:
        cor = soup.find('span',{'class' : 'vtex-store-components-3-x-skuSelectorSelectorImageValue c-muted-1 t-small'}).text
    except:
        cor = milhoes_dados['Cor']
        
    
    # Parte 2 - memoria polegada voltagem capacidade_resfriamento sistema_operacional processador memoria_ram
    memoria = ''
    polegadas = ''
    voltagem = ''
    capacidade_resf = ''
    so = ''
    processador = ''
    memoria_ram = ''
    
    action_primary = soup.find_all('div',{'class' : 'vtex-store-components-3-x-frameAround absolute b--action-primary br3 bw1 ba'})

    for i in action_primary:
        specs = i.parent.parent.parent.find('span',{'class' : 'vtex-store-components-3-x-skuSelectorName c-muted-1 t-small overflow-hidden'}).text
        if specs == 'Memória' or specs == 'Armazenamento': 
            memoria = i.findNext('div').text
            
        if memoria == '':
            memoria = milhoes_dados['Memória Total Interna (GB)']
            
        if specs == 'Polegadas':
            polegadas = i.findNext('div').text
            
        if polegadas == '':
            polegadas = milhoes_dados['Tela']
            
        if specs == 'Voltagem':
            voltagem = i.findNext('div').text
            
        if specs == 'Capacidade':
            capacidade_resf = i.findNext('div').text
            
        if capacidade_resf == '':
            capacidades_resf = milhoes_dados['Capacidade de Resfriamento (BTU/h)']
            
        if specs == 'Sistema Operacional':
            so = i.findNext('div').text
            
        if specs == 'Processador':
            processador = i.findNext('div').text
            
        if processador == '':
            processador = milhoes_dados['Tipo de Processador']
            
        if specs == 'Memória RAM':
            memoria_ram = i.findNext('div').text
            
        if memoria_ram == '':
            memoria_ram = milhoes_dados['Memória RAM(GB)']
            
    # Parte 3 - marca descricao1 mpn
    
    descricao = dict_json['description']
    marca = dict_json['brand']
    mpn = dict_json['mpn']
       
    # Parte 4 - peso
    peso = milhoes_dados['Peso (g)']
    
    # Parte 5 categoria sub1 sub2
    categoria = soup.find('span',{'class' : 'vtex-breadcrumb-1-x-arrow vtex-breadcrumb-1-x-arrow--1 ph2 c-muted-2'}).text
    
    subcategoria1 = soup.find('span',{'class' : 'vtex-breadcrumb-1-x-arrow vtex-breadcrumb-1-x-arrow--2 ph2 c-muted-2'}).text
    
    try:
        subcategoria2 = soup.find('span',{'class' : 'vtex-breadcrumb-1-x-arrow vtex-breadcrumb-1-x-arrow--3 ph2 c-muted-2'}).text
    except:
        subcategoria2 = ''
        
    # Parte 6 - Descricao2
    prod_descr = soup.find_all('ul',{'class' : 'samsungbr-samsung-store-0-x-ProductDescriptionBR'})

    prod_d = ''

    for i in range(len(prod_descr)):
        if i != len(prod_descr)-1:
            prod_d += prod_descr[i].text.strip() + ' '
        else:
            prod_d += prod_descr[i].text.strip() +''
            
    
    # Parte 7 - imagens
    img_list = salva_imagens(soup, sku)
    
    
    
    # Parte Final
    
    my_dict = {
        'ean' : ean,
        'sku' : sku,
        'mpn' : mpn,
        'nome' : nome,
        'marca' : marca,
        'categoria' : categoria,
        'subcategoria1' : subcategoria1,
        'subcategoria2' : subcategoria2,
        'cor' : cor,
        'peso (g)' : peso,
        'preco' : preco,
        'memoria interna (gb)' : memoria,
        'polegadas' : polegadas,
        'voltagem' : voltagem,
        'capacidade de resfriamento (BTU/h)' : capacidade_resf,
        'sistema operacional' : so,
        'processador' : processador,
        'memória ram' : memoria_ram,
        'descricao1' : descricao,
        'descricao2' : prod_d,
        'img_codes' : img_list[0],
        'img_links' : img_list[1],
        
        'Velocidade do Processador' : milhoes_dados['Velocidade do Processador'],
        'Peso da embalagem' : milhoes_dados['Peso da embalagem'],
        'Capacidade da Bateria (mAh, Typical)' : milhoes_dados['Capacidade da Bateria (mAh, Typical)'],
        'Tamanho (Tela Principal)' : milhoes_dados['Tamanho (Tela Principal)'],
        'Resolução (Tela Principal)' : milhoes_dados['Resolução (Tela Principal)'],
        'Tecnologia (Tela Principal)' : milhoes_dados['Tecnologia (Tela Principal)'],
        'Resolução - Câmera Traseira' : milhoes_dados['Resolução - Câmera Traseira'],
        'Resolução - Câmera Frontal' : milhoes_dados['Resolução - Câmera Frontal'],
        'Resolução - Gravação de Vídeos' : milhoes_dados['Resolução - Gravação de Vídeos'],
        'Número de Chip' : milhoes_dados['Número de Chip'],
        'USB Interface' : milhoes_dados['USB Interface'],
        'Modelos Compatíveis' : milhoes_dados['Modelos Compatíveis'],
        'Itens inclusos' :milhoes_dados['Itens inclusos'],
        'Potência' : milhoes_dados['Potência'],
        'Microfone' :milhoes_dados['Microfone'],
        'Alto-falante' : milhoes_dados['Alto-falante'],
        'Impedância' : milhoes_dados['Impedância'],
        'Versão de Bluetooth' : milhoes_dados['Versão de Bluetooth'],
        'Controle' : milhoes_dados['Controle'],
        'Resolução' : milhoes_dados['Resolução'],
        'Frequência da tela' : milhoes_dados['Frequência da tela'],
        'Número de Canais' : milhoes_dados['Número de Canais'],
        'Capacidade de Lavagem (kg)' :milhoes_dados['Capacidade de Lavagem (kg)'],
        'Capacidade de Secagem (kg)' : milhoes_dados['Capacidade de Secagem (kg)'],
        'Total (L)' : milhoes_dados['Total (L)'],
        'Tecnologia de Refrigeração' : milhoes_dados['Tecnologia de Refrigeração'],
        'Peso Líquido - Unidade Interna' : milhoes_dados['Peso Líquido - Unidade Interna'],
        'Peso Líquido - Unidade Externa' : milhoes_dados['Peso Líquido - Unidade Externa'],
        'Capacidade de armazenamento de pó' : milhoes_dados['Capacidade de armazenamento de pó'],
        'Desempenho' : milhoes_dados['Desempenho'],
        'Proporção de Tela' : milhoes_dados['Proporção de Tela'] 
    }
    
    return my_dict


# Verificação/Criação do arquivo 'produtos versionados'
fileProdVer = Path(name_file_ProdVer) 

if fileProdVer.is_file():
    prod_versionado = pd.read_csv(name_file_ProdVer)
    
else:
    prod_versionado = pd.DataFrame(columns = col_Produtos)
    prod_versionado.to_csv(name_file_ProdVer, index = False)
    
sam = sample(range(len(lidos)),300)
# Executando

#count = 1
#for i in sam:
#    print(count)
#    count += 1

for i in range(len(lidos)):
    now = agora()
    
    if lidos.loc[i, 'Link'] in list(coletados.Link):
        continue
        
    else:
        url_product = lidos.loc[i, 'Link']
        print(i)
        print(url_product)
        
        soup = carrega_pagina(url_product, 2)
        skus = []
        preco = []
        
        try:
            applicationjson = str(soup.find('script',{'type' : 'application/ld+json'}))
            applicationjson = applicationjson.split('json">')[1].split('</script>')[0]
            dict_json = json.loads(applicationjson)
            for x in dict_json['offers']['offers']:
                skus.append(x['sku'])
                preco.append(x['price'])
                
        except:
            quebrados = pd.read_csv(fileQuebrados)
            quebrados = quebrados.append({
                'Data' : now,
                'Link' : lidos.loc[i, 'Link']
            }, ignore_index = True)
            quebrados.to_csv(name_file_Quebrados, index = False)
            print('Algo de errado aconteceu!')
            continue
            
        for s in range(len(skus)):
            url = url_product + '?skuId=' + skus[s]
            
            if url in list(quebrados.Link):
                continue
            
            try:
                info_dados = pega_dados(url, dict_json, skus[s], preco[s])

                produtos = pd.read_csv(fileProdutos)
                produtos = produtos.append(info_dados, ignore_index=True)
                produtos.to_csv(name_file_Produtos, index = False)

                prod_versionado = pd.read_csv(name_file_ProdVer)
                prod_versionado = prod_versionado.append(info_dados, ignore_index=True)
                prod_versionado.to_csv(name_file_ProdVer, index = False)
                
                
            except:
                quebrados = pd.read_csv(fileQuebrados)
                quebrados = quebrados.append({
                    'Data' : now,
                    'Link' : lidos.loc[i, 'Link']
                }, ignore_index = True)
                quebrados.to_csv(name_file_Quebrados, index = False)
                print('Algo de errado aconteceu!')
                continue
            
            
        coletados = pd.read_csv(fileColetados)
        coletados = coletados.append({
            'Data' : now,
            'Link' : lidos.loc[i, 'Link']
        }, ignore_index = True)
        coletados.to_csv(name_file_Coletados, index = False)
