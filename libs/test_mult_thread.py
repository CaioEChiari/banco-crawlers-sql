import pandas as pd
from psycopg2.pool import ThreadedConnectionPool
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import psycopg2
import threading
import time

class My_Thread(threading.Thread):
   def __init__(self, threadID, name, counter, base, conn):
      threading.Thread.__init__(self)
      self.conn = conn
      self.threadID = threadID
      self.name = name
      self.counter = counter
      self.base = base

   def run(self):
      print("Starting " + self.name)
      cur = self.conn.cursor()
      aa.pegar_dados_produtos(urls=self.base, cur=cur)
      self.conn.close()
      print("Exiting " + self.name)

# Create new threads]
aa = Mac()
base = aa.pegar_urls_produtos()
max_threads = 20
threads = []
x = int(len(base) / max_threads)
final_list = lambda x: [base[i:i+x] for i in range(0, len(base), x)]
bases = final_list(x)

DSN = "postgresql://postgres:toor@localhost/crawlers"
tcp = ThreadedConnectionPool(1, max_threads + 2, DSN)

start = time.time()

for i in range(len(bases)):
   conn = tcp.getconn()
   conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
   threads.append(My_Thread(i, "Thread-" + str(i), i, bases[i], conn))

# Start new Threads
for thread_ in threads:
   thread_.start()



####################################################################################33
while True:
   cont = 0
   #time.sleep(10)
   for thread_ in threads:
      if thread_.is_alive():
         cont += 1
   if 0 == cont:
      break
finish = time.time()
com_threads = finish - start
print('Com threads:', com_threads)
print(com_threads / 60)
# Com threads: 172.89713287353516
# 2.881618881225586
#####################################
# import pandas as pd
# import threading
# import time

# class Criador_Threads():
#    def __init__(self, thread_number, base_completa):
#       # Create new threads
#       self.thread_number = thread_number
#       self.threads = []
#       x = int(len(base_completa) / thread_number)
#       final_list = lambda x: [base_completa[i:i+x] for i in range(0, len(base_completa), x)]
#       bases = final_list(x)

#       for i in range(len(bases)):
#          #print(i, "Thread-" + str(i), i, bases[i])
#          #print(bases[i])
#          self.threads.append(My_Thread(i, "Thread-" + str(i), i, bases[i]))

#       # Start new Threads
#       for thread_ in self.threads:
#          thread_.start()

#       while True:
#          cont = 0
#          time.sleep(10)
#          for thread_ in threads:
#             if thread_.is_alive():
#                cont += 1
#             if len(threads) == cont:
#                break


# class My_Thread(threading.Thread):
#    def __init__(self, threadID, name, counter, base):
#       threading.Thread.__init__(self)
#       self.threadID = threadID
#       self.name = name
#       self.counter = counter
#       self.base = base

#    def run(self):
#       aa = Arezzo()
#       print("Starting " + self.name)
#       self.resultados = aa.pega_dados
#       print("Exiting " + self.name)

#######################################################
import psycopg2 
import random
import concurrent.futures
from psycopg2.pool import ThreadedConnectionPool
import time

aa = Mac()
base = aa.pegar_urls_produtos()
#base = base_bk
DSN = "postgresql://postgres:toor@localhost/crawlers"
thread_number = 100
#tcp = ThreadedConnectionPool(1, thread_number + 1, DSN)
x = int(len(base) / thread_number)
final_list = lambda x: [base[i:i+x] for i in range(0, len(base), x)]
bases = final_list(x)
start = time.time()
with concurrent.futures.ThreadPoolExecutor(max_workers=thread_number + 1) as executor:
    futures = []
    for base in bases:
        futures.append(executor.submit(aa.pegar_dados_produtos, urls=base))
    for future in concurrent.futures.as_completed(futures):
        print(future.result())
finish = time.time()

print(finish - start)

#sem_threads = finish - start
com_treads = finish - start

print('sem_threads:', sem_threads)
print('com_treads:', com_treads)

####################################################

import psycopg2 
import random
from concurrent.futures import ThreadPoolExecutor, as_completed
from psycopg2.pool import ThreadedConnectionPool

def write_sim_to_db(all_ids2):
    if all_ids1[i] != all_ids2:
        conn = tcp.getconn()
        c = conn.cursor()
        c.execute("""SELECT count(*) FROM similarity WHERE prod_id1 = %s AND prod_id2 = %s""", (all_ids1[i], all_ids2,))
        count = c.fetchone()
        if count[0] == 0:
            sim_sum = random.random()
            c.execute("""INSERT INTO similarity(prod_id1, prod_id2, sim_sum) 
                    VALUES(%s, %s, %s)""", (all_ids1[i], all_ids2, sim_sum,))
            conn.commit()
        tcp.putconn(conn)

DSN = "postgresql://postgres:toor@localhost/crawlers"
tcp = ThreadedConnectionPool(1, 10, DSN)

all_ids1 = list(n for n in range(1000))
all_ids2_list = list(n for n in range(1000))

for i in range(len(all_ids1)):
    with ThreadPoolExecutor(max_workers=2) as pool:
        results = [pool.submit(write_sim_to_db, i) for i in all_ids2_list]