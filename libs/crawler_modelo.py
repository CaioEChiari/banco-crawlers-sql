from abc import ABC, ABCMeta, abstractmethod
from .banco_de_dados import Banco_De_Dados
from .imagens import Image_Manipulation
from .arquivo_csv import Arquivo_CSV
from urllib.parse import unquote
from abc import abstractmethod
import concurrent.futures
from pathlib import Path
import os, os.path, csv
import platform

class Crawler_Modelo(ABC):
    @classmethod
    def __init__(self):
        system = platform.system()
        if system == 'Linux':
            self.barra = '\\'
        elif system == 'Windows':
            self.barra = '/'
        self.my_path = self.encontrar_path()
        diretorio_raiz = os.getcwd().replace('\\', self.barra)
        self.schema_nome = self.pega_nome_classe()
        self.arq_csv = Arquivo_CSV(f"{diretorio_raiz}{self.barra}relatorios", self.barra)
        self.img = Image_Manipulation(self.my_path, self.schema_nome, self.barra)
        directories = ['images']
        sub_directories = ['downloads', 'resized', 'resizing_error']
        try:
            os.mkdir(f'{diretorio_raiz}{self.barra}relatorios')
            print(f"Directory 'relatorios' Created")
        except FileExistsError:
            pass
        for directory in directories:
            try:
                os.mkdir(f'{self.my_path}{self.barra}{directory}')
                print(f"Directory '{directory}' Created")
            except FileExistsError:
                pass
            for sub_directory in sub_directories:
                try:
                    os.mkdir(f'{self.my_path}{self.barra}{directory}{self.barra}{sub_directory}')
                    print(f"Directory 'images{self.barra}{sub_directory}' Created")
                except FileExistsError:
                    pass

    @abstractmethod
    def pegar_urls_produtos(self):
        """
        produtos_encontrados = self.carregar_links_encontrados_csv()
        ----------------------------------------------------
        if a_href in produtos_encontrados or a_href == site + '#':
            continue
        if a_href not in produtos_encontrados:
            self.adicionar_links_encontrados_csv(a_href)
            produtos_encontrados.append(a_href)
        """
        pass

    @abstractmethod
    def pegar_dados_produtos(self, urls='0'):
        """
        if type(urls) is not list:
            urls = [urls]
        coletados = self.carregar_links_coletados_csv()
        if urls == '0':
            urls = self.carregar_links_encontrados_csv()
        for url in urls:
            if url in coletados:
                continue
            else:
                self.adicionar_links_produtos_novos_csv(url)
            get(url)
            produtos = {}
        ----------------------------------------------------
        self.gerar_arquivo('produtos.csv', produtos)
        self.gerar_arquivo_links_coletados(url)
        """
        pass

    @classmethod
    def executar(self, download_upload_image=True, max_threads=20):
        """
        Executa o processo todo de coleta do crawler
        """
        print(f'\nIniciando crawler [{self.schema_nome}]\n')
        self.bd = Banco_De_Dados(self.schema_nome)
        self.coletados = self.bd.carregar_links_coletados_bd()
        urls = self.pegar_urls_produtos(self)
        tcp = self.bd.criar_conexoes_tcp_threads(max_threads)
        urls = self.dividir_base_urls(urls, max_threads)
        self.criar_threads(urls, tcp, max_threads)
        tcp.closeall()
        ## def carregar_planilha # carrega a planilha da integração (connector)
        ## def make_match # match automatico
        if download_upload_image == True:
            imagens = self.bd.carregar_imagens_bd()
            self.img.imagens_dowload(imagens)
            self.img.resize_image()
            # self.img.imagens_upload()
        self.bd.conn.close()

    @classmethod
    def dividir_base_urls(self, urls, max_threads):
        x = int(len(urls) / max_threads)
        urls_bases = lambda x: [urls[i:i+x] for i in range(0, len(urls), x)]
        urls_bases = urls_bases(x)
        return urls_bases

    @classmethod
    def criar_threads(self, urls_bases, tcp, max_threads):
        with concurrent.futures.ThreadPoolExecutor(max_workers=max_threads + 1) as executor:
            for base in urls_bases:
                cur = self.bd.cria_cursor(tcp)
                executor.submit(self.pegar_dados_produtos(self, urls=base, cur=cur))

    @classmethod
    def corrigir_encode_texto(self, texto):
        """
        Recebe uma string e verifica se há problemas com encode devido
        a acentos e caracteres especiais, e retorna a string corrigida
        """
        try:
            return unquote(texto, errors='strict')
        except UnicodeDecodeError:
            return unquote(texto, encoding='latin-1')

    @classmethod
    def encontrar_path(self):
        """
        Retorna o diretorio atual
        """
        nome_classe = self.pega_nome_classe()
        my_path = os.getcwd()
        my_path = my_path.replace('\\',self.barra)
        my_path = f'{str(my_path)}{self.barra}crawlers{self.barra}{nome_classe}'
        return my_path

    @classmethod
    def pega_nome_classe(self):
        return str(self.mro()[0]).split('.')[-1][:-2].lower()

    @classmethod
    def gerar_codigo_interno(self, marca, codigo_match):
        vogais = ['A','Á','À','Â','Ã','E','É','È','I','Í','Ï','O','Ó','Ô','Õ','Ö','U','Ú']
        codigo_interno = marca.upper()
        for vogal in vogais:
            codigo_interno = codigo_interno.replace(vogal,'')
        codigo_interno = codigo_interno + '-' + codigo_match
        codigo_interno = codigo_interno.replace(' ','_')
        return codigo_interno