from curadoria.libs.gerar_arquivo import Gerar_Arquivo
from selenium.webdriver.chrome.service import Service
from selenium import webdriver
from time import sleep


# from curadoria.crawlers.americanas import coleta_dados
# from curadoria.crawlers.arezzo import coleta_dados
from curadoria.crawlers.carrefour.carrefour import Carrefour
# from curadoria.crawlers.magalu import coleta_dados
# from curadoria.crawlers.mercado_livre import coleta_dados
# from curadoria.crawlers.ohboy import coleta_dados

crawlers = {
    # 'americanas': coleta_dados(),
    # 'arezzo': coleta_dados(),
    'carrefour': Carrefour(),
    # 'mercado_livre': coleta_dados(),
    # 'ohboy': coleta_dados()
}

class Selecionar_Crawler:
    def selecionar_crawler(urls_sites, driver):
        dados = []
        ga = Gerar_Arquivo()
        if type(urls_sites) is not list:
            urls_sites = [urls_sites]
        for url_site in urls_sites:
            linha = []
            driver.get(url_site)
            link_produto = driver.current_url
            crawler = link_produto.replace('https://','').split('/')[0]
            linha.append(crawler)
            linha.append(link_produto)
            dados.append(linha)
        ga.urls_busca(dados)
            
    def executar(arquivo):
        for row in arquivo:

            objeto_crawler = crawlers[crawler_escolhido]
            objeto_crawler.coleta_dados()
        pass

crawlers_count = []
for row in dados:
    crawler_escolhido = row[0].replace('www.','').replace('.com','').replace('.br','').replace('.','')
    print(row[0])
    try:
        objeto_crawler = crawlers_test[crawler_escolhido]
        objeto_crawler.run()
    except Exception as e:
        print(e)

crawlers_test = {
    'produtomercadolivre' : produtomercadolivre(),
    'amazon' : amazon(),
    'americanas' : americanas(),
    'tocadotabuleiro' : tocadotabuleiro(),
    'submarino' : submarino(),
    'lojaalfatechinformatica' : lojaalfatechinformatica(),
    'rihappy' : rihappy(),
    'casasbahia' : casasbahia(),
    'caixinhaboardgames' : caixinhaboardgames(),
    'lojasjckids' : lojasjckids(),
    'google' : google(),
    'extra' : extra(),
    'viladosjogos' : viladosjogos(),
    'shopee' : shopee(),
    'foundit' : foundit(),
    'pontofrio' : pontofrio(),
    'shoptime' : shoptime(),
}

class produtomercadolivre:
    def run():
        print('Crawler escolhido: produtomercadolivre')

class amazon:
    def run():
        print('Crawler escolhido: amazon')

class americanas:
    def run():
        print('Crawler escolhido: americanas')

class tocadotabuleiro:
    def run():
        print('Crawler escolhido: tocadotabuleiro')

class submarino:
    def run():
        print('Crawler escolhido: submarino')

class lojaalfatechinformatica:
    def run():
        print('Crawler escolhido: lojaalfatechinformatica')

class rihappy:
    def run():
        print('Crawler escolhido: rihappy')

class casasbahia:
    def run():
        print('Crawler escolhido: casasbahia')

class caixinhaboardgames:
    def run():
        print('Crawler escolhido: caixinhaboardgames')

class lojasjckids:
    def run():
        print('Crawler escolhido: lojasjckids')

class google:
    def run():
        print('Crawler escolhido: google')

class extra:
    def run():
        print('Crawler escolhido: extra')

class viladosjogos:
    def run():
        print('Crawler escolhido: viladosjogos')

class shopee:
    def run():
        print('Crawler escolhido: shopee')

class foundit:
    def run():
        print('Crawler escolhido: foundit')

class pontofrio:
    def run():
        print('Crawler escolhido: pontofrio')

class shoptime:
    def run():
        print('Crawler escolhido: shoptime')
