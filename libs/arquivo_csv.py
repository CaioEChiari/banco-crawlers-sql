from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.utils import COMMASPACE, formatdate
from email.mime.text import MIMEText
from datetime import datetime
from os.path import basename
import email.message
import csv, sys, os
import smtplib

class Arquivo_CSV:
    def __init__(self, csv_path, barra):
        self.csv_path = csv_path
        self.barra = barra

    def escrever_arquivo_csv(self, nome_do_arquivo, dados):
        if not nome_do_arquivo.endswith('.csv'):
            nome_do_arquivo += '.csv'
        chaves = []
        conteudos = []
        for key, value in dados.items():
            chaves.append(key)
            conteudos.append(value)
        arquivo = f'{self.csv_path}/{nome_do_arquivo}'
        if os.path.isfile(arquivo):
            file = open(arquivo, 'a', newline='')
            writer = csv.writer(file)
        else:
            file = open(arquivo, 'w', newline='')
            writer = csv.writer(file)
            writer.writerow(chaves)
        writer.writerow(conteudos)
        file.close()

    def carregar_arquivo_csv(self, nome_do_arquivo):
        """
        Carrega os links dos produtos encontrados do arquivo: 
        links_encontrados.csv
        E retorna uma lista com os resultados
        """
        if not nome_do_arquivo.endswith('.csv'):
            nome_do_arquivo += '.csv'
        arquivo = f'{self.csv_path}{self.barra}{nome_do_arquivo}'
        try:
            with open(arquivo, 'r') as file:
                encontrados = list(csv.reader(file))
                for i in range(len(encontrados)):
                    encontrados[i] = str(encontrados[i][0])
            return encontrados
        except FileNotFoundError:
            return []
        
    def deletar_relatorios(self):
        for relatorio in os.listdir(self.csv_path):
            os.remove(f'{self.csv_path}{self.barra}{relatorio}')

    def gravar_erro(self, file, crawler='', url='', image_file=''):
        exc_type, exc_msg, exc_tb = sys.exc_info()
        exc_type = str(exc_type).replace('<class ', '').replace('>', '')
        if exc_type == 'KeyboardInterrupt':
            return
        exc_msg = str(exc_msg)
        number_row = str(exc_tb.tb_lineno)
        erro = {
            'file_name': exc_tb.tb_frame.f_code.co_filename,
            'exc_type': exc_type,
            'exc_msg': str(exc_msg),
            'number_row': str(exc_tb.tb_lineno),
        }
        if crawler != '':
            erro['crawler'] = crawler
        if url != '':
            erro['url'] = url
        if image_file != '':
            erro['image_file'] = image_file            
        self.escrever_arquivo_csv(file, erro)

    def send_mail(self):
        diretorio_raiz = os.getcwd().replace('\\', self.barra)
        files = [f'{diretorio_raiz}{self.barra}relatorios{self.barra}{i}' for i in os.listdir(f'{diretorio_raiz}{self.barra}relatorios') if os.path.isfile(os.path.join(f'{diretorio_raiz}{self.barra}relatorios', i))]
        body_email = '''
        Relatórios de erros em anexo.
        '''
        send_to = ['matheus.martins@nappsolutions.com', 'artur.baccarin@nappsolutions.com']
        sender = 'napp.curadoria@gmail.com'
        msg = MIMEMultipart()
        msg['Subject'] = 'Relatórios de Erros'
        msg['From'] = sender
        password = 'curadoria154878'
        msg['To'] = COMMASPACE.join(send_to)
        msg['Date'] = formatdate(localtime=True)
        msg.add_header('Content-Type', 'text/html')
        msg.attach(MIMEText(body_email))
        for f in files or []:
            with open(f, "rb") as fil:
                part = MIMEApplication(fil.read(), Name=basename(f))
            # After the file is closed
            part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
            msg.attach(part)

        s = smtplib.SMTP('smtp.gmail.com: 587')
        s.starttls()
        s.login(msg['From'], password)
        s.sendmail(sender, send_to, msg.as_string().encode('utf-8'))
        s.close()