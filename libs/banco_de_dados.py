from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from psycopg2.pool import ThreadedConnectionPool
import psycopg2

class Banco_De_Dados:
    def __init__(self, schema_nome):
        self.schema_nome = schema_nome
        self.cria_banco_de_dados()
        self.conn, self.cur = self.conectar_banco_de_dados('crawlers')
        self.cria_banco_schema_campos_obrigatorios()

    def cria_banco_de_dados(self):
        conn, cur = self.conectar_banco_de_dados('postgres')
        cur.execute(f"SELECT 1 FROM pg_catalog.pg_database WHERE datname = 'crawlers'")
        existe = cur.fetchone()
        if not existe:
            cur.execute('create database crawlers')
        cur.close()
        conn.close()

    def criar_dicionario_campos_obrigatorios(self):
        produtos = {
            'referencia': '',
            'sku': '',
            'ean': '',
            'product_id': '',
            'nome': '',
            'categoria': '',
            'nome_crawler': '',
            'marca': '',
            'cor': '',
            'tamanho': '',
            'imagens': '',
            'azure_imagens': '',
            'descricao': '',
            'data_coleta': '',
            'url': ''}
        return produtos

    def conectar_banco_de_dados(self, bd_nome='crawlers'):
        conn = psycopg2.connect(f'dbname={bd_nome} user=user_crawlers password=napp154878')
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = conn.cursor()
        return conn, cur

    def cria_banco_schema_campos_obrigatorios(self):
        self.cur.execute('''
                SELECT table_schema
                FROM information_schema.tables
                WHERE table_schema != 'pg_catalog'
                AND table_schema != 'information_schema'
                AND table_type='BASE TABLE'
                ORDER BY table_schema''')
        schemas = self.cur.fetchall()
        schemas_lista = []
        for schema in schemas:
            schemas_lista.append(schema[0])
        if self.schema_nome not in schemas_lista:
            try:
                self.cur.execute(f'''CREATE schema {self.schema_nome}''')
            except psycopg2.errors.lookup('23505'):
                pass
        self.cur.execute(f'''
                SELECT table_name
                FROM information_schema.tables
                WHERE table_schema = '{self.schema_nome}';
                ''')
        tabelas = self.cur.fetchall()
        tabelas_lista = []
        for tabela in tabelas:
            tabelas_lista.append(tabela[0])
        if 'campos_obrigatorios' not in tabelas_lista:
            self.cur.execute(f'''
                CREATE TABLE {self.schema_nome}.campos_obrigatorios(
                    id SERIAL PRIMARY KEY,
                    referencia varchar,
                    sku varchar,
                    ean varchar,
                    product_id varchar,
                    nome varchar,
                    categoria varchar,
                    nome_crawler varchar NOT NULL,
                    marca varchar,
                    cor varchar,
                    tamanho varchar,
                    imagens varchar NOT NULL,
                    azure_imagens varchar,
                    descricao varchar,
                    data_coleta varchar NOT NULL,
                    url varchar NOT NULL,
                    campos_adicionais jsonb
                    );
                ''')

    def select_db_to_list(self, selecteds):
        lista = []
        for select in selecteds:
            lista.append(select[0])
        return lista

    def adicionar_dados_obrigatorios_produto_bd(self, cur, campos_obrigatorios, campos_adicionais={}): # // adicionar_links_encontrados // adicionar_links_coletados
        cur.execute(f'''
            SELECT column_name
            FROM information_schema.columns
            WHERE table_schema = '{self.schema_nome}'
            AND table_name = 'campos_obrigatorios';
            ''')
        columns_names = cur.fetchall()
        columns = ''
        dicionarios = [campos_obrigatorios, campos_adicionais]
        for dicionario in dicionarios:
            for key in dicionario.keys():
                if dicionario[key] is None:
                    dicionario[key] = ''
                if dicionario[key].find("d'") != -1:
                    dicionario[key] = dicionario[key].replace("d'",'d’')
                if dicionario[key].find("'") != -1:
                    dicionario[key] = dicionario[key].replace("'","''")
                if dicionario[key].find('"') != -1:
                    dicionario[key] = dicionario[key].replace('"',"''")
        dados = []
        for column in columns_names:
            if column[0] == 'id':
                continue
            columns += column[0] + ', '
            if column[0] == 'campos_adicionais':
                continue
            dados.extend([campos_obrigatorios[column[0]]])
        columns = columns[:-2]
        
        dados = str(dados).replace('[','').replace(']','')
        dados = dados.replace('"',"'")
        campos_adicionais = str(campos_adicionais).replace("'",'"')
        dados += f", '{campos_adicionais}'"

        try:
            cur.execute(f'''
                INSERT INTO {self.schema_nome}.campos_obrigatorios(
                    {columns})
                VALUES(
                    {dados})
                ''')
            print(f"{campos_obrigatorios['nome_crawler']} - Produto cadastrado com sucesso")
            return True
        except psycopg2.errors.lookup('23505'):
            print(f"{campos_obrigatorios['nome_crawler']} - Produto já cadastrado")
            return False

    def carregar_links_coletados_bd(self):
        self.cur.execute(f'''
            SELECT url
            FROM {self.schema_nome}.campos_obrigatorios
            ''')
        urls = self.cur.fetchall()
        urls = self.select_db_to_list(urls)
        return urls

    #def carregar_imagens_bd(self):
    #    self.cur.execute(f'''
    #        SELECT codigo_interno, imagens
    #        FROM {self.schema_nome}.campos_obrigatorios
    #        ''')
    #    imagens = self.cur.fetchall()
    #    return imagens

    def criar_conexoes_tcp_threads(self, max_threads):
        DSN = "postgresql://postgres:toor@localhost/crawlers"
        tcp = ThreadedConnectionPool(1, max_threads + 2, DSN)
        return tcp

    def cria_cursor(self, tcp):
        conn = tcp.getconn()
        conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = conn.cursor()
        return cur

    def add_azure_image_url_bd(self):
        pass