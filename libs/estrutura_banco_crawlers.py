from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import psycopg2

# conn = psycopg2.connect('dbname=postgres user=postgres password=toor')
conn = psycopg2.connect('dbname=crawler_tip_top user=postgres password=toor')
# conn = psycopg2.connect('dbname=teste user=postgres password=toor')
# conn = psycopg2.connect('dbname=produtos_curados user=postgres password=toor')
conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
cur = conn.cursor()

db_nome = str(Mac.mro()[0]).split('.')[-1][:-2]
cur.execute(f"SELECT 1 FROM pg_catalog.pg_database WHERE datname = '{db_nome}'")
exists = cur.fetchone()
if not exists:
    cur.execute(f'create database {db_nome}')

# cur.execute('drop database crawler_tip_top')

#codigo_interno PRIMARY KEY é só as consoantes da marca e replace(' ','-') + '-' + codigo_match
#e será o nome da imagem no azure
cur.execute('''
            CREATE TABLE crawler(
            codigo_interno varchar PRIMARY KEY,
            codigo_referencia varchar,
            sku varchar,
            ean varchar,
            product_id varchar,
            nome varchar,
            categoria varchar,
            marca varchar,
            cor varchar,
            tamanho varchar,
            imagens varchar,
            descricao varchar,
            data_coleta varchar,
            url varchar);
            ''')

# cur.execute('''
#             CREATE TABLE produtos_curados(
#             codigo_interno varchar PRIMARY KEY,
#             codigo_match varchar,
#             ean varchar,
#             nome_produto_o2o varchar,
#             nome_produto_connector varchar,
#             descricao varchar,
#             imagem varchar,
#             marca varchar,
#             cor varchar,
#             tamanho varchar,
#             categoria_google varchar,
#             categoria_connector varchar,
#             categoria_id_connector varchar,
#             segmento_referencia varchar,
#             data_adequacao varchar,
#             url varchar);
#             ''')

# tabela_segmento:
# 'codigo_interno'
# .
# .
valor = ['nomesss','numeross123']
cur.execute(f"INSERT INTO teste(nome, numero) VALUES, ({valor[0]}, {valor[1]})")
cur.execute("INSERT INTO teste(nome, numero) VALUES (%s, %s)", ('caio', '23'))


cur.execute('''
            INSERT INTO crawler_tip_top(
                codigo_interno,
                codigo_referencia,
                sku,
                ean,
                product_id,
                nome,
                categoria,
                marca,
                cor,
                tamanho,
                imagens,
                descricao,
                data_coleta,
                url)
            VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',(
                123, 
                'xpto')
            ''')

# cria colunas faltantes
cur.execute('''
            SELECT column_name
            FROM information_schema.columns
            WHERE table_schema = 'public'
            AND table_name = 'campos_adicionais';
            ''')
columns_names = cur.fetchall()
# print(columns_names)
columns = []
set_dict = set(list(dicionario_produto.keys()))
for column in columns_names:
    columns.append(column[0])
if len(set(columns) - set_dict) > 1:
    print('cria campos')
else:
    print('ok')
cur.close()
conn.close()


dicionario_produto['marca'] = 'Tip Top'
dicionario_produto['categoria'] = ''
dicionario_produto['ean'] = ''
dicionario_produto['codigo_referencia'] = 'PR.1501.1012060'
dicionario_produto['url'] = 'https://www.tiptop.com.br/macacao-bebe-ziper-e-capuz-unicornio-com-pezinho-pelucia-p370737'
dicionario_produto['data_coleta'] = '2021-09-17 17:33:18.571744'
dicionario_produto['sku'] = '1501.130911'
dicionario_produto['product_id'] = 370773
dicionario_produto['nome'] = 'MACACAO BEBE ROSA RN'
dicionario_produto['cor'] = 'ROSA'
dicionario_produto['tamanho'] = 'RN'
dicionario_produto['imagens'] = 'https://d3mstcthfjpw3m.cloudfront.net/Custom/Content/Products/37/07/370737_macacao-bebe-rosa-e-1012060_l1_637514064130375773.jpg, '
dicionario_produto['descricao'] = 'O Macacão bebê zíper e capuz'

dicionario_produto = {
    'codigo_interno' : '',
    'codigo_match' : '',
    'codigo_referencia' : '',
    'sku' : '',
    'ean' : '',
    'product_id' : '',
    'nome' : '',
    'categoria' : '',
    'marca' : '',
    'cor' : '',
    'tamanho' : '',
    'imagens' : '',
    'descricao' : '',
    'data_coleta' : '',
    'url' : '',
}

'''
##-------------------------------------
# Create the file repository configuration:
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
# Import the repository signing key:
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
# Update the package lists:
sudo apt-get update
# Install the latest version of PostgreSQL.
# If you want a specific version, use 'postgresql-12' or similar instead of 'postgresql':
sudo apt-get -y install postgresql
sudo su postgres
psql
'''

import psycopg2
from psycopg2.extensions import AsIs

bd_nome = 'crawlers'
conn = psycopg2.connect(f'dbname={bd_nome} user=postgres password=toor')
conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
cur = conn.cursor()

cur.execute('''SELECT table_schema
                      FROM information_schema.tables
                      WHERE table_schema != 'pg_catalog'
                      AND table_schema != 'information_schema'
                      AND table_type='BASE TABLE'
                      ORDER BY table_schema''')

schemas = cur.fetchall()
schemas_lista = []
for schema in schemas:
    schemas_lista.append(schema[0])