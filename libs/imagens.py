from azure.core.exceptions import HttpResponseError, ResourceExistsError
from azure.storage.blob import BlobServiceClient, ContentSettings
from libs.banco_de_dados import Banco_De_Dados
from libs.arquivo_csv import Arquivo_CSV
from PIL import UnidentifiedImageError
from urllib.error import HTTPError
import requests.packages.urllib3
from PIL import Image, ImageFile
from shutil import copyfile
import requests
import socket
import urllib
import PIL
import csv
import os


class Image_Manipulation():
    def __init__(self, my_path, schema_nome, barra):
        self.barra = barra
        self.my_path = my_path
        self.schema_nome = schema_nome
        self.bd = Banco_De_Dados(self.schema_nome)
        diretorio_raiz = os.getcwd().replace('\\', self.barra)
        self.arq_csv = Arquivo_CSV(f'{diretorio_raiz}{self.barra}relatorios', self.barra)
        self.downloads_path = f'{self.my_path}{self.barra}images{self.barra}downloads'
        self.resizing_error_path = f'{self.my_path}{self.barra}images{self.barra}resizing_error'
        self.resized_path = f'{self.my_path}{self.barra}images{self.barra}resized'
        requests.packages.urllib3.disable_warnings()

    def imagens_dowload(self, imagens):
        """
        ### Em desenvolvimento ###
        """
        hdr = {'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'}
        cont_img = 0
        for imagem in imagens:
            cont_img += 1
            links = imagem[1].split(', ')
            name = imagem[0]
            num = ''
            print(f'Downloading image {cont_img} of {len(imagens)}')
            cont = 0
            for link in links:
                if link == None or link == 'None':
                    continue
                if cont > 0:
                    num = '_' + str(cont)
                cont += 1
                try:
                    response = requests.get(link)
                    file = open(f'{self.downloads_path}{self.barra}{name}{num}.jpg', 'wb')
                    file.write(response.content)
                    file.close()
                except:
                    try:
                        req = urllib.request.Request(link, headers=hdr)
                        response = urllib.request.urlopen(req, timeout=5)
                        file = open(f'{self.downloads_path}{self.barra}{name}{num}.jpg', 'wb')
                        file.write(response.read())
                        file.close()
                    except UnicodeEncodeError:
                        response = requests.get(link, timeout=5)
                        file = open(f'{self.downloads_path}{self.barra}{name}{num}.jpg', 'wb')
                        file.write(response.content)
                        file.close()
                    except:
                        self.arq_csv.gravar_erro('images_errors', crawler=self.schema_nome, image_file=f'{name}{num}.jpg', url=link)
                file.close()

    def save_and_remove(self, object_image, image_file):
        object_image.save(f'{self.resized_path}{self.barra}{image_file}')
        os.remove(f'{self.downloads_path}{self.barra}{image_file}')

    def image_resizing_error(self, image_file, error):
        copyfile(f'{self.downloads_path}{self.barra}{image_file}',f'{self.resizing_error_path}{self.barra}{image_file}')
        os.remove(f'{self.downloads_path}{self.barra}{image_file}')
        self.arq_csv.gravar_erro('images_errors', crawler=self.schema_nome, image_file=image_file)

    def resize_image(self, target_size=1000):
        ImageFile.LOAD_TRUNCATED_IMAGES = True
        images_files = [f for f in os.listdir(self.downloads_path) if os.path.isfile(os.path.join(self.downloads_path, f))]
        i = 0
        for image_file in images_files:
            i += 1
            print(f'Resizing image {str(i)} / {str(len(images_files))}')
            try:
                image = Image.open(f'{self.downloads_path}{self.barra}{image_file}')
                image = self.remove_transparency(image)
                width, height = image.size
                if width != height:
                    size = (target_size, target_size)
                    percent = target_size / max(width, height)
                    fsize = int(min(width, height)*float(percent))
                    if width > height:
                        parametro = (target_size, fsize)
                    else:
                        parametro = (fsize, target_size)
                    image = image.resize(parametro, PIL.Image.LANCZOS)
                    new = Image.new('RGB', size, (255, 255, 255))
                    new.paste(image,(int((size[0] - image.size[0]) / 2), int((size[1] - image.size[1]) / 2)))
                    self.save_and_remove(new, image_file)
                elif width != target_size or height != target_size:
                    print(f'Changing size of image {image_file}')
                    image = image.resize((target_size, target_size), PIL.Image.LANCZOS)
                    self.save_and_remove(image, image_file)
                else:
                    print(f'Image {image_file} already in the right size')
                    self.save_and_remove(image, image_file)
                    i += 1
                    continue
            except UnidentifiedImageError:
                self.image_resizing_error(image_file, 'Image corrupted')
            except OSError as err:
                if err == 'cannot write mode P as JPEG':
                    converted = image.convert('RGB')
                    self.save_and_remove(converted, image_file)
                else:
                    image.load()
                    bg = Image.new('RGB', image.size, (255, 255, 255))
                    try:
                        bg.paste(image, mask=image.split()[3])
                        self.save_and_remove(bg, image_file)
                    except IndexError:
                        print(f'Image {image_file} has alpha transparency')
                        self.image_resizing_error(image_file, 'Image has alpha transparency')
                        continue
            except Exception as error:
                error = str(error)
                try:
                    image.close()
                except:
                    pass
                print(f'ERROR resizing {image_file}')
                self.image_resizing_error(image_file, error)

    def remove_transparency(self, image, bg_colour=(255, 255, 255)):
        ImageFile.LOAD_TRUNCATED_IMAGES = True
        if image.mode in ('RGBA', 'LA', 'P') or (image.mode == 'P' and 'transparency' in image.info):
            alpha = image.convert('RGBA').split()[-1]
            bg = Image.new('RGBA', image.size, bg_colour + (255,))
            bg.paste(image, mask=alpha)
            return bg
        else:
            return image

    def imagens_upload(overwrite=False, container='images', acc='nappcatalog'):
        # str(input('Insert you Azure account name: '))
        # acc = acc
        # str(input('Paste your Azure Storage Connection String here: ')) #OR os.getenv('AZURE_STORAGE_CONNECTION_STRING')
        connection_string = 'DefaultEndpointsProtocol=https;AccountName=nappcatalog;AccountKey=/d/mboxBjXlTcPU27/U+IkoxelNvqC+s/bvvzrKI3FYnegHTkVTGyUuSCujoc3mOELlybbznx5R7/bsK4RKu5g==;EndpointSuffix=core.windows.net'
        blob_service_client = BlobServiceClient.from_connection_string(connection_string)
        container_client = blob_service_client.get_container_client(container)
        try:
            container_client.create_container()
        except ResourceExistsError:
            pass
        url_pre = f'https://{acc}.blob.core.windows.net/{container}/'
        images_files = [i for i in os.listdir(self.resized_path) if os.path.isfile(os.path.join(self.resized_path, i))]
        my_content = ContentSettings(content_type='image/jpeg')
        cont = 0
        data = {}
        for image_file in images_files:
            cont += 1
            #Upload images to container
            try:
                with open(f'{self.resized_path}{self.barra}{image_file}', 'rb') as f:
                    fileName = f"{image_file.split('.jpg')[0]}"
                    url_uploads = f'{url_pre}{str(image_file)}'
                    container_client.upload_blob(name=image_file, data=f, overwrite=overwrite, content_settings=my_content)
                    print(f'Image [{image_file}] Upload completed - {str(cont)}{self.barra}{str(len(images_files))}')
                    print(f'Image URL: [{url_uploads}]')
            except FileNotFoundError:
                print(f'Arquivo [{image_file}] não encontrado')
                self.arq_csv.gravar_erro('upload_errors', crawler=self.schema_nome, image_file=image_file, url=url_uploads, error='Arquivo não encontrado')
            except ResourceExistsError:
                print(f'Arquivo [{image_file}] já existe no Azure')
                self.arq_csv.gravar_erro('upload_errors', crawler=self.schema_nome, image_file=image_file, url=url_uploads, error='Arquivo já existe no Azure')
            code = image_file.split('_')[0].replace('.jpg','')
            if code not in list(data.keys()):
                data[code] = []
            data[code].append(image_file)

########################################################################################################
## Testes upload

# container = 'images'
# acc = 'nappcatalog'
# images_files = aa.img.resized_path
# images_files = [i for i in os.listdir(images_files) if os.path.isfile(os.path.join(images_files, i))]
# url_pre = f'https://{acc}.blob.core.windows.net/{container}/'
# cont = 0
# data = {}
# for image_file in images_files:
#     cont += 1
#     code = image_file.split('_')[0].replace('.jpg','')
#     url_uploads = f'{url_pre}{str(image_file)}'
#     if code not in list(data.keys()):
#         data[code] = []
#     data[code].append(url_uploads)