from libs.arquivo_csv import Arquivo_CSV
import os, platform
from crawlers.havaianas.havaianas import Havaianas
from crawlers.tip_top.tip_top import Tip_Top
from crawlers.arezzo.arezzo import Arezzo
from crawlers.mac.mac import Mac
from crawlers.zee_dog.zee_dog import Zee_Dog
from crawlers.cia_maritima.cia_maritima import Cia_Maritima
from crawlers.puket.puket import Puket

def main():
    system = platform.system()
    if system == 'Linux':
        barra = '\\'
    elif system == 'Windows':
        barra = '/'
    my_path = os.getcwd()
    my_path = my_path.replace('\\', barra)
    my_path += f'{barra}relatorios'
    arq = Arquivo_CSV(my_path, barra)

    threads_default = 20
    crawlers = {#'Havaianas': {'object': Havaianas(), 'download_and_upload': False, 'max_threads': threads_default},
                #'Tip_Top': {'object': Tip_Top(), 'download_and_upload': False, 'max_threads': threads_default},
                #'Arezzo': {'object': Arezzo(), 'download_and_upload': False, 'max_threads': 5},
                #'Mac': {'object': Mac(), 'download_and_upload': False, 'max_threads': threads_default},
                #'Cia_Maritima': {'object': Cia_Maritima(), 'download_and_upload': False, 'max_threads': 5},
                #'Zee_Dog': {'object': Zee_Dog(), 'download_and_upload': False, 'max_threads': threads_default},
                #'Puket': {'object': Puket(), 'download_and_upload':False, 'max_threads': 5}
                }

    for key, value in crawlers.items():
        try:
            crawler = value['object']
            crawler.executar(value['download_and_upload'], value['max_threads'])
        except:
            arq.gravar_erro('erros_main', crawler=key)
    arq.send_mail()
    arq.deletar_relatorios()

if __name__ == "__main__":
    main()